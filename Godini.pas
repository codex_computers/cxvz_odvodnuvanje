unit Godini;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl,
  dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, ActnList, cxBarEditItem,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ExtCtrls, cxCheckBox, cxGroupBox, dxRibbonSkins,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  cxNavigator, dxRibbonCustomizationForm, System.Actions,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;

type
  TfrmGodini = class(TfrmMaster)
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1OTVORENA: TcxGridDBColumn;
    cxGrid1DBTableView1ZATVORENA: TcxGridDBColumn;
    cxGrid1DBTableView1ARHIVIRANA: TcxGridDBColumn;
    aArhiviraj: TAction;
    aDearhiviraj: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    aZatvori: TAction;
    cxGroupBoxSostojba: TcxGroupBox;
    chbOtvorena: TcxDBCheckBox;
    chbSaldirana: TcxDBCheckBox;
    chbArhivirana: TcxDBCheckBox;
    chbZatvorena: TcxDBCheckBox;
    cxGrid1DBTableView1SALDIRANA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aArhivirajExecute(Sender: TObject);
    procedure aDearhivirajExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGodini: TfrmGodini;

implementation

uses dmKonekcija, DaNe, Utils, dmUnit, dmMaticni;

{$R *.dfm}

procedure TfrmGodini.aArhivirajExecute(Sender: TObject);
var
   status: TStatusWindowHandle;
begin
//  inherited;
 { if ((dm.tblGodiniARHIVIRANA.Value = 0) and (dm.tblGodiniZATVORENA.Value = 1)) then
  begin
      frmDaNe := TfrmDaNe.Create(self, '���������� �� ������ - ' + IntToStr(dm.tblGodiniGODINA.Value) , '���� ��������� ������ �� �� ���������� ��������,', 1);
      if (frmDaNe.ShowModal <> mrYes) then
      begin
          Abort;
      end
      else
      begin
        frmDaNe := TfrmDaNe.Create(self, '������� �� ����������!  ' + IntToStr(dm.tblGodiniGODINA.Value) , '�������� �� ����������? ���������� ���� �� ����� ��������!,', 1);
        if (frmDaNe.ShowModal = mrYes) then
        begin
           status := cxCreateStatusWindow();
            try
                dm.procArhiviraj.ParamByName('GODINA').AsInteger:= dm.tblGodiniGODINA.Value;
                dm.procArhiviraj.ParamByName('FIRMA').AsInteger:= dm.tblGodiniRE.Value;

                dm.procArhiviraj.Prepare;
                dm.procArhiviraj.ExecProc;
            finally
        	    cxRemoveStatusWindow(status);
            end;
            ShowMessage('�������� � ����������');
            dm.tblGodini.FullRefresh;
        end;

      end;
  end
  else
  begin
      ShowMessage('�������� � ��� ���������� ��� �� � ���������');
  end;
       }
end;

procedure TfrmGodini.aAzurirajExecute(Sender: TObject);
begin
  if dm.tblGodiniARHIVIRANA.value = 0 then
    inherited
  else
    ShowMessage('�������� � ���������� ������ ����� �� ���������');

end;

procedure TfrmGodini.aBrisiExecute(Sender: TObject);
begin
//  inherited;

    frmDaNe := TfrmDaNe.Create(self, '������ �� ������ - '+IntToStr(dm.tblGodiniGODINA.Value) , '���� ��������� ������ �� �� ��������� ��������, �� ����� ��������� � ���� �������� �� ���������� ������', 1);
    if (frmDaNe.ShowModal <> mrYes) then
    begin
        Abort;
    end
    else
    begin
//        dm.proc_BrisiGodina.ParamByName('GODINA').AsInteger:= dm.tblGodiniGODINA.Value;
//        dm.proc_BrisiGodina.ParamByName('ARHIVIRANA').AsInteger:= dm.tblGodiniARHIVIRANA.Value;
//        dm.proc_BrisiGodina.ParamByName('FIRMA').AsInteger:= dm.tblGodiniRE.Value;
//
//        dm.proc_BrisiGodina.Prepare;
//        dm.proc_BrisiGodina.ExecProc;

        dm.tblGodini.Delete;
        dm.tblGodini.FullRefresh;
    end;
end;

procedure TfrmGodini.aDearhivirajExecute(Sender: TObject);
begin
//  inherited;
 { if ((dm.tblGodiniARHIVIRANA.value = 1) and (dm.tblGodiniZATVORENA.value = 1)) then
  begin
      frmDaNe := TfrmDaNe.Create(self, '������������ �� ������ - '+IntToStr(dm.tblGodiniGODINA.Value) , '���� ��������� ������ �� �� ������������ ��������,', 1);
      if (frmDaNe.ShowModal <> mrYes) then
      begin
          Abort;
      end
      else
      begin
          dm.procDearhiviraj.ParamByName('GODINA').AsInteger:= dm.tblGodiniGODINA.Value;
          dm.procDearhiviraj.ParamByName('FIRMA').AsInteger:= dm.tblGodiniRE.Value;

          dm.procDearhiviraj.Prepare;
          dm.procDearhiviraj.ExecProc;

          dm.tblGodini.FullRefresh;
      end;

  end
  else
  begin
      ShowMessage('�������� �� � ���������� ��� �� � ���������');
  end;
    }

end;

procedure TfrmGodini.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.tblGodini.Close;

  dm.tblGodini.ParamByName('o').Value:= 1;
  dm.tblGodini.Open;

  dm.godina := dm.tblGodiniGODINA.Value;
  dm.zatvorena:= dm.tblGodiniZATVORENA.Value;
  dm.otvorena:= dm.tblGodiniOTVORENA.Value;
end;

procedure TfrmGodini.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblGodini.Close;

  dm.tblGodini.ParamByName('o').Value:= '%';
  dm.tblGodini.Open;
end;

end.
