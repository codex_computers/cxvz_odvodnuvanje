unit StariTuzbi;

interface

uses
  DateUtils,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, System.Actions,
  FIBDataSet, pFIBDataSet, cxNavigator, dxCore, cxDateUtils, cxSpinEdit,
  FIBQuery, pFIBQuery, pFIBStoredProc;

type
//  niza = Array[1..5] of Variant;

  TfrmStariTuzbi = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dsStari: TDataSource;
    tblStari: TpFIBDataSet;
    tblStariID: TFIBIntegerField;
    tblStariRE: TFIBIntegerField;
    tblStariFAKTURA_BROJ: TFIBBCDField;
    tblStariSEZONA: TFIBIntegerField;
    tblStariDOLG: TFIBBCDField;
    tblStariOPIS: TFIBStringField;
    tblStariVODOKORISNIK_NAZIV: TFIBStringField;
    GridPanel1: TGridPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1VODOKORISNIK_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1FAKTURA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1SEZONA: TcxGridDBColumn;
    cxGrid1DBTableView1DOLG: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    tblStariVODOKORISNIK_ID: TFIBBCDField;
    cxGrid1DBTableView1VODOKORISNIK_ID: TcxGridDBColumn;
    dsDUG: TDataSource;
    tblDUG: TpFIBDataSet;
    tblDUGRE_OUT: TFIBIntegerField;
    tblDUGVODOKORISNIK_ID: TFIBBCDField;
    tblDUGVODOKORISNIK_NAZIV: TFIBStringField;
    tblDUGEMB: TFIBStringField;
    tblDUGDANOCEN: TFIBStringField;
    tblDUGLK: TFIBStringField;
    tblDUGPRAVNO_LICE: TFIBSmallIntField;
    tblDUGFAKTURA_GODINA_ORIGINAL: TFIBIntegerField;
    tblDUGDATUM_F: TFIBDateField;
    tblDUGGODINA: TFIBIntegerField;
    tblDUGBROJ: TFIBBCDField;
    tblDUGTIP: TFIBIntegerField;
    tblDUGADRESA: TFIBStringField;
    tblDUGMESTO_NAZIV: TFIBStringField;
    tblDUGOPSTINA_OZNAKA: TFIBIntegerField;
    tblDUGOPSTINA_NAZIV: TFIBStringField;
    tblDUGO_IZNOS_DOLG: TFIBBCDField;
    tblDUGOPIS: TFIBStringField;
    tblDUGOPIS_FAKTURA: TFIBStringField;
    tblDUGSTARA_SIFRA: TFIBIntegerField;
    tblDUGEMBEDB: TFIBStringField;
    tblDUGTEL: TFIBStringField;
    cxGrid2DBTableView1RE_OUT: TcxGridDBColumn;
    cxGrid2DBTableView1VODOKORISNIK_ID: TcxGridDBColumn;
    cxGrid2DBTableView1VODOKORISNIK_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1EMB: TcxGridDBColumn;
    cxGrid2DBTableView1DANOCEN: TcxGridDBColumn;
    cxGrid2DBTableView1EMBEDB: TcxGridDBColumn;
    cxGrid2DBTableView1TEL: TcxGridDBColumn;
    cxGrid2DBTableView1LK: TcxGridDBColumn;
    cxGrid2DBTableView1PRAVNO_LICE: TcxGridDBColumn;
    cxGrid2DBTableView1FAKTURA_GODINA_ORIGINAL: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM_F: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1TIP: TcxGridDBColumn;
    cxGrid2DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid2DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1OPSTINA_OZNAKA: TcxGridDBColumn;
    cxGrid2DBTableView1OPSTINA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1O_IZNOS_DOLG: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS_FAKTURA: TcxGridDBColumn;
    cxGrid2DBTableView1STARA_SIFRA: TcxGridDBColumn;
    pnlNadDug: TPanel;
    pnlPodDug: TPanel;
    btnOznaci: TcxButton;
    Datum_do: TcxDateEdit;
    Label3: TLabel;
    Label1: TLabel;
    cxCheckBox1: TcxCheckBox;
    sGodina: TcxSpinEdit;
    btnPokazi: TcxButton;
    aPokazi: TAction;
    dateDatum: TcxDateEdit;
    Label2: TLabel;
    txtBroj: TcxTextEdit;
    lblBroj: TLabel;
    aZapisiSelektirani: TAction;
    tblDUGODVODNUVANJE: TFIBIntegerField;
    cxGrid2DBTableView1ODVODNUVANJE: TcxGridDBColumn;
    cxGridPopupMenu2: TcxGridPopupMenu;
    tblStariODVODNUVANJE: TFIBSmallIntField;
    tblStariDATUM: TFIBDateField;
    tblStariBROJ: TFIBStringField;
    cxGrid1DBTableView1ODVODNUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    tblDUGTUZENA: TFIBIntegerField;
    cxGrid2DBTableView1TUZENA: TcxGridDBColumn;
    Label4: TLabel;
    txtNotar: TcxTextEdit;
    Label5: TLabel;
    txtNPN: TcxTextEdit;
    tblStariNOTAR: TFIBStringField;
    tblStariNPN: TFIBStringField;
    cxGrid1DBTableView1NOTAR: TcxGridDBColumn;
    cxGrid1DBTableView1NPN: TcxGridDBColumn;
    pTuzbaPostoi: TpFIBStoredProc;
    aSkrijDolgovi: TAction;
    aSkrijTuzbi: TAction;
    aIzramni: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    txtIzvrsitel: TcxTextEdit;
    txtIbroj: TcxTextEdit;
    Label6: TLabel;
    Label7: TLabel;
    tblStariIZVRSITEL: TFIBStringField;
    tblStariIBROJ: TFIBStringField;
    cxGrid1DBTableView1IZVRSITEL: TcxGridDBColumn;
    cxGrid1DBTableView1IBROJ: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure btnOznaciClick(Sender: TObject);
    procedure aPokaziExecute(Sender: TObject);
    procedure tblStariBeforeDelete(DataSet: TDataSet);
    procedure aZapisiSelektiraniExecute(Sender: TObject);

    procedure cxGrid2DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aSkrijDolgoviExecute(Sender: TObject);
    procedure aSkrijTuzbiExecute(Sender: TObject);
    procedure aIzramniExecute(Sender: TObject);

  private
    { Private declarations }
     kako:boolean;
    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;
    function ImaTuzba(fakturaBroj: Int64): boolean;
    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmStariTuzbi: TfrmStariTuzbi;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmStariTuzbi.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmStariTuzbi.aNovExecute(Sender: TObject);
begin


//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmStariTuzbi.aAzurirajExecute(Sender: TObject);
var //kako:boolean;
  stil:TcxStyle;
begin
  kako:= not kako; // cxGrid1DBTableView1PL_BROJ.Editing;
  if kako then stil:=dmres.Golden else stil:=nil;

  cxGrid1DBTableView1DATUM.Options.Editing:=kako; // Editing:=kako;
  cxGrid1DBTableView1DATUM.Styles.Content:= stil; // dmkon.Golden; // Color:= //clYellow;
  cxGrid1DBTableView1BROJ.options.Editing:=kako;
  cxGrid1DBTableView1BROJ.Styles.Content:=stil;
  cxGrid1DBTableView1OPIS.options.Editing:=kako;
  cxGrid1DBTableView1OPIS.Styles.Content:=stil;

  cxGrid1DBTableView1NOTAR.options.Editing:=kako;
  cxGrid1DBTableView1NOTAR.Styles.Content:=stil;
  cxGrid1DBTableView1NPN.options.Editing:=kako;
  cxGrid1DBTableView1NPN.Styles.Content:=stil;

  cxGrid1DBTableView1IZVRSITEL.options.Editing:=kako;
  cxGrid1DBTableView1IZVRSITEL.Styles.Content:=stil;
  cxGrid1DBTableView1IBROJ.options.Editing:=kako;
  cxGrid1DBTableView1IBROJ.Styles.Content:=stil;

end;
//begin
//  // �������� �� ������� �� ��������� � ������ �� �� ����� ��� - �����-������
//
//
//
////  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
////  begin
////    dPanel.Enabled:=True;
////    lPanel.Enabled:=False;
////    prva.SetFocus;
////    cxGrid1DBTableView1.DataController.DataSet.Edit;
////  end
////  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
//end;

//	����� �� ������ �� ������������� �����
procedure TfrmStariTuzbi.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmStariTuzbi.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  brisiGridVoBaza(Name,cxGrid2DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmStariTuzbi.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmStariTuzbi.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmStariTuzbi.aIzramniExecute(Sender: TObject);
begin
  GridPanel1.ColumnCollection.BeginUpdate;
  GridPanel1.ColumnCollection[0].Value:=50;
  GridPanel1.ColumnCollection[2].Value:=50;
  GridPanel1.ColumnCollection.EndUpdate;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmStariTuzbi.aSkrijDolgoviExecute(Sender: TObject);
begin
  GridPanel1.ColumnCollection.BeginUpdate;
  GridPanel1.ColumnCollection[0].Value:=85;
  GridPanel1.ColumnCollection[2].Value:=15;
  GridPanel1.ColumnCollection.EndUpdate;
end;

procedure TfrmStariTuzbi.aSkrijTuzbiExecute(Sender: TObject);
begin
  GridPanel1.ColumnCollection.BeginUpdate;
  GridPanel1.ColumnCollection[0].Value:=15;
  GridPanel1.ColumnCollection[2].Value:=85;
  GridPanel1.ColumnCollection.EndUpdate;
end;

procedure TfrmStariTuzbi.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmStariTuzbi.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmStariTuzbi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmStariTuzbi.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmStariTuzbi.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmStariTuzbi.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmStariTuzbi.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmStariTuzbi.tblStariBeforeDelete(DataSet: TDataSet);
begin

    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmStariTuzbi.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmStariTuzbi.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmStariTuzbi.prefrli;
begin
end;

procedure TfrmStariTuzbi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmStariTuzbi.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmStariTuzbi.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
 //   procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link1);

    sobrano := true;

    tblStari.Close;
//    tblStari.ParamByName('APP').Value:= dmKon.aplikacija;   //prae problem VZ i VZO
    if dmkon.aplikacija = 'VZ' then
    begin
      tblStari.ParamByName('ODVODNUVANJE').Value:=0;
    end
    else
    begin
      tblStari.ParamByName('ODVODNUVANJE').Value:=1;
    end;

    tblStari.ParamByName('RE').Value:=dmKon.re;
    tblStari.Open;

    Datum_do.Date:=Date;
    sGodina.EditValue:=YearOf(Date);
    kako:=False;
//    tblDUG.Close;
//    tblDUG.ParamByName('APP').Value:=dmKon.aplikacija;
//    tblDUG.ParamByName('RE').Value:=dmKon.re;
//    tblDUG.ParamByName('SEZONA').Value:=2018;
//    tblDUG.ParamByName('PRETHODNI_GOD').Value:=1;
//    tblDUG.ParamByName('DATUM_ZAKLUCNO').Value:='31.12.2018';
////    tblDUG.ParamByName('DOLG_POGOLEM_OD').Value:=0;
//    tblDUG.Open;



//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmStariTuzbi.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmStariTuzbi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmStariTuzbi.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmStariTuzbi.cxGrid2DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid2DBTableView1TUZENA.Index] = 1) then
  begin
   aStyle := dmRes.RedLight;
  end;
end;

//  ����� �� �����
procedure TfrmStariTuzbi.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

procedure TfrmStariTuzbi.aZapisiSelektiraniExecute(Sender: TObject);
var
  I,RecordIndex, ColumnIndex: Integer;
  FirstName: string;
  status: TStatusWindowHandle;
begin
  status := cxCreateStatusWindow();
  try
    for I := 0 to cxGrid2DBTableView1.Controller.SelectedRecordCount - 1 do
    begin
      // zasite selektirani vo tabelata dolgovi - izvrsi nekoa akcija
      ColumnIndex := cxGrid2DBTableView1VODOKORISNIK_NAZIV.Index;
      RecordIndex := cxGrid2DBTableView1.Controller.SelectedRecords[I].RecordIndex;

      //proverka za da ne otide edna faktura 2 pati vo stari tuzbi
      if not ImaTuzba(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1BROJ.Index]) then
      begin
        tblStari.Insert;
        tblStariRE.Value:=dmkon.re;
        tblStariODVODNUVANJE.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1ODVODNUVANJE.Index]);
        tblStariFAKTURA_BROJ.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1BROJ.Index]);
        tblStariSEZONA.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1FAKTURA_GODINA_ORIGINAL.Index]);
        tblStariVODOKORISNIK_ID.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1VODOKORISNIK_ID.Index]);
        tblStariDOLG.Value:= (cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1O_IZNOS_DOLG.Index]);
        tblStariOPIS.Value:=VarToStr(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1OPIS.Index]);
        if (dateDatum.Text <> '') then tblStariDATUM.Value:=dateDatum.EditValue;
        tblStariBROJ.Value:=txtBroj.Text;
        tblStariNOTAR.Value:=txtNotar.Text;
        tblStariNPN.Value:=txtNPN.Text;

        tblStariIZVRSITEL.Value:=txtIzvrsitel.Text;
        tblStariIBROJ.Value:=txtIbroj.Text;

        tblStari.Post;
      end;

  //    FirstName := VarToStr(cxGrid2DBTableView1.DataController.Values[RecordIndex, ColumnIndex]);
  //    ShowMessage(FirstName);
    end;
    //aPokazi.Execute; -- da ne e bavno
  finally
    cxRemoveStatusWindow(status);
  end;

end;

function TfrmStariTuzbi.ImaTuzba(fakturaBroj:Int64):boolean;
begin
   pTuzbaPostoi.Close();

   pTuzbaPostoi.ParamByName('FAKTURA_BROJ').Value:=fakturaBroj;
   pTuzbaPostoi.Prepare;
   pTuzbaPostoi.ExecProc;

   if pTuzbaPostoi.FieldByName('rezultat').Value = 1 then
   begin
     result:=True;
   end
   else
   begin
     result:=False;
   end;

end;


procedure TfrmStariTuzbi.btnOznaciClick(Sender: TObject);
var
  I,RecordIndex, ColumnIndex: Integer;
  FirstName: string;
  status: TStatusWindowHandle;
begin
  status := cxCreateStatusWindow();
  try
    for I := 0 to cxGrid2DBTableView1.Controller.SelectedRecordCount - 1 do
    begin
      // zasite selektirani vo tabelata dolgovi - izvrsi nekoa akcija
      ColumnIndex := cxGrid2DBTableView1VODOKORISNIK_NAZIV.Index;
      RecordIndex := cxGrid2DBTableView1.Controller.SelectedRecords[I].RecordIndex;

      //proverka za da ne otide edna faktura 2 pati vo stari tuzbi
      if not ImaTuzba(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1BROJ.Index]) then
      begin
        tblStari.Insert;
        tblStariRE.Value:=dmkon.re;
        tblStariODVODNUVANJE.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1ODVODNUVANJE.Index]);
        tblStariFAKTURA_BROJ.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1BROJ.Index]);
        tblStariSEZONA.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1FAKTURA_GODINA_ORIGINAL.Index]);
        tblStariVODOKORISNIK_ID.Value:=(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1VODOKORISNIK_ID.Index]);
        tblStariDOLG.Value:= (cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1O_IZNOS_DOLG.Index]);
        tblStariOPIS.Value:=VarToStr(cxGrid2DBTableView1.DataController.Values[RecordIndex, cxGrid2DBTableView1OPIS.Index]);
        if (dateDatum.Text <> '') then tblStariDATUM.Value:=dateDatum.EditValue;
        tblStariBROJ.Value:=txtBroj.Text;
        tblStariNOTAR.Value:=txtNotar.Text;
        tblStariNPN.Value:=txtNPN.Text;
        tblStariIZVRSITEL.Value:=txtIzvrsitel.Text;
        tblStariIBROJ.Value:=txtIbroj.Text;

        tblStari.Post;
      end;

  //    FirstName := VarToStr(cxGrid2DBTableView1.DataController.Values[RecordIndex, ColumnIndex]);
  //    ShowMessage(FirstName);
    end;
    //aPokazi.Execute;   --avtomatski da se osvezat podatocite - ke bide bavno ke
  finally
    cxRemoveStatusWindow(status);
  end;





end;



//	����� �� ���������� �� �������
procedure TfrmStariTuzbi.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmStariTuzbi.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmStariTuzbi.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmStariTuzbi.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmStariTuzbi.aPokaziExecute(Sender: TObject);
var
  status: TStatusWindowHandle;
begin
  if ((Datum_do.Text <> '') and (sGodina.Text<>'' )) then
  begin
  status := cxCreateStatusWindow();
  try
    tblDUG.Close;
    tblDUG.ParamByName('APP').Value:=dmKon.aplikacija;
    tblDUG.ParamByName('RE').Value:= dmKon.re;
    tblDUG.ParamByName('SEZONA').Value:= sGodina.EditValue;
    if cxCheckBox1.Checked = true then
       tblDUG.ParamByName('PRETHODNI_GOD').Value:= 1
    else
       tblDUG.ParamByName('PRETHODNI_GOD').Value:= 0;
  //  tblDUG.ParamByName('DATUM_OD').Value:= Datum_od.EditValue;
    tblDUG.ParamByName('DATUM_ZAKLUCNO').Value:= Datum_do.EditValue;
 //   tblDUG.ParamByName('DOLG_POGOLEM_OD').Value:= iznos.EditValue;
    tblDUG.Open;
    if tblDUG.RecordCount>0 then cxGrid2.SetFocus;
  finally
    cxRemoveStatusWindow(status);
  end;


//    if dm.tblDUG.RecordCount>0 then cxGrid2.SetFocus;
////    if dm.tblPDolznici.RecordCount>0 then cxGrid1DBTableView1VODOKORISNIK_NAZIV.fo
//
//    tblDUG.Close;
//    tblDUG.ParamByName('APP').Value:=dmKon.aplikacija;
//    tblDUG.ParamByName('RE').Value:=dmKon.re;
//    tblDUG.ParamByName('SEZONA').Value:=2018;
//    tblDUG.ParamByName('PRETHODNI_GOD').Value:=1;
//    tblDUG.ParamByName('DATUM_ZAKLUCNO').Value:='31.12.2018';
////    tblDUG.ParamByName('DOLG_POGOLEM_OD').Value:=0;
//    tblDUG.Open;


  end
  else ShowMessage('�������� ������ !');
end;

procedure TfrmStariTuzbi.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmStariTuzbi.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    //cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    //cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmStariTuzbi.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmStariTuzbi.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmStariTuzbi.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmStariTuzbi.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
