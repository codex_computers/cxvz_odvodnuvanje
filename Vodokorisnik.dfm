inherited frmVodokorisnik: TfrmVodokorisnik
  Caption = #1042#1086#1076#1082#1086#1088#1080#1089#1085#1080#1082
  ClientHeight = 709
  ClientWidth = 984
  ExplicitWidth = 1000
  ExplicitHeight = 748
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 984
    Height = 306
    ExplicitWidth = 984
    ExplicitHeight = 306
    inherited cxGrid1: TcxGrid
      Width = 980
      Height = 302
      ExplicitWidth = 980
      ExplicitHeight = 302
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmAlways
        DataController.DataSource = dm.dsVodokorisnici
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxSmallImages
          Properties.Items = <
            item
              Description = #1040#1082#1090#1080#1074#1077#1085
              ImageIndex = 79
              Value = 1
            end
            item
              Description = #1053#1077#1072#1082#1090#1080#1074#1077#1085
              ImageIndex = 77
              Value = 0
            end>
        end
        object cxGrid1DBTableView1STATUS_OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS_OPIS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView1PRAVNO_LICE: TcxGridDBColumn
          DataBinding.FieldName = 'PRAVNO_LICE'
          Width = 69
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TATKO: TcxGridDBColumn
          DataBinding.FieldName = 'TATKO'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1EMB: TcxGridDBColumn
          DataBinding.FieldName = 'EMB'
          Width = 100
        end
        object cxGrid1DBTableView1DANOCEN: TcxGridDBColumn
          DataBinding.FieldName = 'DANOCEN'
        end
        object cxGrid1DBTableView1POVRSINA: TcxGridDBColumn
          DataBinding.FieldName = 'POVRSINA'
          Width = 82
        end
        object cxGrid1DBTableView1LK: TcxGridDBColumn
          DataBinding.FieldName = 'LK'
          Width = 78
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 157
        end
        object cxGrid1DBTableView1ADRESA_2: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA_2'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 183
        end
        object cxGrid1DBTableView1TEL: TcxGridDBColumn
          DataBinding.FieldName = 'TEL'
          Width = 100
        end
        object cxGrid1DBTableView1FAX: TcxGridDBColumn
          DataBinding.FieldName = 'FAX'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 432
    Width = 984
    Height = 254
    ExplicitTop = 432
    ExplicitWidth = 984
    ExplicitHeight = 254
    inherited Label1: TLabel
      Left = 26
      Top = 69
      ExplicitLeft = 26
      ExplicitTop = 69
    end
    object lblnaziv: TLabel [1]
      Left = 26
      Top = 93
      Width = 74
      Height = 13
      Caption = #1053#1072#1079#1080#1074' ('#1080#1084#1077') :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 558
      Top = 139
      Width = 43
      Height = 13
      Caption = #1052#1077#1089#1090#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel [3]
      Left = 300
      Top = 115
      Width = 55
      Height = 13
      Caption = #1055#1088#1077#1079#1080#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel [4]
      Left = 26
      Top = 115
      Width = 79
      Height = 13
      Caption = #1058#1072#1090#1082#1086#1074#1086' '#1080#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [5]
      Left = 574
      Top = 162
      Width = 27
      Height = 13
      Caption = #1058#1077#1083' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [6]
      Left = 800
      Top = 162
      Width = 35
      Height = 13
      Caption = #1060#1072#1082#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [7]
      Left = 326
      Top = 157
      Width = 29
      Height = 13
      Caption = #1045#1052#1041' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel [8]
      Left = 527
      Top = 92
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = #1040#1076#1088#1077#1089#1072' ('#1083#1082'):'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel [9]
      Left = 560
      Top = 69
      Width = 41
      Height = 13
      Caption = #1051#1050' '#1073#1088'. :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel [10]
      Left = 306
      Top = 69
      Width = 82
      Height = 13
      Caption = #1057#1090#1072#1088#1072' '#1096#1080#1092#1088#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [11]
      Left = 551
      Top = 115
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = #1040#1076#1088#1077#1089#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel [12]
      Left = 297
      Top = 179
      Width = 58
      Height = 13
      Caption = #1044#1072#1085#1086#1095#1077#1085' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl1: TLabel [13]
      Left = 527
      Top = 23
      Width = 75
      Height = 26
      Caption = #1055#1086#1074#1088#1096#1080#1085#1072' '#1085#1072' '#1079#1077#1084#1112#1080#1096#1090#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Left = 111
      Top = 89
      DataBinding.DataField = 'IME'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      ExplicitLeft = 111
      ExplicitTop = 89
      ExplicitWidth = 400
      Width = 400
    end
    inherited OtkaziButton: TcxButton
      Left = 882
      Top = 215
      TabOrder = 18
      ExplicitLeft = 882
      ExplicitTop = 215
    end
    inherited ZapisiButton: TcxButton
      Left = 801
      Top = 215
      TabOrder = 17
      ExplicitLeft = 801
      ExplicitTop = 215
    end
    object txtID: TcxDBTextEdit
      Left = 111
      Top = 66
      TabStop = False
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsVodokorisnici
      Properties.ReadOnly = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object txtPrezime: TcxDBTextEdit
      Left = 361
      Top = 112
      DataBinding.DataField = 'PREZIME'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object txtTatko: TcxDBTextEdit
      Left = 111
      Top = 112
      DataBinding.DataField = 'TATKO'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object txtTel: TcxDBTextEdit
      Left = 606
      Top = 159
      DataBinding.DataField = 'TEL'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 14
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object txtEMB: TcxDBTextEdit
      Left = 361
      Top = 154
      DataBinding.DataField = 'EMB'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object rgPravnoLice: TcxDBRadioGroup
      Left = 111
      Top = 138
      TabStop = False
      Caption = #1058#1080#1087' '#1042#1086#1076#1086#1082#1086#1088#1080#1089#1085#1080#1082
      DataBinding.DataField = 'PRAVNO_LICE'
      DataBinding.DataSource = dm.dsVodokorisnici
      Properties.Items = <
        item
          Caption = #1055#1088#1072#1074#1085#1086' '#1083#1080#1094#1077
          Value = 1
        end
        item
          Caption = #1060#1080#1079#1080#1095#1082#1086' '#1083#1080#1094#1077
          Value = 0
        end>
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      Height = 62
      Width = 152
    end
    object txtMesto: TcxDBTextEdit
      Left = 606
      Top = 136
      BeepOnEnter = False
      DataBinding.DataField = 'MESTO'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 12
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 47
    end
    object cmbMesto: TcxDBLookupComboBox
      Left = 659
      Top = 136
      BeepOnEnter = False
      DataBinding.DataField = 'MESTO'
      DataBinding.DataSource = dm.dsVodokorisnici
      ParentFont = False
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListOptions.AnsiSort = True
      Properties.ListOptions.RowSelect = False
      Properties.ListSource = dmMat.dsMesto
      Style.Color = clWindow
      TabOrder = 13
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 299
    end
    object txtFax: TcxDBTextEdit
      Left = 837
      Top = 159
      DataBinding.DataField = 'FAX'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 15
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object txtAdresa: TcxDBTextEdit
      Left = 606
      Top = 89
      DataBinding.DataField = 'ADRESA'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 10
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 352
    end
    object txtLK: TcxDBTextEdit
      Left = 607
      Top = 66
      DataBinding.DataField = 'LK'
      DataBinding.DataSource = dm.dsVodokorisnici
      Properties.CharCase = ecUpperCase
      TabOrder = 9
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object txtOldID: TcxDBTextEdit
      Left = 394
      Top = 66
      TabStop = False
      BeepOnEnter = False
      DataBinding.DataField = 'OLD_ID'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 16
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 117
    end
    object txtAdresa2: TcxDBTextEdit
      Left = 606
      Top = 112
      DataBinding.DataField = 'ADRESA_2'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 11
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 352
    end
    object txtDanocen: TcxDBTextEdit
      Left = 361
      Top = 176
      DataBinding.DataField = 'DANOCEN'
      DataBinding.DataSource = dm.dsVodokorisnici
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object cxGroupBoxStatus: TcxGroupBox
      Left = 111
      Top = 4
      Caption = #1057#1090#1072#1090#1091#1089
      TabOrder = 0
      Height = 56
      Width = 400
      object cxDBCheckBox1: TcxDBCheckBox
        Left = 20
        Top = 24
        Caption = #1040#1082#1090#1080#1074#1077#1085
        DataBinding.DataField = 'STATUS'
        DataBinding.DataSource = dm.dsVodokorisnici
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        TabOrder = 0
        Width = 67
      end
      object STATUS_OPIS: TcxDBTextEdit
        Left = 93
        Top = 23
        DataBinding.DataField = 'STATUS_OPIS'
        DataBinding.DataSource = dm.dsVodokorisnici
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 284
      end
    end
    object cxPOVRSINA: TcxDBTextEdit
      Left = 607
      Top = 26
      DataBinding.DataField = 'POVRSINA'
      DataBinding.DataSource = dm.dsVodokorisnici
      Properties.CharCase = ecUpperCase
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 984
    ExplicitWidth = 984
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 686
    Width = 984
    ExplicitTop = 686
    ExplicitWidth = 984
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 256
    Top = 248
  end
  inherited PopupMenu1: TPopupMenu
    Left = 352
    Top = 280
  end
  inherited dxBarManager1: TdxBarManager
    Left = 584
    Top = 160
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 245
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 565
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 1018
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 280
    object aKarticaFinansiska: TAction
      Caption = #1060#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1082#1072#1088#1090#1080#1094#1072
      ImageIndex = 64
      ShortCut = 121
      OnExecute = aKarticaFinansiskaExecute
    end
    object aKarticaSiteGodini: TAction
      Caption = #1060#1080#1085#1072#1085#1089#1080#1082#1072' '#1082#1072#1088#1090#1080#1094#1072' '#1079#1072' '#1089#1080#1090#1077' '#1075#1086#1076#1080#1085#1080
      ImageIndex = 80
      ShortCut = 122
      OnExecute = aKarticaSiteGodiniExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 42788.417250046300000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 784
    Top = 56
    PixelsPerInch = 96
  end
end
