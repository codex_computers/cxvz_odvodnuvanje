object frmStariTuzbi: TfrmStariTuzbi
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1057#1090#1072#1088#1080' '#1090#1091#1078#1073#1080
  ClientHeight = 682
  ClientWidth = 984
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 984
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          Caption = #1055#1086#1075#1083#1077#1076
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 659
    Width = 984
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object GridPanel1: TGridPanel
    Left = 0
    Top = 126
    Width = 984
    Height = 533
    Align = alClient
    ColumnCollection = <
      item
        Value = 49.998601714424640000
      end
      item
        SizeStyle = ssAbsolute
        Value = 40.000000000000000000
      end
      item
        Value = 50.001398285575360000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = cxGrid1
        Row = 0
        RowSpan = 3
      end
      item
        Column = 2
        Control = cxGrid2
        Row = 1
      end
      item
        Column = 2
        Control = pnlNadDug
        Row = 0
      end
      item
        Column = 2
        Control = pnlPodDug
        Row = 2
      end>
    RowCollection = <
      item
        SizeStyle = ssAbsolute
        Value = 50.000000000000000000
      end
      item
        Value = 100.000000000000000000
      end
      item
        SizeStyle = ssAbsolute
        Value = 80.000000000000000000
      end>
    TabOrder = 2
    ExplicitTop = 120
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 470
      Height = 531
      Align = alClient
      TabOrder = 0
      RootLevelOptions.DetailTabsPosition = dtpTop
      ExplicitWidth = 440
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1080' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077'...'
        DataController.DataSource = dsStari
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            FieldName = 'VODOKORISNIK_NAZIV'
            Column = cxGrid1DBTableView1VODOKORISNIK_NAZIV
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1ODVODNUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'ODVODNUVANJE'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1VODOKORISNIK_ID: TcxGridDBColumn
          DataBinding.FieldName = 'VODOKORISNIK_ID'
          Options.Editing = False
        end
        object cxGrid1DBTableView1VODOKORISNIK_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VODOKORISNIK_NAZIV'
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1FAKTURA_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'FAKTURA_BROJ'
          Options.Editing = False
          Width = 101
        end
        object cxGrid1DBTableView1SEZONA: TcxGridDBColumn
          DataBinding.FieldName = 'SEZONA'
          Options.Editing = False
        end
        object cxGrid1DBTableView1DOLG: TcxGridDBColumn
          DataBinding.FieldName = 'DOLG'
          Options.Editing = False
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Options.Editing = False
          Width = 80
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1NOTAR: TcxGridDBColumn
          DataBinding.FieldName = 'NOTAR'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1NPN: TcxGridDBColumn
          DataBinding.FieldName = 'NPN'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1IZVRSITEL: TcxGridDBColumn
          DataBinding.FieldName = 'IZVRSITEL'
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1IBROJ: TcxGridDBColumn
          DataBinding.FieldName = 'IBROJ'
          Options.Editing = False
          Width = 100
        end
      end
      object cxGrid1Level1: TcxGridLevel
        Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1057#1058#1040#1056#1048' '#1058#1059#1046#1041#1048
        GridView = cxGrid1DBTableView1
      end
    end
    object cxGrid2: TcxGrid
      Left = 511
      Top = 51
      Width = 472
      Height = 401
      Align = alClient
      TabOrder = 1
      RootLevelOptions.DetailTabsPosition = dtpTop
      ExplicitLeft = 541
      ExplicitWidth = 442
      ExplicitHeight = 431
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid2DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1080' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077'...'
        DataController.DataSource = dsDUG
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
            FieldName = 'O_IZNOS_DOLG'
            Column = cxGrid2DBTableView1O_IZNOS_DOLG
          end
          item
            Kind = skCount
            FieldName = 'VODOKORISNIK_NAZIV'
            Column = cxGrid2DBTableView1VODOKORISNIK_NAZIV
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CheckBoxPosition = cbpIndicator
        OptionsSelection.CheckBoxVisibility = [cbvDataRow]
        OptionsView.Footer = True
        Styles.OnGetContentStyle = cxGrid2DBTableView1StylesGetContentStyle
        object cxGrid2DBTableView1RE_OUT: TcxGridDBColumn
          DataBinding.FieldName = 'RE_OUT'
          Visible = False
        end
        object cxGrid2DBTableView1TUZENA: TcxGridDBColumn
          DataBinding.FieldName = 'TUZENA'
          Visible = False
          Width = 24
        end
        object cxGrid2DBTableView1ODVODNUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'ODVODNUVANJE'
          Visible = False
        end
        object cxGrid2DBTableView1VODOKORISNIK_ID: TcxGridDBColumn
          DataBinding.FieldName = 'VODOKORISNIK_ID'
        end
        object cxGrid2DBTableView1VODOKORISNIK_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VODOKORISNIK_NAZIV'
          Width = 150
        end
        object cxGrid2DBTableView1EMB: TcxGridDBColumn
          DataBinding.FieldName = 'EMB'
          Visible = False
          Width = 90
        end
        object cxGrid2DBTableView1DANOCEN: TcxGridDBColumn
          DataBinding.FieldName = 'DANOCEN'
          Visible = False
          Width = 84
        end
        object cxGrid2DBTableView1EMBEDB: TcxGridDBColumn
          DataBinding.FieldName = 'EMBEDB'
          Width = 100
        end
        object cxGrid2DBTableView1TEL: TcxGridDBColumn
          DataBinding.FieldName = 'TEL'
          Width = 100
        end
        object cxGrid2DBTableView1LK: TcxGridDBColumn
          DataBinding.FieldName = 'LK'
          Visible = False
          Width = 64
        end
        object cxGrid2DBTableView1PRAVNO_LICE: TcxGridDBColumn
          DataBinding.FieldName = 'PRAVNO_LICE'
          Visible = False
        end
        object cxGrid2DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Visible = False
        end
        object cxGrid2DBTableView1DATUM_F: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_F'
          Visible = False
        end
        object cxGrid2DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 75
        end
        object cxGrid2DBTableView1FAKTURA_GODINA_ORIGINAL: TcxGridDBColumn
          DataBinding.FieldName = 'FAKTURA_GODINA_ORIGINAL'
        end
        object cxGrid2DBTableView1O_IZNOS_DOLG: TcxGridDBColumn
          DataBinding.FieldName = 'O_IZNOS_DOLG'
          HeaderAlignmentHorz = taRightJustify
          Width = 81
        end
        object cxGrid2DBTableView1TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          Visible = False
        end
        object cxGrid2DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 100
        end
        object cxGrid2DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 100
        end
        object cxGrid2DBTableView1OPSTINA_OZNAKA: TcxGridDBColumn
          DataBinding.FieldName = 'OPSTINA_OZNAKA'
          Visible = False
        end
        object cxGrid2DBTableView1OPSTINA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OPSTINA_NAZIV'
          Width = 100
        end
        object cxGrid2DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 100
        end
        object cxGrid2DBTableView1OPIS_FAKTURA: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS_FAKTURA'
          Width = 100
        end
        object cxGrid2DBTableView1STARA_SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'STARA_SIFRA'
        end
      end
      object cxGrid2Level1: TcxGridLevel
        Caption = #1044#1054#1051#1043#1054#1042#1048
        GridView = cxGrid2DBTableView1
      end
    end
    object pnlNadDug: TPanel
      Left = 511
      Top = 1
      Width = 472
      Height = 50
      Align = alClient
      Color = 15790320
      ParentBackground = False
      TabOrder = 2
      ExplicitLeft = 541
      ExplicitWidth = 442
      object Label3: TLabel
        Left = -9
        Top = 27
        Width = 78
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' '#1076#1086':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 4
        Top = 5
        Width = 65
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1077#1079#1086#1085#1072':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Datum_do: TcxDateEdit
        Left = 75
        Top = 24
        TabOrder = 2
        Width = 102
      end
      object cxCheckBox1: TcxCheckBox
        Left = 157
        Top = 2
        TabStop = False
        Caption = #1055#1088#1077#1090#1093#1086#1076#1085#1080' '#1075#1086#1076#1080#1085#1080
        State = cbsChecked
        TabOrder = 1
        Transparent = True
        Width = 119
      end
      object sGodina: TcxSpinEdit
        Tag = 1
        Left = 75
        Top = 2
        TabOrder = 0
        Width = 67
      end
      object btnPokazi: TcxButton
        Left = 348
        Top = 12
        Width = 75
        Height = 25
        Action = aPokazi
        TabOrder = 3
      end
    end
    object pnlPodDug: TPanel
      Left = 511
      Top = 452
      Width = 472
      Height = 80
      Align = alClient
      Color = 15790320
      ParentBackground = False
      TabOrder = 3
      ExplicitLeft = 541
      ExplicitTop = 482
      ExplicitWidth = 442
      ExplicitHeight = 50
      object Label2: TLabel
        Left = 21
        Top = 8
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblBroj: TLabel
        Left = 193
        Top = 10
        Width = 45
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 25
        Top = 31
        Width = 46
        Height = 12
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1086#1090#1072#1088':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 192
        Top = 33
        Width = 46
        Height = 12
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1055#1053':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 192
        Top = 55
        Width = 46
        Height = 12
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1041#1088#1086#1112':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 3
        Top = 55
        Width = 68
        Height = 12
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1048#1079#1074#1088#1096#1080#1090#1077#1083':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnOznaci: TcxButton
        Left = 376
        Top = 23
        Width = 79
        Height = 25
        Caption = #1047#1072#1087#1080#1096#1080
        TabOrder = 6
        OnClick = btnOznaciClick
      end
      object dateDatum: TcxDateEdit
        Left = 77
        Top = 5
        TabOrder = 0
        Width = 102
      end
      object txtBroj: TcxTextEdit
        Left = 244
        Top = 6
        Properties.MaxLength = 100
        TabOrder = 1
        Width = 102
      end
      object txtNotar: TcxTextEdit
        Left = 77
        Top = 28
        Properties.CharCase = ecUpperCase
        Properties.MaxLength = 100
        TabOrder = 2
        Width = 102
      end
      object txtNPN: TcxTextEdit
        Left = 244
        Top = 28
        Properties.MaxLength = 100
        TabOrder = 3
        Width = 102
      end
      object txtIzvrsitel: TcxTextEdit
        Left = 77
        Top = 51
        Properties.CharCase = ecUpperCase
        Properties.MaxLength = 100
        TabOrder = 4
        Width = 102
      end
      object txtIbroj: TcxTextEdit
        Left = 244
        Top = 50
        Properties.MaxLength = 100
        TabOrder = 5
        Width = 102
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 368
    Top = 472
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 636
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 428
      DockedTop = 0
      FloatLeft = 1018
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aSkrijDolgovi
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aIzramni
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aSkrijTuzbi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPokazi: TAction
      Caption = #1055#1088#1080#1082#1072#1078#1080
      ImageIndex = 22
      OnExecute = aPokaziExecute
    end
    object aZapisiSelektirani: TAction
      Caption = #1047#1072#1087#1080#1096#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080
      ImageIndex = 6
      OnExecute = aZapisiSelektiraniExecute
    end
    object aSkrijDolgovi: TAction
      Caption = #1058#1091#1078#1073#1080
      ImageIndex = 47
      OnExecute = aSkrijDolgoviExecute
    end
    object aSkrijTuzbi: TAction
      Caption = #1044#1086#1083#1075#1086#1074#1080
      ImageIndex = 48
      OnExecute = aSkrijTuzbiExecute
    end
    object aIzramni: TAction
      Caption = #1062#1077#1085#1090#1088#1080#1088#1072#1085#1086
      OnExecute = aIzramniExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 104
    Top = 488
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 448
    Top = 336
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dsStari: TDataSource
    DataSet = tblStari
    Left = 728
    Top = 16
  end
  object tblStari: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    TS.ID,'
      '    TS.RE,'
      '    TS.VODOKORISNIK_ID,'
      '    V.NAZIV VODOKORISNIK_NAZIV,'
      '    TS.FAKTURA_BROJ,'
      '    TS.SEZONA,'
      '    TS.DOLG,'
      '    TS.ODVODNUVANJE,'
      '    TS.DATUM,'
      '    TS.BROJ,'
      '    ts.NOTAR,'
      '    ts.NPN,'
      '    ts.IZVRSITEL,'
      '    ts.IBROJ,'
      '    TS.OPIS'
      'from VZ_TUZBA_STARA TS'
      'inner join VZ_VODOKORISNIK V on TS.VODOKORISNIK_ID = V.ID'
      'where (TS.RE = :RE)'
      '    and ts.odvodnuvanje=:odvodnuvanje'
      '--      and ((:APP = '#39'VZ'#39' and TS.ODVODNUVANJE = 0) or'
      '  --         (:APP = '#39'VZO'#39' and TS.ODVODNUVANJE = 1))')
    AutoUpdateOptions.UpdateTableName = 'VZ_TUZBA_STARA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.CanChangeSQLs = True
    AutoUpdateOptions.GeneratorName = 'GEN_VZ_TUZBA_STARA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeDelete = tblStariBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 688
    Top = 16
    object tblStariID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
    end
    object tblStariRE: TFIBIntegerField
      DisplayLabel = #1056#1045
      FieldName = 'RE'
    end
    object tblStariVODOKORISNIK_ID: TFIBBCDField
      DisplayLabel = #1042#1086#1076#1086#1082#1086#1088#1080#1089#1085#1080#1082' - '#1096#1080#1092'.'
      FieldName = 'VODOKORISNIK_ID'
      Size = 0
    end
    object tblStariVODOKORISNIK_NAZIV: TFIBStringField
      DisplayLabel = #1042#1086#1076#1086#1082#1086#1088#1080#1089#1085#1080#1082
      FieldName = 'VODOKORISNIK_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStariFAKTURA_BROJ: TFIBBCDField
      DisplayLabel = #1060#1072#1082#1090#1091#1088#1072' '#1073#1088#1086#1112
      FieldName = 'FAKTURA_BROJ'
      Size = 0
    end
    object tblStariSEZONA: TFIBIntegerField
      DisplayLabel = #1057#1077#1079#1086#1085#1072
      FieldName = 'SEZONA'
    end
    object tblStariDOLG: TFIBBCDField
      DisplayLabel = #1044#1086#1083#1075
      FieldName = 'DOLG'
      Size = 2
    end
    object tblStariOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStariODVODNUVANJE: TFIBSmallIntField
      DisplayLabel = #1054#1076#1074#1086#1076#1085#1091#1074#1072#1114#1077
      FieldName = 'ODVODNUVANJE'
    end
    object tblStariDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblStariBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStariNOTAR: TFIBStringField
      DisplayLabel = #1053#1086#1090#1072#1088
      FieldName = 'NOTAR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStariNPN: TFIBStringField
      DisplayLabel = #1053#1055#1053
      FieldName = 'NPN'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStariIZVRSITEL: TFIBStringField
      DisplayLabel = #1048#1079#1074#1088#1096#1080#1090#1077#1083
      FieldName = 'IZVRSITEL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStariIBROJ: TFIBStringField
      DisplayLabel = #1048#1041#1088#1086#1112
      FieldName = 'IBROJ'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDUG: TDataSource
    DataSet = tblDUG
    Left = 728
    Top = 72
  end
  object tblDUG: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '    P.RE_OUT,'
      '    p.ODVODNUVANJE,'
      '    P.VODOKORISNIK_ID,'
      '    P.VODOKORISNIK_NAZIV,'
      '    P.EMB,'
      '    P.DANOCEN,'
      '    P.LK,'
      '    P.PRAVNO_LICE,'
      '    P.FAKTURA_GODINA_ORIGINAL,'
      '    P.DATUM_F,'
      '    P.GODINA,'
      '    P.BROJ,'
      '    P.TIP,'
      '    P.ADRESA,'
      '    P.MESTO_NAZIV,'
      '    P.OPSTINA_OZNAKA,'
      '    P.OPSTINA_NAZIV,'
      '    P.O_IZNOS_DOLG,'
      '    P.OPIS,'
      '    P.OPIS_FAKTURA,'
      '    P.STARA_SIFRA,'
      '    P.EMBEDB,'
      '    p.TUZENA,'
      '    P.TEL'
      ''
      
        'from PROC_VZ_NEOTUZEN_DOLG(:APP, :RE, :SEZONA, :PRETHODNI_GOD, :' +
        'DATUM_ZAKLUCNO) P'
      '--left join vz_tuzba_stara st on p.broj = st.faktura_broj'
      '--where st.faktura_broj is null')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 688
    Top = 72
    object tblDUGRE_OUT: TFIBIntegerField
      FieldName = 'RE_OUT'
    end
    object tblDUGVODOKORISNIK_ID: TFIBBCDField
      DisplayLabel = #1042#1086#1076#1086#1082#1086#1088#1080#1089#1085#1080#1082' '#1096#1080#1092'.'
      FieldName = 'VODOKORISNIK_ID'
      Size = 0
    end
    object tblDUGVODOKORISNIK_NAZIV: TFIBStringField
      DisplayLabel = #1042#1086#1076#1086#1082#1086#1088#1080#1089#1085#1080#1082
      FieldName = 'VODOKORISNIK_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGEMB: TFIBStringField
      DisplayLabel = #1045#1052#1041
      FieldName = 'EMB'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGDANOCEN: TFIBStringField
      DisplayLabel = #1044#1072#1085#1086#1095#1077#1085
      FieldName = 'DANOCEN'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGEMBEDB: TFIBStringField
      DisplayLabel = #1045#1052#1041'/'#1045#1044#1041
      FieldName = 'EMBEDB'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGTEL: TFIBStringField
      DisplayLabel = #1058#1077#1083'.'
      FieldName = 'TEL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGLK: TFIBStringField
      DisplayLabel = #1051#1050
      FieldName = 'LK'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGPRAVNO_LICE: TFIBSmallIntField
      DisplayLabel = #1055#1088#1072#1074#1085#1086
      FieldName = 'PRAVNO_LICE'
    end
    object tblDUGFAKTURA_GODINA_ORIGINAL: TFIBIntegerField
      DisplayLabel = #1057#1077#1079#1086#1085#1072
      FieldName = 'FAKTURA_GODINA_ORIGINAL'
    end
    object tblDUGDATUM_F: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1092#1072#1082#1090'.'
      FieldName = 'DATUM_F'
    end
    object tblDUGGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblDUGBROJ: TFIBBCDField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1092#1072#1082#1090
      FieldName = 'BROJ'
      Size = 0
    end
    object tblDUGTIP: TFIBIntegerField
      FieldName = 'TIP'
    end
    object tblDUGADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGOPSTINA_OZNAKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090#1080#1085#1072
      FieldName = 'OPSTINA_OZNAKA'
    end
    object tblDUGOPSTINA_NAZIV: TFIBStringField
      DisplayLabel = #1054#1087#1096#1090#1080#1085#1072
      FieldName = 'OPSTINA_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGO_IZNOS_DOLG: TFIBBCDField
      DisplayLabel = #1044#1086#1083#1075
      FieldName = 'O_IZNOS_DOLG'
      Size = 2
    end
    object tblDUGOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGOPIS_FAKTURA: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089' '#1092#1072#1082#1090#1091#1088#1072
      FieldName = 'OPIS_FAKTURA'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDUGSTARA_SIFRA: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1088#1072' '#1096#1080#1092'.'
      FieldName = 'STARA_SIFRA'
    end
    object tblDUGODVODNUVANJE: TFIBIntegerField
      DisplayLabel = #1054#1076#1074#1086#1076#1085#1091#1074#1072#1114#1077
      FieldName = 'ODVODNUVANJE'
    end
    object tblDUGTUZENA: TFIBIntegerField
      DisplayLabel = #1058
      FieldName = 'TUZENA'
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 360
    Top = 408
  end
  object pTuzbaPostoi: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_VZ_TUZBA_POSTOI (?FAKTURA_BROJ)')
    StoredProcName = 'PROC_VZ_TUZBA_POSTOI'
    Left = 824
    Top = 48
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
