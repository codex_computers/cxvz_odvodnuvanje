unit Resenija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, cxNavigator,
  cxImageComboBox, Vcl.DBCtrls;

type
//  niza = Array[1..5] of Variant;

  TfrmResenija = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dPanel: TPanel;
    lPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_DOK: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_DOK_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DOK: TcxGridDBColumn;
    cxGrid1DBTableView1DDO: TcxGridDBColumn;
    cxGrid1DBTableView1DOK_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1VODOKORISNIK_ID: TcxGridDBColumn;
    cxGrid1DBTableView1PRAVNO_LICE: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1EMB: TcxGridDBColumn;
    cxGrid1DBTableView1DANOCEN: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_ZEMJISTE: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_ZEMJISTE_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1POVRSINA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1FOND_ZA_VODI: TcxGridDBColumn;
    cxGrid1DBTableView1DDV: TcxGridDBColumn;
    cxGrid1DBTableView1REF_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ARC_O_BR: TcxGridDBColumn;
    cxGrid1DBTableView1ARC_BR: TcxGridDBColumn;
    cxGrid1DBTableView1ARC_PODBR: TcxGridDBColumn;
    cxGrid1DBTableView1ARC_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DOSTAVA: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGroupBoxPodatociResenie: TcxGroupBox;
    DATUM_DOK: TcxDBDateEdit;
    Label2: TLabel;
    cxGroupBoxIznosPovrsina: TcxGroupBox;
    Label6: TLabel;
    edtPOVRSINA: TcxDBTextEdit;
    Label7: TLabel;
    CENA: TcxDBTextEdit;
    Label8: TLabel;
    FOND_ZA_VODI: TcxDBTextEdit;
    DDV: TcxDBTextEdit;
    Label9: TLabel;
    Label12: TLabel;
    IZNOS: TcxDBTextEdit;
    BROJ: TcxDBTextEdit;
    cxGroupBoxArhivskiBroj: TcxGroupBox;
    ARC_O_BR: TcxDBTextEdit;
    ARC_BROJ: TcxDBTextEdit;
    Label15: TLabel;
    Label14: TLabel;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    aArhivskiBroj: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aBroj: TAction;
    cxDBRadioGroupTipResenie: TcxDBRadioGroup;
    REF_ID_NAZIV: TcxDBLookupComboBox;
    REF_ID: TcxDBTextEdit;
    LabelRef: TLabel;
    cxGroupBoxUplataDostava: TcxGroupBox;
    Label11: TLabel;
    Label1: TLabel;
    DATUM_DOSTAVA: TcxDBDateEdit;
    DDO: TcxDBDateEdit;
    aREF: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aPResenieZadolzuvanje: TAction;
    aPResenieOsloboduvanje: TAction;
    aDResenieZadolzuvanje: TAction;
    aDResenieOsloboduvanje: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    PopupMenu2: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    PopUpMenu: TAction;
    cxGrid1DBTableView1GODINA_SEZONA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    pnlVK: TPanel;
    lbl1: TLabel;
    txtVK: TcxTextEdit;
    cmbVK: TcxExtLookupComboBox;
    btnPrikazi: TcxButton;
    actPrikazi: TAction;
    lbl2: TLabel;
    cxGroupBoxPodatociParcela: TcxGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    BROJ_PARCELA: TcxDBTextEdit;
    IMOTEN_LIST: TcxDBTextEdit;
    lbl5: TLabel;
    K_OPSTINA: TcxDBTextEdit;
    RadioDB_tipZemjiste: TcxDBRadioGroup;
    K_OPSTINA_NAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1IZNOS_VKUPEN: TcxGridDBColumn;
    cxGrid1DBTableView1IMOTEN_LIST: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_PARCELA: TcxGridDBColumn;
    cxGrid1DBTableView1K_OPSTINA: TcxGridDBColumn;
    cxGrid1DBTableView1KO_NAZIV: TcxGridDBColumn;
    actZemiCenaOdSifrarnik: TAction;
    cxGrid1DBTableViewPravnoFizicko: TcxGridDBColumn;
    GODINA_SEZONA: TcxDBComboBox;
    lbl6: TLabel;
    cxGrid1DBTableView1STATUS_VODOKORISNIK: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_OPIS: TcxGridDBColumn;
    actResenieZaOsloboduvanje: TAction;
    actZadolziOslobodi: TAction;
    dxbrlrgbtn1: TdxBarLargeButton;
    actResenieZaDoZadolzuvanje: TAction;
    actEnablePoResOslobodiZadolzi: TAction;
    dxbrlrgbtn2: TdxBarLargeButton;
    actPFinansiskaKartica: TAction;
    dxbrlrgbtn3: TdxBarLargeButton;
    actPFinansiskaKarticaSiteGodini: TAction;
    dxbrlrgbtn4: TdxBarLargeButton;
    actPOpomena: TAction;
    actDOpomena: TAction;
    actDOpomena1: TMenuItem;
    cxGrid1DBTableView1IZNOS_VKUPEN_ZAOKRUZEN: TcxGridDBColumn;
    aArhivskiDopolnitelniRes: TAction;
    cxGrid1DBTableView1IZNOS_NA_VODI: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_NA_DDV: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aArhivskiBrojExecute(Sender: TObject);
    procedure aBrojExecute(Sender: TObject);
    procedure dxBarLargeButton14Click(Sender: TObject);
    procedure cxDBRadioGroupTipReseniePropertiesChange(Sender: TObject);
    procedure aREFExecute(Sender: TObject);
    procedure aPResenieZadolzuvanjeExecute(Sender: TObject);
    procedure aPResenieOsloboduvanjeExecute(Sender: TObject);
    procedure aDResenieZadolzuvanjeExecute(Sender: TObject);
    procedure aDResenieOsloboduvanjeExecute(Sender: TObject);
    procedure PopUpMenuExecute(Sender: TObject);
    procedure actPrikaziExecute(Sender: TObject);
    procedure cmbVKPropertiesEditValueChanged(Sender: TObject);
    procedure actZemiCenaOdSifrarnikExecute(Sender: TObject);
    procedure actResenieZaOsloboduvanjeExecute(Sender: TObject);
    procedure actZadolziOslobodiExecute(tip_resenie: Integer);
    procedure actResenieZaDoZadolzuvanjeExecute(Sender: TObject);
    procedure actEnablePoResOslobodiZadolziExecute(Sender: TObject);
    procedure actPFinansiskaKarticaExecute(Sender: TObject);
    procedure actPFinansiskaKarticaSiteGodiniExecute(Sender: TObject);
    procedure RadioDB_tipZemjistePropertiesEditValueChanged(Sender: TObject);
    procedure actPOpomenaExecute(Sender: TObject);
    procedure actDOpomenaExecute(Sender: TObject);
    procedure aArhivskiDopolnitelniResExecute(Sender: TObject);

  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmResenija: TfrmResenija;
  rData : TRepositoryData;
  ARC_O_BR_STAR:string;
implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmResenija.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmResenija.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
     if (txtVK.Text<> '') and (cmbVK.Text <> '') then
       begin
         dPanel.Enabled:=True;
         lPanel.Enabled:=False;
         pnlVK.Enabled:=False;
         DATUM_DOK.SetFocus;
         cxGrid1DBTableView1.DataController.DataSet.Insert;

         dm.tblResenijaVODOKORISNIK_ID.Value:=txtVK.EditingValue;

         dm.qPovrsinaVodokorisnik.Close;
         dm.qPovrsinaVodokorisnik.ParamByName('vodokorisnik_id').Value:=dm.tblResenijaVODOKORISNIK_ID.Value;
         dm.qPovrsinaVodokorisnik.ExecQuery;
         if (not dm.qPovrsinaVodokorisnik.FieldByName('povrsina').IsNull) then
            begin
              dm.tblResenijaPOVRSINA.Value:=dm.qPovrsinaVodokorisnik.FieldByName('povrsina').Value;
              IMOTEN_LIST.SetFocus;
            end;


         dm.qArhivskiBrPredlog.Close;
         dm.qArhivskiBrPredlog.ParamByName('godina').Value:=dm.godina;
         dm.qArhivskiBrPredlog.ParamByName('re').Value:=dmkon.firma_id;
         dm.qArhivskiBrPredlog.ExecQuery;
         if (not dm.qArhivskiBrPredlog.FldByName['arc_o_br'].IsNull) then
            dm.tblResenijaARC_O_BR.Value:=dm.qArhivskiBrPredlog.FldByName['arc_o_br'].Value;

         aArhivskiBroj.Execute;

         dm.tblResenijaTIP_DOK.Value:=11;
         dm.tblResenijaTIP_ZEMJISTE.Value:=1;
         actZemiCenaOdSifrarnik.Execute;
         dm.tblResenijaDATUM_DOK.Value:=Now;
         dm.tblResenijaGODINA_SEZONA.Value:=dm.godina;
         aBroj.Execute();
       end
     else ShowMessage('������ �������� ������������ !');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmResenija.aArhivskiBrojExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) and (ARC_O_BR_STAR <> dm.tblResenijaARC_O_BR.Value)) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
     if ((not dm.tblResenijaARC_O_BR.IsNull) and (dm.tblResenijaARC_O_BR.Value<>'')) then
        begin
          dm.qArhivskiBr.Close;
          dm.qArhivskiBr.ParamByName('arc_o_br').Value:=dm.tblResenijaARC_O_BR.Value;
          dm.qArhivskiBr.ExecQuery;
          if (not dm.qArhivskiBr.FldByName['arc_br'].IsNull) then
            begin
              dm.tblResenijaARC_BR.Value:=dm.qArhivskiBr.FldByName['arc_br'].Value;
              dm.tblResenijaARC_BROJ.Value:=dm.tblResenijaARC_O_BR.Value +'-'+intToStr(dm.qArhivskiBr.FldByName['arc_br'].Value);

              if (not dm.tblResenijaVODOKORISNIK_ID.IsNull) then
               begin
                  dm.qArhivskiPodBr.Close;
                  dm.qArhivskiPodBr.ParamByName('arc_o_br').Value:=dm.tblResenijaARC_O_BR.Value;
                  dm.qArhivskiPodBr.ParamByName('arc_br').Value:=dm.tblResenijaARC_BR.Value;
                  dm.qArhivskiPodBr.ParamByName('vodokorisnik_id').Value:=dm.tblResenijaVODOKORISNIK_ID.Value;
                  dm.qArhivskiPodBr.ExecQuery;
                  if (not dm.qArhivskiPodBr.FldByName['arc_podbr'].IsNull) then
                     begin
                       dm.tblResenijaARC_PODBR.Value:=dm.qArhivskiPodBr.FldByName['arc_podbr'].Value+1;
                       dm.tblResenijaARC_BROJ.Value:=dm.tblResenijaARC_BROJ.Value +'/'+intToStr(dm.qArhivskiPodBr.FldByName['arc_podbr'].Value+1);
                      end;
                end;

            end;
        end;
    end;
end;

procedure TfrmResenija.aArhivskiDopolnitelniResExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) and (ARC_O_BR_STAR <> dm.tblResenijaARC_O_BR.Value)) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
     if ((not dm.tblResenijaARC_O_BR.IsNull) and (dm.tblResenijaARC_O_BR.Value<>'')) then
        begin
          if (not dm.tblResenijaARC_BR.IsNull) then
              dm.tblResenijaARC_BROJ.Value:=dm.tblResenijaARC_O_BR.Value +'-'+intToStr(dm.tblResenijaARC_BR.Value);
              if (not dm.tblResenijaVODOKORISNIK_ID.IsNull) then
               begin
                  dm.qArhivskiPodBrDopolnitelniRes.Close;
                  dm.qArhivskiPodBrDopolnitelniRes.ParamByName('arc_o_br_br').Value:=dm.tblResenijaARC_BROJ.Value;
                  dm.qArhivskiPodBrDopolnitelniRes.ParamByName('vodokorisnik_id').Value:=dm.tblResenijaVODOKORISNIK_ID.Value;
                  dm.qArhivskiPodBrDopolnitelniRes.ExecQuery;
                  if (not dm.qArhivskiPodBrDopolnitelniRes.FldByName['arc_podbr'].IsNull) then
                     begin
                       dm.tblResenijaARC_PODBR.Value:=dm.qArhivskiPodBrDopolnitelniRes.FldByName['arc_podbr'].Value+1;
                       dm.tblResenijaARC_BROJ.Value:=dm.tblResenijaARC_BROJ.Value +'/'+intToStr(dm.qArhivskiPodBrDopolnitelniRes.FldByName['arc_podbr'].Value+1);
                      end;
                end;
        end;
    end;
end;

procedure TfrmResenija.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    pnlVK.Enabled:=False;
    DATUM_DOK.SetFocus;
    ARC_O_BR_STAR:=dm.tblResenijaARC_O_BR.Value;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmResenija.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmResenija.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
 // brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmResenija.aREFExecute(Sender: TObject);
begin
      if dm.tblResenijaTIP_DOK.Value = 13then
         begin
            REF_ID_NAZIV.Enabled:=True;
            REF_ID.Enabled:=True;

            REF_ID_NAZIV.Tag:=1;
            REF_ID.Tag:=1;
            dm.tblResenieZadolzuvanjeRef.Close;
            dm.tblResenieZadolzuvanjeRef.ParamByName('vodokorisnik_id').Value:=dm.tblResenijaVODOKORISNIK_ID.Value;
            dm.tblResenieZadolzuvanjeRef.Open;
            aBroj.Execute();
         end
      else
         begin
            REF_ID.Clear;
            aBroj.Execute();
            REF_ID_NAZIV.Enabled:=False;
            REF_ID.Enabled:=False;
            REF_ID_NAZIV.Tag:=0;
            REF_ID.Tag:=0;
         end;
end;

procedure TfrmResenija.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;


//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmResenija.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmResenija.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmResenija.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmResenija.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          if ((kom = cmbVK) or (kom = txtVK))  then
            begin
              frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
              frmNurkoRepository.kontrola_naziv := 'cmbVK';//kom.Name;
              frmNurkoRepository.ShowModal;

              if (frmNurkoRepository.ModalResult = mrOk) then
              begin
                cmbVK.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
                btnPrikazi.SetFocus;
              end;
              frmNurkoRepository.Free;
          end;
      	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmResenija.cxDBRadioGroupTipReseniePropertiesChange(
  Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
      aREF.Execute();
    end;
end;

procedure TfrmResenija.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmResenija.cxDBTextEditAllExit(Sender: TObject);
var
  kom : TWinControl;
begin
  TEdit(Sender).Color:=clWhite;

    kom := Sender as TWinControl;
    if ((kom = txtVK) and (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse)) then
      begin
         if ((txtVK.Text <> '')) then
            cmbVK.EditValue:=txtVK.Text
         else
            cmbVK.Clear;
      end;

   if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
     if Sender = txtVK then
       begin
         aArhivskiBroj.Execute();
         aREF.Execute();
       end
     else if Sender = cmbVK then
       begin
         aArhivskiBroj.Execute();
         aREF.Execute();
       end
     else if Sender = ARC_O_BR then aArhivskiBroj.Execute

     else  if Sender = DATUM_DOSTAVA then
           if not dm.tblResenijaDATUM_DOSTAVA.IsNull then
              dm.tblResenijaDDO.Value:= dm.tblResenijaDATUM_DOSTAVA.Value + 60;
    end;
end;


procedure TfrmResenija.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmResenija.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmResenija.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmResenija.PopUpMenuExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500);
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmResenija.prefrli;
begin
end;

procedure TfrmResenija.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmResenija.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmResenija.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    dm.tblResenija.Close;
    dm.tblResenija.ParamByName('v_pom').Value:=0;
    dm.tblResenija.Open;

    dm.tblResenieZadolzuvanjeRef.Close;
    //dm.tblResenieZadolzuvanjeRef.ParamByName('vodokorisnik_id').Value:=dm.tblResenijaVODOKORISNIK_ID.Value;
    dm.tblResenieZadolzuvanjeRef.Open;

    //cmbVK.RepositoryItem := dmRes.InitRepository(-901, 'cmbVK', Name, rData);
    //txtVK.SetFocus;

    cmbVK.RepositoryItem := dmRes.InitRepository(-901, 'cmbVK', Name, rData);
    txtVK.SetFocus;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmResenija.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmResenija.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmResenija.dxBarLargeButton14Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmResenija.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  dolg:Double;
begin
  ZapisiButton.SetFocus;
//
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      REF_ID_NAZIV.Enabled:=False;
      REF_ID.Enabled:=False;
      REF_ID_NAZIV.Tag:=0;
      REF_ID.Tag:=0;
      cxDBRadioGroupTipResenie.Enabled:=True;

      cxGrid1DBTableView1.DataController.DataSet.Post;

      // raspredeli avans ako postoi
      dolg:=dm.return6(dm.pPROC_VZ_ODV_UPLATA_OD_AVANSI, 'RESENIE_ID', 'DOLG_V', null, null, null, null, dm.tblResenijaID.Value, dm.tblResenijaIZNOS_VKUPEN.Value, null, null, null, null,'DOLG_O');

      if (dolg < dm.tblResenijaIZNOS_VKUPEN.Value)then
          ShowMessage('����������� ����� �� �����. ������� ���� � '+ FloatToStr(dolg));

      dPanel.Enabled:=false;
      lPanel.Enabled:=true;
      pnlVK.Enabled:=true;
      ARC_O_BR.TabStop:=True;
      actEnablePoResOslobodiZadolzi.Execute;
      cxGrid1.SetFocus;
    end;
  end;
end;

procedure TfrmResenija.cmbVKPropertiesEditValueChanged(Sender: TObject);
begin
 if cmbVK.Text <> '' then
	begin
//    txtVK.EditValue := cmbVK.EditValue;
		txtVK.Text := cmbVK.EditValue;
	end
	else
	begin
//    txtVK.EditValue:='';
		txtVK.Text := '';
 	end;
end;

//	����� �� ���������� �� �������
procedure TfrmResenija.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      pnlVK.Enabled:=true;
      ARC_O_BR.TabStop:=True;
      cxGrid1.SetFocus;

      REF_ID_NAZIV.Enabled:=False;
      REF_ID.Enabled:=False;
      REF_ID_NAZIV.Tag:=0;
      REF_ID.Tag:=0;
      cxDBRadioGroupTipResenie.Enabled:=True;
      actEnablePoResOslobodiZadolzi.Execute;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmResenija.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmResenija.aPResenieOsloboduvanjeExecute(Sender: TObject);
begin
     try
        dmRes.Spremi('VZO',2);
        dmKon.tblSqlReport.ParamByName('resenie_id').Value:=dm.tblResenijaID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenija.aPResenieZadolzuvanjeExecute(Sender: TObject);
begin
     try
        dmRes.Spremi('VZO',1);
        dmKon.tblSqlReport.ParamByName('resenie_id').Value:=dm.tblResenijaID.Value;
//        dmKon.tblSqlReport.ParamByName('re').Value:=dm.tblResenijaRE.Value;
//        dmKon.tblSqlReport.ParamByName('sezona_godina').Value:=dm.godina;
//        dmKon.tblSqlReport.ParamByName('datum_do').Value:=Now;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenija.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmResenija.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmResenija.aSnimiPecatenjeExecute(Sender: TObject);
begin
 zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmResenija.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmResenija.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmResenija.aBrojExecute(Sender: TObject);
begin
     dm.tblResenijaBROJ.Value:=dm.return6(dm.pBroj,'RE', 'TIP_DOK', 'GODINA', null, null, null,dmKon.firma_id, dm.tblResenijaTIP_DOK.Value, dm.godina, null, null, null,'BROJ');
end;

procedure TfrmResenija.actDOpomenaExecute(Sender: TObject);
begin
     dmRes.Spremi('VZO',5);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenija.actEnablePoResOslobodiZadolziExecute(Sender: TObject);
begin
          cxDBRadioGroupTipResenie.Enabled:=True;
          GODINA_SEZONA.Enabled:=true;
          RadioDB_tipZemjiste.Enabled:=true;
          cxGroupBoxPodatociParcela.Enabled:=true;
end;

procedure TfrmResenija.actPOpomenaExecute(Sender: TObject);
begin
  if ((txtVK.Text<>'') and (cmbVK.Text<>'')) then
  begin
    try
      dmRes.Spremi('VZO',5);
      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;

      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;

  end
  else
  begin
    ShowMessage('�������� ������������');
    txtVK.SetFocus;
  end;
end;

procedure TfrmResenija.actPFinansiskaKarticaExecute(Sender: TObject);
begin
  if ((txtVK.Text<>'') and (cmbVK.Text<>'')) then
  begin
    try
      dmRes.Spremi('VZO',3);
      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;

      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;

  end
  else
  begin
    ShowMessage('�������� ������������');
    txtVK.SetFocus;
  end;
end;

procedure TfrmResenija.actPFinansiskaKarticaSiteGodiniExecute(Sender: TObject);
begin
if ((txtVK.Text<>'') and (cmbVK.Text<>'')) then
  begin
    try
      dmRes.Spremi('VZO',4);
      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;

      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;

  end
  else
  begin
    ShowMessage('�������� ������������');
    txtVK.SetFocus;
  end;
end;


procedure TfrmResenija.actPrikaziExecute(Sender: TObject);
begin
  dm.tblResenija.Close;
  if ((txtVK.Text <> '') and (cmbVK.Text <> '')) then
    begin
      dm.tblResenija.ParamByName('v_pom').Value:=1;
      dm.tblResenija.ParamByName('v_id').Value:=txtVK.EditValue;
    end
  else dm.tblResenija.ParamByName('v_pom').Value:=0;
  dm.tblResenija.Open;

  cxGrid1.SetFocus;
end;



procedure TfrmResenija.actResenieZaDoZadolzuvanjeExecute(Sender: TObject);
begin
         actZadolziOslobodiExecute(12);
end;

procedure TfrmResenija.actResenieZaOsloboduvanjeExecute(Sender: TObject);
begin
       actZadolziOslobodiExecute(13);
end;

procedure TfrmResenija.actZadolziOslobodiExecute(tip_resenie: Integer);
 var vodokorisnik_id_v,  ddv, fond_za_vodi,resenie_id,godinasezona,tip_zemjiste,k_opstina, arc_br:Integer;
    povrsina, cena:Double;
    imoten_list,broj_parcela, arc_o_broj:string;
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
     if dm.tblResenijaTIP_DOK.Value=11 then
        begin
          dPanel.Enabled:=True;
          lPanel.Enabled:=False;
          pnlVK.Enabled:=False;

          resenie_id:=dm.tblResenijaID.Value;
          vodokorisnik_id_v:=dm.tblResenijaVODOKORISNIK_ID.Value;
          godinasezona:=dm.tblResenijaGODINA_SEZONA.Value;
          tip_zemjiste:=dm.tblResenijaTIP_ZEMJISTE.Value;
          povrsina:=dm.tblResenijaPOVRSINA.Value;
          cena:=dm.tblResenijaCENA.Value;
          fond_za_vodi:=dm.tblResenijaFOND_ZA_VODI.Value;
          ddv:=dm.tblResenijaDDV.Value;
          imoten_list:=dm.tblResenijaIMOTEN_LIST.Value;
          broj_parcela:=dm.tblResenijaBROJ_PARCELA.Value;
          k_opstina:=dm.tblResenijaK_OPSTINA.Value;
          arc_o_broj:=dm.tblResenijaARC_O_BR.Value;
          arc_br:=dm.tblResenijaARC_BR.Value;

          cxGrid1DBTableView1.DataController.DataSet.Insert;

          dm.tblResenijaVODOKORISNIK_ID.Value:=vodokorisnik_id_v;
          dm.tblResenijaGODINA_SEZONA.Value:=godinasezona;
          dm.tblResenijaDATUM_DOK.Value:=Now;
          dm.tblResenijaTIP_ZEMJISTE.Value:=tip_zemjiste;
          dm.tblResenijaPOVRSINA.Value:=povrsina;
          dm.tblResenijaCENA.Value:=cena;
          dm.tblResenijaFOND_ZA_VODI.Value:=fond_za_vodi;
          dm.tblResenijaDDV.Value:=ddv;
          dm.tblResenijaIMOTEN_LIST.Value:=imoten_list;
          dm.tblResenijaBROJ_PARCELA.Value:=broj_parcela;
          if not  dm.tblResenijaK_OPSTINA.IsNull then
             dm.tblResenijaK_OPSTINA.Value:=k_opstina;
          dm.tblResenijaTIP_DOK.Value:=tip_resenie;
          dm.tblResenijaREF_ID.Value:=resenie_id;
          {dm.qArhivskiBrPredlog.Close;
          dm.qArhivskiBrPredlog.ParamByName('godina').Value:=dm.godina;
          dm.qArhivskiBrPredlog.ParamByName('re').Value:=dmkon.firma_id;
          dm.qArhivskiBrPredlog.ExecQuery;
          if (not dm.qArhivskiBrPredlog.FldByName['arc_o_br'].IsNull) then
              dm.tblResenijaARC_O_BR.Value:=dm.qArhivskiBrPredlog.FldByName['arc_o_br'].Value;  }
          dm.tblResenijaARC_O_BR.Value:=arc_o_broj;
          dm.tblResenijaARC_BR.Value:=arc_br;
          aBroj.Execute;
          aArhivskiDopolnitelniRes.Execute;
          //aArhivskiBroj.Execute;

          REF_ID_NAZIV.Enabled:=False;
          REF_ID.Enabled:=False;
          cxDBRadioGroupTipResenie.Enabled:=False;
          GODINA_SEZONA.Enabled:=false;
          RadioDB_tipZemjiste.Enabled:=false;
          cxGroupBoxPodatociParcela.Enabled:=false;
          cxDBRadioGroupTipResenie.Enabled:=False;

          edtPOVRSINA.setFocus;
          ARC_O_BR.TabStop:=False;
       end
     else ShowMessage('������ �������� ������� �� ����������� !!!')
  end
 else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');

end;

procedure TfrmResenija.actZemiCenaOdSifrarnikExecute(Sender: TObject);
begin
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
      if (not dm.tblResenijaTIP_ZEMJISTE.IsNull) then
         begin
          dm.qCenovnik.Close;
          dm.qCenovnik.ParambyName('godina').Value:=dm.godina;
          dm.qCenovnik.ParambyName('re').Value:=dmKon.firma_id;
          dm.qCenovnik.ParambyName('tip_zemjiste').Value:=dm.tblResenijaTIP_ZEMJISTE.Value;
          dm.qCenovnik.ExecQuery;

          if (not dm.qCenovnik.FldByName['cena_bez_ddv'].IsNull) then
             dm.tblResenijaCENA.Value:= dm.qCenovnik.FldByName['cena_bez_ddv'].Value;
          if (not dm.qCenovnik.FldByName['ddv'].IsNull) then
             dm.tblResenijaDDV.Value:= dm.qCenovnik.FldByName['ddv'].Value;
          if (not dm.qCenovnik.FldByName['fond_za_vodi'].IsNull) then
             dm.tblResenijaFOND_ZA_VODI.Value:= dm.qCenovnik.FldByName['fond_za_vodi'].Value;
         end;
    end;
end;

procedure TfrmResenija.aDResenieOsloboduvanjeExecute(Sender: TObject);
begin
     dmRes.Spremi('VZO',2);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenija.aDResenieZadolzuvanjeExecute(Sender: TObject);
begin
     dmRes.Spremi('VZO',1);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmResenija.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmResenija.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmResenija.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


procedure TfrmResenija.RadioDB_tipZemjistePropertiesEditValueChanged(
  Sender: TObject);
begin
     actZemiCenaOdSifrarnik.Execute;
end;

end.
