unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  dxRibbonSkins, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinOffice2013White, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxSkinscxPCPainter,
  System.ImageList, cxImageList, cxDBLookupComboBox, dxBarExtDBItems, cxCheckBox;

type
  TfrmMain = class(TForm)
    dxRibbon1TabMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    Image2: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    lblDatumVreme: TcxLabel;
    PanelMain: TPanel;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    aFormConfig: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    aPromeniLozinka: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    aLogout: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    aSifCenovnik: TAction;
    dxBarManager1Bar4: TdxBar;
    dxBarLookupComboGodina: TdxBarLookupCombo;
    dxRibbon1TabPreglediReporter: TdxRibbonTab;
    dxBarLargeButton9: TdxBarLargeButton;
    aResenija: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    actFakturi: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    actPocetnaSostojba: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton12: TdxBarLargeButton;
    actVodokorisnici: TAction;
    actSifKatastarskaOpstina: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    actMesto: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    actUplati: TAction;
    dxbrlrgbtn1: TdxBarLargeButton;
    actDolznici: TAction;
    dxbrlrgbtn2: TdxBarLargeButton;
    actEvidencijaStariTuzbi: TAction;
    dxbrlrgbtn3: TdxBarLargeButton;
    actSifPartneri: TAction;
    dxbrlrgbtn4: TdxBarLargeButton;
    aSaldiranje: TAction;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPromeniLozinkaExecute(Sender: TObject);
    procedure aLogoutExecute(Sender: TObject);
    procedure aSifCenovnikExecute(Sender: TObject);
    procedure dxBarLookupComboGodinaKeyValueChange(Sender: TObject);
    procedure aResenijaExecute(Sender: TObject);
    procedure actFakturiExecute(Sender: TObject);
    procedure actPocetnaSostojbaExecute(Sender: TObject);
    procedure actVodokorisniciExecute(Sender: TObject);
    procedure actSifKatastarskaOpstinaExecute(Sender: TObject);
    procedure actMestoExecute(Sender: TObject);
    procedure actUplatiExecute(Sender: TObject);
    procedure actDolzniciExecute(Sender: TObject);
    procedure actEvidencijaStariTuzbiExecute(Sender: TObject);
    procedure PanelMainClick(Sender: TObject);
    procedure actSifPartneriExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, PromeniLozinka, Cenovnik, dmUnit, Godini, MK, Resenija, PocetnaSostojba, KatasterskiOpstini, Vodokorisnik, Mesto, Uplata, Dolznici, StariTuzbi, Partner;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.actDolzniciExecute(Sender: TObject);
begin
  frmDolznici:= TfrmDolznici.Create(Self, True);
  frmDolznici.ShowModal;
  frmDolznici.Free;
end;

procedure TfrmMain.actEvidencijaStariTuzbiExecute(Sender: TObject);
begin
  frmStariTuzbi := TfrmStariTuzbi.Create(Self, True);
  frmStariTuzbi.ShowModal;
  frmStariTuzbi.Free;
end;

procedure TfrmMain.actFakturiExecute(Sender: TObject);
begin
  frmCenovnik := TfrmCenovnik.Create(Self, True);
  frmCenovnik.ShowModal;
  frmCenovnik.Free;
end;

procedure TfrmMain.actMestoExecute(Sender: TObject);
begin
    frmMesto := TfrmMesto.Create(Self, True);
    frmMesto.ShowModal;
    frmMesto.Free;
end;

procedure TfrmMain.actPocetnaSostojbaExecute(Sender: TObject);
begin
    frmPocetnaSostojba := TfrmPocetnaSostojba.Create(Self, True);
    frmPocetnaSostojba.ShowModal;
    frmPocetnaSostojba.Free;
end;

procedure TfrmMain.actSifKatastarskaOpstinaExecute(Sender: TObject);
begin
    frmKatasterskiOpstini := TfrmKatasterskiOpstini.Create(Self, True);
    frmKatasterskiOpstini.ShowModal;
    frmKatasterskiOpstini.Free;
end;

procedure TfrmMain.actSifPartneriExecute(Sender: TObject);
begin
    frmPartner:= TfrmPartner.Create(Self, True);
    frmPartner.ShowModal;
    frmPartner.Free;
end;

procedure TfrmMain.actUplatiExecute(Sender: TObject);
begin
    frmUplata := TfrmUplata.Create(Self, True);
    frmUplata.ShowModal;
    frmUplata.Free;
end;

procedure TfrmMain.actVodokorisniciExecute(Sender: TObject);
begin
    frmVodokorisnik := TfrmVodokorisnik.Create(Self, True);
    frmVodokorisnik.ShowModal;
    frmVodokorisnik.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aLogoutExecute(Sender: TObject);
begin
  if (dmKon.Logout = false) then Close
  else
  begin
    SpremiForma(self);

    if dmKon.user = 'SYSDBA' then
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : �������������'
    else
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : ' + dmKon.imeprezime; // ������� �� ���������� ��������

    dm.tblGodini.Close;
    dm.tblGodini.ParamByName('o').Value := 1;
    dm.tblGodini.Open;

    OtvoriTabeli();
  end;
end;

procedure TfrmMain.aPromeniLozinkaExecute(Sender: TObject);
begin
  frmPromeniLozinka:=TfrmPromeniLozinka.Create(nil);
  frmPromeniLozinka.ShowModal;
  frmPromeniLozinka.Free;
end;

procedure TfrmMain.aResenijaExecute(Sender: TObject);
begin
    frmResenija := TfrmResenija.Create(Self, True);
    frmResenija.ShowModal;
    frmResenija.Free;
end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aSifCenovnikExecute(Sender: TObject);
begin
  frmCenovnik := TfrmCenovnik.Create(Self, True);
  frmCenovnik.ShowModal;
  frmCenovnik.Free;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex ����� ��������
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.dxBarLookupComboGodinaKeyValueChange(Sender: TObject);
var
  status: TStatusWindowHandle;
begin
  status := cxCreateStatusWindow();
  try
    dm.godina := dm.tblGodiniGODINA.Value;
    dm.zatvorena := dm.tblGodiniZATVORENA.Value;
    dm.otvorena := dm.tblGodiniOTVORENA.Value;
    dm.arhivirana := dm.tblGodiniARHIVIRANA.Value;
  finally
    cxRemoveStatusWindow(status);
  end;

end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��

  dm.tblGodini.Close;
  dm.tblGodini.ParamByName('o').Value := 1;
  dm.tblGodini.Open;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);

  if (dm.tblGodiniGODINA.Value <> 0) then
  begin
    dxBarLookupComboGodina.KeyValue := dm.tblGodiniGODINA.Value;
    dm.godina := dm.tblGodiniGODINA.Value;
  end
  else
  begin
    ShowMessage('���� ���������� ������ �� ��������� �����!');
    frmGodini:= TfrmGodini.Create(self);
    frmGodini.ShowModal;
    frmGodini.Free;

    if (dm.tblGodiniGODINA.Value <> 0) then
    begin
      dxBarLookupComboGodina.KeyValue := dm.tblGodiniGODINA.Value;
      dm.godina := dm.tblGodiniGODINA.Value;
    end;
  end;


  frmMK := TfrmMK.Create(nil);
  frmMK.ShowModal;
  frmMK.Free;

  dxRibbon1TabMeni.Active := true;

  Caption := '����������� : ' + IntToStr(dm.godina) + '-' + dmKon.firma_naziv;

  dm.tblSetup.ParamByName('p2').Value:='tip_banka';
  dm.tblSetup.Open;
  if not dm.tblSetupV1.IsNull then
     dm.tip_banka:=StrToInt(dm.tblSetupV1.value)
  else
     dm.tip_banka:=0;

  OtvoriTabeli;

  if (dmKon.reporter_dll) then
  begin
    dmRes.ReporterInit;
		dmRes.GenerateReportsRibbon(dxRibbon1,dxRibbon1TabPreglediReporter,dxBarManager1);
  end;

end;

procedure TfrmMain.OtvoriTabeli;
begin
  // ������ �� ���������� dataset-���

  dm.tblVodokorisnici.Open;
  dmMat.tblNurko.Open;
  dmMat.tblMesto.Open;
  dmMat.tblDrzava.Open;
  dmMat.tblRegioni.Open;
  dmMat.tblOpstina.Open;
  dm.tblSifKatastarskaOpstina.Open;
  dm.tblBanki.ParamByName('tip_partner').Value:=dm.tip_banka;
  dm.tblBanki.Open;
  dm.tblKasaIO.ParamByName('godina').Value:=dm.godina;
  dm.tblKasaIO.ParamByName('re').Value:=dmKon.re;
  dm.tblKasaIO.Open();
end;

procedure TfrmMain.PanelMainClick(Sender: TObject);
begin

end;

//TODO


end.
