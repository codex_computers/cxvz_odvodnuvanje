unit Uplata;

interface

uses
  DateUtils,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, System.Actions,
  cxSplitter, dxBarBuiltInMenu, cxNavigator, frxClass, frxDBSet, cxImageComboBox,
  cxLabel, cxDBLabel, dxCore, cxDateUtils;

type
//  niza = Array[1..5] of Variant;

  TfrmUplata = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    pnlVK: TPanel;
    btnPrikazi: TcxButton;
    aPrikaziZaVodokorisnik: TAction;
    cxSplitter1: TcxSplitter;
    pnlPodnozje: TPanel;
    cmbVK: TcxExtLookupComboBox;
    txtVK: TcxTextEdit;
    Label13: TLabel;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    aZapisiStavka: TAction;
    aOtkaziStavka: TAction;
    aEscape: TAction;
    aPecatiPocetna: TAction;
    aPecatiPocetnaDizajn: TAction;
    aResetNurkovci: TAction;
    pnlTipUplata: TPanel;
    pnlGrid: TPanel;
    pnlDolg: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    Selekcija: TcxGridDBColumn;
    Redosled: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    pnlSite: TPanel;
    checkSite: TcxCheckBox;
    pnlDolgLevo: TPanel;
    pnlDolgDesno: TPanel;
    txtUplata: TcxTextEdit;
    Label4: TLabel;
    Label2: TLabel;
    pageTipUplata: TcxPageControl;
    tabBankov: TcxTabSheet;
    tabKasa: TcxTabSheet;
    tabOstanato: TcxTabSheet;
    cmbBanka: TcxLookupComboBox;
    dateBankov: TcxDateEdit;
    txtBankovBroj: TcxTextEdit;
    Label8: TLabel;
    Label1: TLabel;
    Label9: TLabel;
    cmbKasa: TcxLookupComboBox;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    dateKasa: TcxDateEdit;
    txtKasaBroj: TcxTextEdit;
    dateOstanat: TcxDateEdit;
    txtOstanatBroj: TcxTextEdit;
    Label7: TLabel;
    Label10: TLabel;
    txtBanka: TcxTextEdit;
    txtKasa: TcxTextEdit;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    txtSelektiran: TcxTextEdit;
    txtOstanat: TcxTextEdit;
    txtAvansNov: TcxTextEdit;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    aUplati: TAction;
    btnUplati: TcxButton;
    cxSplitter2: TcxSplitter;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2Level2: TcxGridLevel;
    cxGrid2DBTableView2: TcxGridDBTableView;
    Label11: TLabel;
    memoBankovOpis: TcxMemo;
    memoKasaOpis: TcxMemo;
    lbl2: TLabel;
    lbl1: TLabel;
    memoOstanatOpis: TcxMemo;
    txtAvansStarZbir: TcxTextEdit;
    aPopupDIZAJN: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    aKarticaFinansika: TAction;
    aKarticaSiteGodini: TAction;
    aKarticaFinansiskaDIZAJN: TAction;
    aKarticaSiteGodiniDIZAJN: TAction;
    popupDizajn: TPopupMenu;
    aKarticaFinansiskaDIZAJN1: TMenuItem;
    aKarticaSiteGodiniDIZAJN1: TMenuItem;
    dxBarLargeButton22: TdxBarLargeButton;
    aUplatiLista: TAction;
    dxBarLargeButton23: TdxBarLargeButton;
    aAvansiLista: TAction;
    cxGrid1DBTableView1RES_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1RES_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1RES_TIP: TcxGridDBColumn;
    cxGrid1DBTableView1RES_IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1UPLATA_IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1DOLG: TcxGridDBColumn;
    cxGrid1DBTableView1RES_ID: TcxGridDBColumn;
    cxGrid1DBTableView1RES_ODZADOLZI_IZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1RE: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1TIP: TcxGridDBColumn;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1VODOKORISNIK_ID: TcxGridDBColumn;
    cxGrid2DBTableView1FAKTURA_BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1BANKOV_IZVOD_BR: TcxGridDBColumn;
    cxGrid2DBTableView1BANKA_TIP: TcxGridDBColumn;
    cxGrid2DBTableView1BANKA_ID: TcxGridDBColumn;
    cxGrid2DBTableView1KASA_PRIMI_BR: TcxGridDBColumn;
    cxGrid2DBTableView1KASA_RE: TcxGridDBColumn;
    cxGrid2DBTableView1KASA_SMETKA: TcxGridDBColumn;
    cxGrid2DBTableView1DOKUMENT_BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1NALOG_TIP: TcxGridDBColumn;
    cxGrid2DBTableView1NALOG_DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1NALOG: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid2DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1UPLATA_ID: TcxGridDBColumn;
    cxGrid2DBTableView1BANKA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView2UPLATA_ID: TcxGridDBColumn;
    cxGrid2DBTableView2DATUM: TcxGridDBColumn;
    cxGrid2DBTableView2TIPBROJ: TcxGridDBColumn;
    cxGrid2DBTableView2UPLATENIZNOS: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aPrikaziZaVodokorisnikExecute(Sender: TObject);
    procedure cmbVKPropertiesEditValueChanged(Sender: TObject);
    procedure aNovUpdate(Sender: TObject);
    procedure aZapisiStavkaExecute(Sender: TObject);
    procedure aOtkaziStavkaExecute(Sender: TObject);
    procedure aEscapeExecute(Sender: TObject);
    //procedure pEnableDisableTabs(enable :boolean);
    procedure aBrisiUpdate(Sender: TObject);


    procedure aPecatiPocetnaExecute(Sender: TObject);
    procedure aPecatiPocetnaDizajnExecute(Sender: TObject);
    procedure aResetNurkovciExecute(Sender: TObject);
    procedure aAzurirajUpdate(Sender: TObject);
    procedure cmbBankaPropertiesEditValueChanged(Sender: TObject);
    procedure cmbKasaPropertiesEditValueChanged(Sender: TObject);
    procedure pageTipUplataPageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure checkSitePropertiesEditValueChanged(Sender: TObject);
    procedure SelekcijaPropertiesEditValueChanged(Sender: TObject);
    procedure UpdateRedosled(broj : integer);
    procedure aUplatiUpdate(Sender: TObject);
    procedure txtUplataPropertiesEditValueChanged(Sender: TObject);
    procedure btnUplatiClick(Sender: TObject);
    procedure txtUplataExit(Sender: TObject);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid2DBTableView2TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure aKarticaFinansikaExecute(Sender: TObject);
    procedure aKarticaSiteGodiniExecute(Sender: TObject);
    procedure aKarticaFinansiskaDIZAJNExecute(Sender: TObject);
    procedure aKarticaSiteGodiniDIZAJNExecute(Sender: TObject);
    procedure aPopupDIZAJNExecute(Sender: TObject);
    procedure aUplatiListaExecute(Sender: TObject);
    procedure aAvansiListaExecute(Sender: TObject);



  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;
    procedure PresmetajZaNaplata;
    procedure UplataRaspredeli(mnozestvo: string; iznos:Double);  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmUplata: TfrmUplata;
  rData : TRepositoryData;
  vk_selekcija:Integer;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, dmMaticni, UplataNalog, ListaAvansi;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmUplata.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;


//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmUplata.aNovExecute(Sender: TObject);
begin
  if pageTipUplata.ActivePage = tabBankov then
  begin
    dateBankov.Clear;
    txtBanka.Clear;
    cmbBanka.Clear;
    txtBankovBroj.Clear;
    memoBankovOpis.Clear;
  end
  else
  begin
    if pageTipUplata.ActivePage = tabKasa then
    begin
      dateKasa.Clear;
      txtKasa.Clear;
      cmbKasa.Clear;
      txtKasaBroj.Clear;
      memoKasaOpis.Clear;
    end
    else if pageTipUplata.ActivePage = tabOstanato then
    begin
      dateOstanat.Clear;
      txtOstanatBroj.Clear;
      memoOstanatOpis.Clear;
    end;

  end;
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    pnlEdit.Enabled:=True;
//    pnlGrid.Enabled:=False;
//
//    txtGodina.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert; //dm.tbldognov
//    pnlVK.Enabled:=false;
//    dtDatum.Date:=date;
//
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');


 // pnlDogEdit.Enabled:=true;


//  dm.tblDogNov.Insert;
//  dm.tblDogNovBROJ.Value:=1;
//  dm.tblDogNovDATUM.Value:=date;
//  dm.tbldognov.Post;

//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

procedure TfrmUplata.aNovUpdate(Sender: TObject);
begin
//  aNov.Enabled:= (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse);//(dm.tblPocSostojba.State =dsBrowse);
//      and (dm.tblDogStavka.State =dsBrowse)
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmUplata.aAvansiListaExecute(Sender: TObject);
begin
 frmListaAvansi:= TfrmListaAvansi.Create(self,false);
 frmListaAvansi.ShowModal;
 frmListaAvansi.Free;
end;

procedure TfrmUplata.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
////    pnlDogEdit.Enabled:=True;
////    pnlDogGrid.Enabled:=False;
////
////    pnlNivi.Enabled:=false;
////    pnlStavki.Enabled:=false;
////
////    txtDogArhFIks.SetFocus;
//    pnlEdit.Enabled:=true;
//    pnlGrid.Enabled:=False;
//    pnlVK.Enabled:=false;
//
//    cxGrid1DBTableView1.DataController.DataSet.Edit; //dm.tbldognov
//    txtGodina.SetFocus;
//
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');

//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����

procedure TfrmUplata.aAzurirajUpdate(Sender: TObject);
begin
//  aAzuriraj.Enabled:= (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) //(dm.tblPocSostojba.State =dsBrowse)
//      and (cxGrid1DBTableView1.DataController.RecordCount >0);

end;

procedure TfrmUplata.aBrisiExecute(Sender: TObject);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� ������', '���� ��������� ������ �� �� ��������� �������� �� ����� '+dm.tblUplatiUPLATA_ID.AsString, 1);
    if (frmDaNe.ShowModal <> mrYes) then
    begin
       //nisto
    end
    else
    begin
      dm.pUplataBrisi.Close;
      dm.pUplataBrisi.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dm.pUplataBrisi.ParamByName('re').Value:=dmkon.re;
      dm.pUplataBrisi.ParamByName('uplata_id').Value:=dm.tblUplatiUPLATA_ID.Value;
      dm.pUplataBrisi.ExecProc;


      aPrikaziZaVodokorisnik.Execute;
    end;

//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.DataSet.RecordCount <> 0)) then
//        cxGrid2DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmUplata.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  brisiGridVoBaza(Name,cxGrid2DBTableView1);
  brisiGridVoBaza(Name,cxGrid2DBTableView2);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;


//	����� �� ���������� �� ����������
procedure TfrmUplata.aRefreshExecute(Sender: TObject);
begin
  aResetNurkovci.Execute;
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmUplata.aResetNurkovciExecute(Sender: TObject);
begin
  dmRes.FreeRepository(rData);
  rData := TRepositoryData.Create();

  // ������ �� ���� �������� �� �������
  cmbVK.RepositoryItem := dmRes.InitRepositoryRefresh(-901, 'cmbVK', Name, rData)
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmUplata.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmUplata.aKarticaFinansikaExecute(Sender: TObject);
begin
  if ((txtVK.Text<>'') and (cmbVK.Text<>'')) then
  begin
    try
      dmRes.Spremi('VZO',3);
      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;

      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;

  end
  else
  begin
    ShowMessage('�������� ������������');
    txtVK.SetFocus;
  end;
end;

procedure TfrmUplata.aKarticaFinansiskaDIZAJNExecute(Sender: TObject);
begin
     dmRes.Spremi('VZO',3);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmUplata.aKarticaSiteGodiniDIZAJNExecute(Sender: TObject);
begin
  //  try
//      dmRes.Spremi('VZ�',4);
//      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
//      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
//      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;
//
//      dmRes.frxReport1.DesignReport();
   // except
   //   ShowMessage('�� ���� �� �� ������� ���������!');
  //  end;

     dmRes.Spremi('VZO',4);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmUplata.aKarticaSiteGodiniExecute(Sender: TObject);
begin
 if ((txtVK.Text<>'') and (cmbVK.Text<>'')) then
  begin
    try
      dmRes.Spremi('VZO',4);
      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;

      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;

  end
  else
  begin
    ShowMessage('�������� ������������');
    txtVK.SetFocus;
  end;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmUplata.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  zacuvajGridVoBaza(Name,cxGrid2DBTableView2);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmUplata.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmUplata.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          if ((kom = txtBankovBroj) or (kom = txtKasaBroj)or (kom = memoOstanatOpis) )  then
          begin //�� �� �������� �� ������������ ��� �� �������
             PostMessage(Handle,WM_NextDlgCtl,0,0);
            // txtUplata.SetFocus;
          end
          else
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          if ((kom = cmbVK) or (kom = txtVK))  then
            begin
              frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
              frmNurkoRepository.kontrola_naziv := 'cmbVK';//kom.Name;
              frmNurkoRepository.ShowModal;

              if (frmNurkoRepository.ModalResult = mrOk) then
              begin
                cmbVK.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
                btnPrikazi.SetFocus;
              end;
              frmNurkoRepository.Free;
          end;


          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	      end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmUplata.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmUplata.cxDBTextEditAllExit(Sender: TObject);
var
  kom : TWinControl;
begin
  TEdit(Sender).Color:=clWhite;

    kom := Sender as TWinControl;
    if (kom = txtVK)  then
      begin
         if ((txtVK.Text <> '')) then
            begin
             cmbVK.EditValue:=txtVK.Text;
            end
            else
            begin
              cmbVK.Clear;
            end;
      end
      else
    if (kom = txtBanka)  then
      begin
         if ((txtBanka.Text <> '')) then
            begin
             cmbBanka.EditValue:=txtBanka.Text;
            end
            else
            begin
              cmbBanka.Clear;
            end;
      end
      else

    if (kom = txtKasa)  then
      begin
         if ((txtKasa.Text <> '')) then

            begin
             cmbKasa.EditValue:=txtKasa.Text;
            end
            else
            begin
              cmbKasa.Clear;
            end;
      end
      else
      if ((kom = dateBankov) or (kom = dateKasa) or (kom = dateOstanat))   then
      begin
         if (TcxDateEdit(kom).Text= '') then
         begin
           TcxDateEdit(kom).Date :=Date;
         end
         else if YearOf(TcxDateEdit(kom).Date)<>dm.godina then
         begin
            MessageError('������! ������� �� � �� ��������� ������!');
            (Sender as TcxDateEdit).SetFocus();
         end;

      end




end;



//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmUplata.SelekcijaPropertiesEditValueChanged(Sender: TObject);
  //���� �� ����� ������ � ������
var i, momentalen:Integer;
    iznos, avans_v, Avans_iznos_var,OstanatIznos_var,IznosUplata_var, IZNOS_AVANS_var, OstanatNovAvans_var :Double;
begin
     iznos:=0;
     if Selekcija.EditValue = true then
        begin
            vk_selekcija := vk_selekcija + 1;
            Redosled.EditValue:=vk_selekcija;
         end
     else if (selekcija.EditValue  = false) then
         begin
            vk_selekcija := vk_selekcija - 1;
            momentalen := Redosled.EditValue;
            Redosled.EditValue:= - 1;
            UpdateRedosled(momentalen);
         end;
     with cxGrid1DBTableView1.DataController do
     for I := 0 to FilteredRecordCount - 1 do
         begin
           if (GetValue(FilteredRecordIndex[i],Selekcija.Index) = true) then
              iznos:=iznos + GetValue(FilteredRecordIndex[i],cxGrid1DBTableView1DOLG.Index);
         end;

     PresmetajZaNaplata;


//     SelektiranIznos.EditValue:=FormatFloat('0.00 , .',iznos);
//     Avans_iznos_var:= Avans_iznos.EditValue;
//     IznosUplata_var:= IznosUplata.EditValue;
//     OstanatIznos_var:= Avans_iznos_var + IznosUplata_var -iznos;
//     OstanatIznos.EditValue:=FormatFloat('0.00 , .',(OstanatIznos_var));
//     IZNOS_AVANS_var:= dm.tblUplataStavkiIZNOS_AVANS.Value;
//     OstanatNovAvans_var:=OstanatIznos_var - IZNOS_AVANS_var;
//     OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',( OstanatNovAvans_var));
//     if OstanatNovAvans.EditValue < 0 then
//        OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',0);

end;

procedure TfrmUplata.UpdateRedosled(broj : integer);
var  oznaceno, kraj : bool;
begin
  // namali gi ostanatite brojki pogolemi od brojot
   cxGrid1DBTableView1.Controller.GoToFirst(true);

    if (cxGrid1DBTableView1.DataController.RecordCount =0) then kraj:=true
//    if dm.tblUplataStavki.IsEmpty then kraj := true
    else kraj := false;

    while (kraj = false) do
    begin
      if (Selekcija.EditValue <> null) and (Redosled.EditValue > broj) then
      begin
        Redosled.EditValue := Redosled.EditValue - 1;
      end;
      kraj := cxGrid1DBTableView1.Controller.IsFinish;
      cxGrid1DBTableView1.Controller.GoToNext(true,true);
    end;
// za da go vratime fokusot na kliknatoto
    cxGrid1DBTableView1.Controller.GoToFirst(true);

//    if dm.tblUplataStavki.IsEmpty then kraj := true
    if (cxGrid1DBTableView1.DataController.RecordCount =0) then kraj:=true
    else kraj := false;

    while (kraj = false) do
    begin
      if (Redosled.EditValue = -1) then
      begin
        Redosled.EditValue := Variants.Null;
        kraj := true;
      end;
      if kraj = false then
      begin
        kraj := cxGrid1DBTableView1.Controller.IsFinish;
        cxGrid1DBTableView1.Controller.GoToNext(true,true);
      end;
    end;
end;


procedure TfrmUplata.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;


procedure TfrmUplata.txtUplataExit(Sender: TObject);
begin
    PresmetajZaNaplata;
end;

procedure TfrmUplata.txtUplataPropertiesEditValueChanged(Sender: TObject);
begin
//  PresmetajZaNaplata;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmUplata.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmUplata.pageTipUplataPageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
  if NewPage=tabKasa then
  begin
    txtBanka.Tag:= 0;
    cmbBanka.Tag:=0;
    txtBankovBroj.Tag:= 0;
    dateBankov.Tag:=0;

    txtKasa.Tag:= 1;
    cmbKasa.Tag:= 1;
    txtKasaBroj.Tag:= 1;
    dateKasa.Tag:= 1;

    txtOstanatBroj.Tag:=0;
    dateOstanat.Tag:=0;

   // dateKasa.SetFocus;

  end
  else if NewPage = tabOstanato then
       begin
          txtBanka.Tag:= 0;
          cmbBanka.Tag:=0;
          txtBankovBroj.Tag:= 0;
          dateBankov.Tag:=0;

          txtKasa.Tag:= 0;
          cmbKasa.Tag:= 0;
          txtKasaBroj.Tag:= 0;
          dateKasa.Tag:= 0;

          txtOstanatBroj.Tag:=1;
          dateOstanat.Tag:=1;

       //   dateOstanat.SetFocus;
       end
       else if NewPage = tabBankov then
       begin
          txtBanka.Tag:= 1;
          cmbBanka.Tag:=1;
          txtBankovBroj.Tag:= 1;
          dateBankov.Tag:=1;

          txtKasa.Tag:= 0;
          cmbKasa.Tag:= 0;
          txtKasaBroj.Tag:=0;
          dateKasa.Tag:= 0;

          txtOstanatBroj.Tag:=0;
          dateOstanat.Tag:=0;

        //  dateBankov.SetFocus;
       end;
end;




//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmUplata.prefrli;
begin
end;

procedure TfrmUplata.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmUplata.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

 // dm.tblPocSostojba.Close; //ili tuka ili na form close - detail datasetovite da se proverat

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmUplata.FormShow(Sender: TObject);
begin

  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView2,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������

//    dm.tblPocSostojba.Close; //ili tuka ili na form close - detail datasetovite da se proverat
    dm.tblNeplateni.Close;   //ostanat od prethodno da go gase
    dm.tblUplati.Close;
    dm.tblAvansi.Close;
	  cmbVK.RepositoryItem := dmRes.InitRepository(-901, 'cmbVK', Name, rData);
//    txtVK.SetFocus;
  //  txtUplata.Properties. .fo FormatType = DevExpress.Utils.FormatType.Numeric;      ���

    vk_selekcija:=0; // nema selektirani u gridot
    pageTipUplata.ActivePage:= tabBankov; //po defolt da ode na bankov
    dateBankov.SetFocus;
    PresmetajZaNaplata;
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmUplata.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmUplata.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
 // if  then
 //ShowMessage(ACellViewInfo.Item.Caption);
  //TcxGridDBColumn(ACellViewInfo.Item).DataBinding.Field

end;

procedure TfrmUplata.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;



procedure TfrmUplata.cxGrid2DBTableView2TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
    if VarIsNull(AValue) then
    begin
      AText:= '0,00';
      txtAvansStarZbir.EditValue:=0;
      txtAvansStarZbir.Text:=FormatFloat('0.00,.',0);
    end
    else
    begin
        AText := FloatToStrF(AValue ,ffNumber,15,2); //  (Round(AValue));
        txtAvansStarZbir.EditValue:=AValue;
        txtAvansStarZbir.Text:=FormatFloat('0.00,.',AValue);
    end;


//    if VarIsNull(AValue) then AText:= '0,00'
//    else
//    begin
//        AText := FloatToStrF(cxRound(AValue,dm.tipRound) ,ffNumber,15,2); //  (Round(AValue));
//        dm.IznsoSmetka:=cxRound(AValue,dm.tipRound);
//    end;
//    lblIznosSmetka.Caption:=AText;

end;

//  ����� �� �����
procedure TfrmUplata.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if (Validacija(pnlTipUplata) = false) then
  begin
    ShowMessage('pomina');
  end;
//  ZapisiButton.SetFocus;
////
//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(pnlEdit) = false) then
//    begin
//
//      if (st = dsInsert) then
//      begin
////        dm.tblPocSostojbaBROJ.Value:=dm.SledenBrojZadolzuvanje(dmkon.re,0,dm.godina);
//        if dm.tblPocSostojbaBROJ.IsNull then  //da mu daam moznost da si pise
//        begin
//          dm.tblPocSostojbaBROJ.Value:=dm.SledenBrojZadolzuvanje(dm.tblPocSostojbaRE.Value, dm.tblPocSostojbaTIP.Value,dm.tblPocSostojbaGODINA.Value);
//        end;
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute; //ciklicno da gi vnese za eden covek
////        pnlEdit.Enabled:=false;
////        pnlGrid.Enabled:=true;
////        cxGrid1.SetFocus;  //ili da ode na nivite //ili na stavkite
////        pnlVK.Enabled:=true;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        pnlEdit.Enabled:=false;
//        pnlGrid.Enabled:=true;
//        cxGrid1.SetFocus;
//        pnlVK.Enabled:=true;
//      end;
//    end;
//  end;

//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

procedure TfrmUplata.aZapisiStavkaExecute(Sender: TObject);
var
  st: TDataSetState;
begin

//  ZapisiStavkaButton.SetFocus;
//  if not((dm.tblDogStavkaIZNOS.IsNull) or (dm.tblDogStavkaIZNOS.Value=0)) then
//  begin
//      st := cxGrid3DBTableView1.DataController.DataSet.State;
//      if st in [dsEdit,dsInsert] then
//      begin
//        if (Validacija(pnlStavkiEdit) = false) then
//        begin
//    //      if ((st = dsInsert) and inserting) then
//    //      begin
//    //        cxGrid3DBTableView1.DataController.DataSet.Post;
//    //        aNov.Execute;
//    //      end;
//
//    //      if ((st = dsInsert) and (not inserting)) then
//          if (st = dsInsert) then
//          begin
//            cxGrid3DBTableView1.DataController.DataSet.Post;
//            pnlStavkiEdit.Enabled:=false;
//            pnlDogovorNivi.Enabled:=true;
//            tabStavkiGrid.Enabled:=true;
//            pageStavki.ActivePage:=tabStavkiGrid;
//            cxGrid3.SetFocus;
//            pnlVK.Enabled:=true;
//          end;
//
//          if (st = dsEdit) then
//          begin
//            cxGrid3DBTableView1.DataController.DataSet.Post;
//            pnlStavkiEdit.Enabled:=false;
//            pnlDogovorNivi.Enabled:=true;
//            tabStavkiGrid.Enabled:=true;
//            pageStavki.ActivePage:=tabStavkiGrid;
//            cxGrid3.SetFocus;
//            pnlVK.Enabled:=true;
//          end;
//        end;
//      end;
//  end
//  else
//  begin
//    ShowMessage('������� ���� �� ����������� ������� �� ����������');
//  end;
//

end;

procedure TfrmUplata.UplataRaspredeli (mnozestvo:string; iznos:Double);
begin
  dm.pUplataRaspredeli.Close;
  dm.pUplataRaspredeli.Params.ClearValues;  //da gi ciste uplatite za drugo
  dm.pUplataRaspredeli.ParamByName('RE').Value:=dmKon.re;
  dm.pUplataRaspredeli.ParamByName('VODOKORISNIK_ID').Value:=txtVK.EditValue; //dm.tblNeplateni.ParamByName('VODOKORISNIK_ID').Value; //istiot od datasetot - txtVK.editvalue;
  dm.pUplataRaspredeli.ParamByName('ID_MNOZESTVO').Value:=mnozestvo;
  dm.pUplataRaspredeli.ParamByName('GODINA').Value:=dm.godina;
  dm.pUplataRaspredeli.ParamByName('IZNOS').Value:=iznos; //txtUplata.EditValue;

  if pageTipUplata.ActivePage = tabBankov then
  begin
      dm.pUplataRaspredeli.ParamByName('TIP').Value:=1; //bankov
      dm.pUplataRaspredeli.ParamByName('DATUM').Value:=dateBankov.EditValue; //bankov
      dm.pUplataRaspredeli.ParamByName('OPIS').Value:=memoBankovOpis.EditValue;

      dm.pUplataRaspredeli.ParamByName('BANKOV_IZVOD_BR').Value:=txtBankovBroj.EditValue;
      dm.pUplataRaspredeli.ParamByName('BANKA_TIP').Value:=dm.tip_banka;
      dm.pUplataRaspredeli.ParamByName('BANKA_ID').Value:=txtBanka.EditValue;
  end
  else if pageTipUplata.ActivePage = tabKasa then
       begin
         dm.pUplataRaspredeli.ParamByName('TIP').Value:=2; //kasa
         dm.pUplataRaspredeli.ParamByName('DATUM').Value:=dateKasa.EditValue; //kasa
         dm.pUplataRaspredeli.ParamByName('OPIS').Value:=memoKasaOpis.EditValue;

         dm.pUplataRaspredeli.ParamByName('KASA_PRIMI_BR').Value:=txtKasaBroj.EditValue;
         dm.pUplataRaspredeli.ParamByName('KASA_RE').Value:=dm.tblKasaIORE.Value;
         dm.pUplataRaspredeli.ParamByName('KASA_SMETKA').Value:=txtKasa.EditValue;
       end
  else //tabostanato
       begin
         dm.pUplataRaspredeli.ParamByName('DATUM').Value:=dateOstanat.EditValue; //ostanat
         dm.pUplataRaspredeli.ParamByName('OPIS').Value:=memoOstanatOpis.EditValue;
         dm.pUplataRaspredeli.ParamByName('TIP').Value:=3; //ostanati

         dm.pUplataRaspredeli.ParamByName('DOKUMENT_BROJ').Value:=txtOstanatBroj.EditValue;
       end;

  dm.pUplataRaspredeli.Prepare;
  dm.pUplataRaspredeli.ExecProc;


//
end;


procedure TfrmUplata.btnUplatiClick(Sender: TObject);
var id_mnozestvo:string;
   momentalen:Integer;
   uplata:Double;
   kraj:Boolean;
begin
    if (Validacija(pnlTipUplata)=false) then
    begin
        id_mnozestvo := '';
        momentalen := 1;
        while (momentalen <= vk_selekcija) and (vk_selekcija > 0) do
           begin
             cxGrid1DBTableView1.Controller.GoToFirst(true);
              if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then kraj :=False else kraj:= True;

//             if dm.tblUplata.IsEmpty then kraj := true
//             else kraj := false;
             while (kraj = false) do
                begin
                  if (Redosled.EditValue = momentalen) then
                    begin
                      if id_mnozestvo <> '' then
                      begin
                        id_mnozestvo:=id_mnozestvo+','+dm.tblNeplateniRES_BROJ.AsString;
                      // id_mnozestvo := id_mnozestvo + ',' + dm.tblUplataStavkiGODINA.AsString+dm.tblUplataStavkiFAKTURA_GODINA_ORIGINAL.AsString+'&'+dm.tblUplataStavkiBROJ.AsString
                      end
                      else
                      begin
                        id_mnozestvo:=dm.tblNeplateniRES_BROJ.AsString;
                       //  id_mnozestvo := dm.tblUplataStavkiGODINA.AsString+dm.tblUplataStavkiFAKTURA_GODINA_ORIGINAL.AsString+'&'+dm.tblUplataStavkiBROJ.AsString;
                      end;

                      momentalen := momentalen + 1;
                    end;
                  kraj := cxGrid1DBTableView1.Controller.IsFinish;
                  cxGrid1DBTableView1.Controller.GoToNext(true,true);
                end;
           end;


        //ShowMessage(id_mnozestvo);
       if StrToFloatEx(txtUplata.Text,uplata) then
       begin
          UplataRaspredeli(id_mnozestvo, uplata);
          txtUplata.EditValue:=0;
          //aPrikaziZaVodokorisnik.Execute;
          cmbVK.Clear;
          if pageTipUplata.ActivePage = tabBankov then
          begin
            memoBankovOpis.Clear;
            txtVK.SetFocus;
          end
          else
          begin
            if pageTipUplata.ActivePage = tabKasa then
            begin
              txtKasaBroj.Clear;
              cmbKasa.Clear;
              dateKasa.Clear;
              memoKasaOpis.Clear;
              dateKasa.SetFocus;
            end
            else //tabostanato
            begin
              txtOstanatBroj.Clear;
              memoOstanatOpis.Clear;
              dateOstanat.Clear;
              dateOstanat.SetFocus;
            end;
          end;

       end
       else
       begin
          ShowMessage('����� ������');
       end;


    end;
end;

procedure TfrmUplata.checkSitePropertiesEditValueChanged(Sender: TObject);
var i:Integer;
    status: TStatusWindowHandle;
    iznos, OstanatIznos_var, Avans_iznos_var, IznosUplata_var, OstanatNovAvans_var, IZNOS_AVANS_var:double;
begin
  if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
   begin
   status := cxCreateStatusWindow();
   iznos:=0;
   vk_selekcija:=0;
   try
   	if checkSite.Checked then
      begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , Selekcija.Index]:=  true;
               iznos:=iznos + GetValue(FilteredRecordIndex[i],cxGrid1DBTableView1DOLG.Index);
               vk_selekcija := vk_selekcija + 1;
               Values[FilteredRecordIndex[i] , Redosled.Index]:=  vk_selekcija;
            end;
          txtUplata.SetFocus;
      end
      else
      begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , Selekcija.Index]:= false;
               Values[FilteredRecordIndex[i] , Redosled.Index] := Variants.Null;
               iznos:=0;
            end;
          cxGrid1.SetFocus;
     end
      finally
	       cxRemoveStatusWindow(status);

      PresmetajZaNaplata;

//         SelektiranIznos.EditValue:=FormatFloat('0.00 , .',iznos);
//         Avans_iznos_var:=Avans_iznos.EditValue;
//         IznosUplata_var:=IznosUplata.EditValue;
//         OstanatIznos_var:= Avans_iznos_var+IznosUplata_var - iznos;
//         OstanatIznos.EditValue:=FormatFloat('0.00 , .',(OstanatIznos_var));
//         IZNOS_AVANS_var:= dm.tblUplataStavkiIZNOS_AVANS.Value;
//         OstanatNovAvans_var:= OstanatIznos_var - IZNOS_AVANS_var;
//         OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',(OstanatNovAvans_var));
//         if OstanatNovAvans.EditValue < 0 then
//            OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',0);
      end;
   end;
end;

procedure TfrmUplata.cmbBankaPropertiesEditValueChanged(Sender: TObject);
begin
  if cmbBanka.Text <> '' then
 	begin
//    txtVK.EditValue := cmbVK.EditValue;
		txtBanka.Text := cmbBanka.EditValue;
 	end
 	else
 	begin
//    txtVK.EditValue:='';
		txtBanka.Text := '';

 	end;

end;

procedure TfrmUplata.cmbKasaPropertiesEditValueChanged(Sender: TObject);
begin
  if cmbKasa.Text <> '' then
 	begin
//    txtVK.EditValue := cmbVK.EditValue;
		txtKasa.Text := cmbKasa.EditValue;
 	end
 	else
 	begin
//    txtVK.EditValue:='';
		txtKasa.Text := '';

 	end;
end;

procedure TfrmUplata.cmbVKPropertiesEditValueChanged(Sender: TObject);
begin
  if cmbVK.Text <> '' then
 	begin
//    txtVK.EditValue := cmbVK.EditValue;
		txtVK.Text := cmbVK.EditValue;
//    ShowMessage(cmbVK.EditValue);
//    ShowMessage(cmbVK.EditingValue);
 	end
 	else
 	begin
//    txtVK.EditValue:='';
		txtVK.Text := '';

 	end;
  dm.tblNeplateni.Close;
  dm.tblUplati.Close;
  dm.tblAvansi.Close;
  PresmetajZaNaplata;
end;

//	����� �� ���������� �� �������
procedure TfrmUplata.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(pnlEdit);
//      pnlEdit.Enabled := false;
//      pnlGrid.Enabled := true;
//      cxGrid1.SetFocus;
//      pnlVK.Enabled:=true;
//  end;

//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(pnlEdit);
//      pnlEdit.Enabled := false;
//      pnlGrid.Enabled:=True;
//      cxGrid1.SetFocus;
//      pnlVK.Enabled:=true;
end;

procedure TfrmUplata.aOtkaziStavkaExecute(Sender: TObject);
begin
//        cxGrid3DBTableView1.DataController.DataSet.Cancel;
//        RestoreControls(pnlStavkiEdit);
//        pnlStavkiEdit.Enabled:=false;
//        pnlDogovorNivi.Enabled:=true;
//        tabStavkiGrid.Enabled:=true;
//        pageStavki.ActivePage:=tabStavkiGrid;
//        cxGrid3.SetFocus;
//        pnlVK.Enabled:=true;

//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(pnlDogEdit);
//      pnlDogEdit.Enabled := false;
//      pnlDogGrid.Enabled:=True;
//      pnlStavki.Enabled:=true;
//      pnlNivi.Enabled := true;
//      cxGrid1.SetFocus;

end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmUplata.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmUplata.aPecatiPocetnaDizajnExecute(Sender: TObject);
begin
//    try
//      dmRes.Spremi('VZ',33001);
////      dmKon.tblSqlReport.ParamByName('id').Value:=dm.tblDogovoriID.Value;
//      dmRes.frxReport1.DesignReport();
//    except
//      ShowMessage('�� ���� �� �� ������� ���������!');
//    end;
end;

procedure TfrmUplata.aPecatiPocetnaExecute(Sender: TObject);
begin
//    if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then //dm.tblDogNov
//    begin
//        try
//          dmRes.Spremi('VZ',33001);
//          dmKon.tblSqlReport.ParamByName('id').Value:=dm.tblDogNovID.Value;
//          dmRes.frxReport1.ShowReport();
//         except
//            ShowMessage('�� ���� �� �� ������� ���������!');
//         end;
//    end;
end;

procedure TfrmUplata.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmUplata.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmUplata.aPopupDIZAJNExecute(Sender: TObject);
begin
  popupDizajn.Popup(500,500);
end;

procedure TfrmUplata.PresmetajZaNaplata();
var
//  iznosZaNaplata :Double;
//  iznosUplata:Double;
  v_selektiran, v_uplata, v_star_avans, v_nov_avans, v_ostanat:Double;
  i:integer;
begin
    if StrToFloatEx(txtUplata.Text,v_uplata) then
    begin
      v_selektiran:=0;
     //pretrci gi site selekritani stavki vo gridot i izesapi dolu avansi i ostanat dolg i za uplata
      with cxGrid1DBTableView1.DataController do
      for I := 0 to FilteredRecordCount - 1 do
      begin
        if (GetValue(FilteredRecordIndex[i],Selekcija.Index) = true) then
        begin
          v_selektiran:=v_selektiran + GetValue(FilteredRecordIndex[i],cxGrid1DBTableView1DOLG.Index);
        end;
      end;

      if not(StrToFloatEx(txtAvansStarZbir.Text,v_star_avans)) then v_star_avans:=0;
//      v_star_avans:=dm.tblNeplateniIZNOS_AVANS.AsFloat;
      v_ostanat:=v_selektiran-(v_star_avans+v_uplata);
      if v_ostanat<0 then
      begin
        v_nov_avans:= v_star_avans+v_uplata -v_selektiran;
        v_ostanat := 0;
      end
      else
      begin
        v_nov_avans:=0;
      end;
//      txtAvansStarZbir.Text:= FormatFloat('0.00,.',v_selektiran);
      txtSelektiran.Text:=  FormatFloat('0.00,.',v_selektiran);
      txtOstanat.Text:=  FormatFloat('0.00,.',v_ostanat);
      txtAvansNov.Text:=  FormatFloat('0.00,.',v_nov_avans);
      txtUplata.Text:=  FormatFloat('0.00,.',v_uplata);
    end
    else
    begin
      ShowMessage('����� ������');
      txtUplata.SetFocus;
    end;
end;


procedure TfrmUplata.aPrikaziZaVodokorisnikExecute(Sender: TObject);
begin
   //������ ���������� �� ��������������, ��������� ���� � �������� �� ����������

   if ((txtVK.Text <> '') and (cmbVK.Text <> '')) then
   begin
         //nedospeani fakturi da mu otvore - za stikliranje koi ke i placa
      //ako ima nedospeani - nova uplata da prae akcija = enabled
      dm.tblNeplateni.Close;
      dm.tblNeplateni.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dm.tblNeplateni.ParamByName('re').Value:=dmKon.re;
      dm.tblNeplateni.ParamByName('godina').Value:=dm.godina;
      dm.tblNeplateni.Open;

      dm.tblUplati.Close;
      dm.tblUplati.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dm.tblUplati.ParamByName('re').Value:=dmKon.re;
      dm.tblUplati.ParamByName('godina').Value:=dm.godina;
      dm.tblUplati.Open;

      dm.tblAvansi.Close;
      dm.tblAvansi.ParamByName('vodokorisnik_id').Value:=txtVK.EditValue;
      dm.tblAvansi.ParamByName('re').Value:=dmKon.re;
      dm.tblAvansi.ParamByName('godina').Value:=dm.godina;
      dm.tblAvansi.Open;

      if not (dm.tblAvansi.IsEmpty) then  //ako ima avansi pokazi go tabota avansi
      begin
        cxgrid2.ActiveLevel:=cxGrid2Level2;
      end
      else
      begin
        cxgrid2.ActiveLevel:=cxGrid2Level1;
        txtAvansStarZbir.EditValue:=0;
        txtAvansStarZbir.Text:=FormatFloat('0.00,.',0);


      end;

      checkSite.Checked:=false;  //�� ������ ������� �� �� �������� ����
      vk_selekcija:=0;
      PresmetajZaNaplata;
      if not (dm.tblNeplateni.IsEmpty) then
      begin
        checkSite.Checked:=true; //edit value changed
      end
      else
      begin
        txtUplata.SetFocus; //avans ce ode celo
      end;
      //checkSite.SetFocus else txtUplata.SetFocus; //cxGrid1.SetFocus;




   end;


end;

procedure TfrmUplata.aSnimiPecatenjeExecute(Sender: TObject);
begin
//  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmUplata.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    //cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    //cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

procedure TfrmUplata.aUplatiListaExecute(Sender: TObject);
begin
  frmUplataNalog:=TfrmUplataNalog.Create(self);
  frmUplataNalog.ShowModal;
  frmUplataNalog.Free;
//
//  //ako ima odbrano nekoj partner da gi osveze
//  dm.tblUplati.FullRefresh;
//  dm.tblAvansi.FullRefresh;

end;

procedure TfrmUplata.aUplatiUpdate(Sender: TObject);
var iznosUplata:Double;
begin
  dm.saldirana:=0;
  aUplati.Enabled:=(dm.saldirana = 0) //ne e saldirana godinata
                and(dm.tblNeplateni.State = dsBrowse)
                  and StrToFloatEx(txtUplata.Text,iznosUplata)
                  and (iznosUplata >0)
                //  and (Validacija(pnlTipUplata)=False)
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmUplata.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmUplata.aBrisiUpdate(Sender: TObject);
begin
//    aBrisi.Enabled:= (dm.saldirana = 0) //ne e saldirana godinata
//     and ((cxgrid2.ActiveLevel = cxGrid2Level1) and not(dm.tblUplati.IsEmpty) and (dm.tblUplatiFIN_NALOG.IsNull));
////  aBrisi.Enabled:= (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) //(dm.tblPocSostojba.State =dsBrowse)
//      and (cxGrid1DBTableView1.DataController.RecordCount >0);
//  aBrisi.Enabled:=(dm.tblPocSostojba.State=dsBrowse)
//    and (dm.tblPocSostojba.RecordCount>0)
end;


procedure TfrmUplata.aEscapeExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse)  then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
    aOtkazi.Execute;  //cancel na dataasetot
  end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmUplata.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmUplata.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmUplata.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
