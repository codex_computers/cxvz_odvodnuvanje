inherited frmGodini: TfrmGodini
  Caption = #1043#1086#1076#1080#1085#1080
  ClientHeight = 577
  ClientWidth = 834
  ExplicitWidth = 850
  ExplicitHeight = 616
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 834
    Height = 330
    ExplicitWidth = 834
    ExplicitHeight = 330
    inherited cxGrid1: TcxGrid
      Width = 830
      Height = 326
      ExplicitWidth = 830
      ExplicitHeight = 326
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Width = 65
        end
        object cxGrid1DBTableView1OTVORENA: TcxGridDBColumn
          DataBinding.FieldName = 'OTVORENA'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 80
        end
        object cxGrid1DBTableView1ZATVORENA: TcxGridDBColumn
          DataBinding.FieldName = 'ZATVORENA'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 80
        end
        object cxGrid1DBTableView1SALDIRANA: TcxGridDBColumn
          DataBinding.FieldName = 'SALDIRANA'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 80
        end
        object cxGrid1DBTableView1ARHIVIRANA: TcxGridDBColumn
          DataBinding.FieldName = 'ARHIVIRANA'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 80
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 456
    Width = 834
    Height = 98
    ExplicitTop = 456
    ExplicitWidth = 834
    ExplicitHeight = 98
    inherited Label1: TLabel
      Left = 16
      Top = 16
      Width = 48
      Caption = #1043#1086#1076#1080#1085#1072' :'
      ExplicitLeft = 16
      ExplicitTop = 16
      ExplicitWidth = 48
    end
    inherited Sifra: TcxDBTextEdit
      Left = 72
      Top = 13
      DataBinding.DataField = 'GODINA'
      ExplicitLeft = 72
      ExplicitTop = 13
      ExplicitWidth = 75
      Width = 75
    end
    inherited OtkaziButton: TcxButton
      Left = 743
      Top = 58
      TabOrder = 3
      ExplicitLeft = 743
      ExplicitTop = 58
    end
    inherited ZapisiButton: TcxButton
      Left = 662
      Top = 58
      TabOrder = 2
      ExplicitLeft = 662
      ExplicitTop = 58
    end
    object cxGroupBoxSostojba: TcxGroupBox
      Left = 164
      Top = 6
      Caption = #1057#1086#1089#1090#1086#1112#1073#1072'...'
      TabOrder = 1
      Height = 80
      Width = 221
      object chbOtvorena: TcxDBCheckBox
        Left = 13
        Top = 21
        Caption = #1054#1090#1074#1086#1088#1077#1085#1072
        DataBinding.DataField = 'OTVORENA'
        Properties.Alignment = taLeftJustify
        Properties.ImmediatePost = True
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        TabOrder = 0
        Transparent = True
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 74
      end
      object chbSaldirana: TcxDBCheckBox
        Left = 100
        Top = 19
        Caption = #1057#1072#1083#1076#1080#1088#1072#1085#1072
        DataBinding.DataField = 'SALDIRANA'
        Properties.Alignment = taLeftJustify
        Properties.ImmediatePost = True
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        TabOrder = 2
        Transparent = True
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 80
      end
      object chbArhivirana: TcxDBCheckBox
        Left = 100
        Top = 46
        Caption = #1040#1088#1093#1080#1074#1080#1088#1072#1085#1072
        DataBinding.DataField = 'ARHIVIRANA'
        Properties.Alignment = taLeftJustify
        Properties.ImmediatePost = True
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        TabOrder = 3
        Transparent = True
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 85
      end
      object chbZatvorena: TcxDBCheckBox
        Left = 13
        Top = 48
        Caption = #1047#1072#1090#1074#1086#1088#1077#1085#1072
        DataBinding.DataField = 'ZATVORENA'
        Properties.Alignment = taLeftJustify
        Properties.ImmediatePost = True
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        TabOrder = 1
        Transparent = True
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 78
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 834
    ExplicitWidth = 834
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 554
    Width = 834
    ExplicitTop = 554
    ExplicitWidth = 834
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 208
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 356
      FloatClientWidth = 120
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 601
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1040#1088#1093#1080#1074#1080#1088#1072#1114#1077
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aArhiviraj
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aDearhiviraj
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    object aArhiviraj: TAction
      Caption = #1040#1088#1093#1080#1074#1080#1088#1072#1112
      ImageIndex = 50
      Visible = False
      OnExecute = aArhivirajExecute
    end
    object aDearhiviraj: TAction
      Caption = #1044#1077#1072#1088#1093#1080#1074#1080#1088#1072#1112
      ImageIndex = 51
      Visible = False
      OnExecute = aDearhivirajExecute
    end
    object aZatvori: TAction
      Caption = 'aZatvori'
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40585.448555891210000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 528
    Top = 192
    PixelsPerInch = 96
  end
end
