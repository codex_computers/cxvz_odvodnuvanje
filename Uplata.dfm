object frmUplata: TfrmUplata
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1059#1087#1083#1072#1090#1080
  ClientHeight = 740
  ClientWidth = 964
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 964
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 1
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          Caption = #1055#1088#1077#1075#1083#1077#1076#1080
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 717
    Width = 964
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' F5 - '#1053#1086#1074', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object pnlPodnozje: TPanel
    Left = 0
    Top = 366
    Width = 964
    Height = 351
    Align = alClient
    TabOrder = 0
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 213
      Width = 962
      Height = 8
      AlignSplitter = salTop
      Control = pnlGrid
    end
    object pnlGrid: TPanel
      Left = 1
      Top = 221
      Width = 962
      Height = 129
      Align = alBottom
      TabOrder = 2
      object Label4: TLabel
        Left = 126
        Top = 58
        Width = 53
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1059#1087#1083#1072#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 56
        Top = 36
        Width = 123
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1074#1072#1085#1089' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 56
        Top = 14
        Width = 123
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1080#1079#1085#1086#1089':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 72
        Top = 80
        Width = 106
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1089#1090#1072#1085#1072#1090' '#1080#1079#1085#1086#1089' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 96
        Top = 102
        Width = 82
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1086#1074' '#1040#1074#1072#1085#1089' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object txtUplata: TcxTextEdit
        Left = 181
        Top = 55
        Properties.Alignment.Horz = taRightJustify
        Properties.OnEditValueChanged = txtUplataPropertiesEditValueChanged
        TabOrder = 0
        Text = '0'
        OnExit = txtUplataExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object OtkaziButton: TcxButton
        Left = 848
        Top = 96
        Width = 75
        Height = 25
        Action = aOtkazi
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 2
        Visible = False
      end
      object ZapisiButton: TcxButton
        Left = 767
        Top = 96
        Width = 75
        Height = 25
        Action = aZapisi
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 3
        Visible = False
      end
      object txtSelektiran: TcxTextEdit
        Left = 181
        Top = 11
        TabStop = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        TabOrder = 4
        Text = '0'
        Width = 121
      end
      object txtOstanat: TcxTextEdit
        Left = 181
        Top = 77
        TabStop = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        TabOrder = 5
        Text = '0'
        Width = 121
      end
      object txtAvansNov: TcxTextEdit
        Left = 181
        Top = 99
        TabStop = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        TabOrder = 6
        Text = '0'
        Width = 121
      end
      object btnUplati: TcxButton
        Left = 311
        Top = 48
        Width = 138
        Height = 41
        Action = aUplati
        TabOrder = 1
        OnClick = btnUplatiClick
        OnKeyDown = EnterKakoTab
      end
      object txtAvansStarZbir: TcxTextEdit
        Left = 181
        Top = 33
        TabStop = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        TabOrder = 7
        Text = '0'
        Width = 121
      end
    end
    object pnlDolg: TPanel
      Left = 1
      Top = 1
      Width = 962
      Height = 212
      Align = alClient
      TabOrder = 1
      object pnlDolgLevo: TPanel
        Left = 1
        Top = 1
        Width = 553
        Height = 210
        Align = alClient
        Caption = 'pnlDolgLevo'
        TabOrder = 0
        object pnlSite: TPanel
          Left = 1
          Top = 1
          Width = 551
          Height = 32
          Align = alTop
          Color = 15790320
          ParentBackground = False
          TabOrder = 0
          object checkSite: TcxCheckBox
            Left = 18
            Top = 4
            Hint = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077' '#1076#1086#1083#1075#1086#1074#1080
            Caption = #1057#1080#1090#1077
            Properties.OnEditValueChanged = checkSitePropertiesEditValueChanged
            TabOrder = 0
            Width = 49
          end
        end
        object cxGrid1: TcxGrid
          Left = 1
          Top = 33
          Width = 551
          Height = 176
          Align = alClient
          TabOrder = 1
          TabStop = False
          object cxGrid1DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            OnCellClick = cxGrid1DBTableView1CellClick
            DataController.DataSource = dm.dsNeplateni
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
                Column = cxGrid1DBTableView1RES_IZNOS
              end
              item
                Format = '0.00 , .'
                Kind = skSum
                Column = cxGrid1DBTableView1RES_ODZADOLZI_IZNOS
              end
              item
                Format = '0.00 , .'
                Kind = skSum
                Column = cxGrid1DBTableView1UPLATA_IZNOS
              end
              item
                Format = '0.00 , .'
                Kind = skSum
                Column = cxGrid1DBTableView1DOLG
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            object Selekcija: TcxGridDBColumn
              Caption = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1112
              DataBinding.ValueType = 'Boolean'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ImmediatePost = True
              Properties.NullStyle = nssUnchecked
              Properties.OnEditValueChanged = SelekcijaPropertiesEditValueChanged
              Width = 30
            end
            object Redosled: TcxGridDBColumn
              Caption = #1056#1077#1076#1086#1089#1083#1077#1076
              DataBinding.ValueType = 'Integer'
              Options.Editing = False
              Width = 54
            end
            object cxGrid1DBTableView1RES_ID: TcxGridDBColumn
              DataBinding.FieldName = 'RES_ID'
              Visible = False
              Width = 118
            end
            object cxGrid1DBTableView1RES_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'RES_BROJ'
              Width = 99
            end
            object cxGrid1DBTableView1RES_DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'RES_DATUM'
              Width = 100
            end
            object cxGrid1DBTableView1RES_TIP: TcxGridDBColumn
              DataBinding.FieldName = 'RES_TIP'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Items = <
                item
                  Description = #1055#1086#1095#1077#1090#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
                  ImageIndex = 0
                  Value = 10
                end
                item
                  Description = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077
                  Value = 11
                end
                item
                  Description = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1076#1086'-'#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077
                  Value = 12
                end>
              Width = 89
            end
            object cxGrid1DBTableView1RES_IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'RES_IZNOS'
              Width = 99
            end
            object cxGrid1DBTableView1RES_ODZADOLZI_IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'RES_ODZADOLZI_IZNOS'
              Width = 198
            end
            object cxGrid1DBTableView1UPLATA_IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'UPLATA_IZNOS'
              Width = 91
            end
            object cxGrid1DBTableView1DOLG: TcxGridDBColumn
              DataBinding.FieldName = 'DOLG'
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object pnlDolgDesno: TPanel
        Left = 561
        Top = 1
        Width = 400
        Height = 210
        Align = alRight
        Caption = #1059#1087#1083#1072#1090#1080' '#1080' '#1072#1074#1072#1085#1089#1080
        Color = 15790320
        ParentBackground = False
        TabOrder = 1
        object cxGrid2: TcxGrid
          Left = 1
          Top = 1
          Width = 398
          Height = 208
          Align = alClient
          TabOrder = 0
          TabStop = False
          RootLevelOptions.DetailTabsPosition = dtpTop
          object cxGrid2DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsUplati
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skSum
                FieldName = 'UPLATENIZNOS'
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            object cxGrid2DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Width = 54
            end
            object cxGrid2DBTableView1UPLATA_ID: TcxGridDBColumn
              DataBinding.FieldName = 'UPLATA_ID'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Visible = False
              Width = 77
            end
            object cxGrid2DBTableView1GODINA: TcxGridDBColumn
              DataBinding.FieldName = 'GODINA'
              Visible = False
              Width = 75
            end
            object cxGrid2DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 74
            end
            object cxGrid2DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Width = 71
            end
            object cxGrid2DBTableView1FAKTURA_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'FAKTURA_BROJ'
              Width = 102
            end
            object cxGrid2DBTableView1IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOS'
              Width = 89
            end
            object cxGrid2DBTableView1TIP: TcxGridDBColumn
              DataBinding.FieldName = 'TIP'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Items = <
                item
                  Description = #1041#1072#1085#1082#1086#1074' '#1080#1079#1074#1086#1076
                  ImageIndex = 0
                  Value = 1
                end
                item
                  Description = #1050#1072#1089#1072
                  Value = 2
                end
                item
                  Description = #1054#1089#1090#1072#1085#1072#1090#1080' '#1076#1086#1082#1091#1084#1077#1085#1090#1080
                  Value = 3
                end>
              Visible = False
              Width = 100
            end
            object cxGrid2DBTableView1VODOKORISNIK_ID: TcxGridDBColumn
              DataBinding.FieldName = 'VODOKORISNIK_ID'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1BANKOV_IZVOD_BR: TcxGridDBColumn
              DataBinding.FieldName = 'BANKOV_IZVOD_BR'
              Width = 110
            end
            object cxGrid2DBTableView1BANKA_TIP: TcxGridDBColumn
              DataBinding.FieldName = 'BANKA_TIP'
              Visible = False
              Width = 143
            end
            object cxGrid2DBTableView1BANKA_ID: TcxGridDBColumn
              DataBinding.FieldName = 'BANKA_ID'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1BANKA_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'BANKA_NAZIV'
              Width = 119
            end
            object cxGrid2DBTableView1KASA_PRIMI_BR: TcxGridDBColumn
              DataBinding.FieldName = 'KASA_PRIMI_BR'
              Visible = False
              Width = 94
            end
            object cxGrid2DBTableView1KASA_RE: TcxGridDBColumn
              DataBinding.FieldName = 'KASA_RE'
              Visible = False
              Width = 115
            end
            object cxGrid2DBTableView1KASA_SMETKA: TcxGridDBColumn
              DataBinding.FieldName = 'KASA_SMETKA'
              Visible = False
              Width = 85
            end
            object cxGrid2DBTableView1DOKUMENT_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'DOKUMENT_BROJ'
              Visible = False
              Width = 145
            end
            object cxGrid2DBTableView1NALOG_TIP: TcxGridDBColumn
              DataBinding.FieldName = 'NALOG_TIP'
              Visible = False
              Width = 73
            end
            object cxGrid2DBTableView1NALOG_DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'NALOG_DATUM'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1NALOG: TcxGridDBColumn
              DataBinding.FieldName = 'NALOG'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              Width = 200
            end
            object cxGrid2DBTableView1CUSTOM1: TcxGridDBColumn
              DataBinding.FieldName = 'CUSTOM1'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1CUSTOM2: TcxGridDBColumn
              DataBinding.FieldName = 'CUSTOM2'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1CUSTOM3: TcxGridDBColumn
              DataBinding.FieldName = 'CUSTOM3'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 200
            end
            object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 200
            end
          end
          object cxGrid2DBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsAvansi
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skSum
                OnGetText = cxGrid2DBTableView2TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                FieldName = 'UPLATENIZNOS'
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ExpandButtonsForEmptyDetails = False
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            object cxGrid2DBTableView2UPLATA_ID: TcxGridDBColumn
              DataBinding.FieldName = 'UPLATA_ID'
              Width = 85
            end
            object cxGrid2DBTableView2DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
            end
            object cxGrid2DBTableView2UPLATENIZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'UPLATENIZNOS'
              Width = 80
            end
            object cxGrid2DBTableView2TIPBROJ: TcxGridDBColumn
              DataBinding.FieldName = 'TIPBROJ'
              Width = 87
            end
          end
          object cxGrid2Level1: TcxGridLevel
            Caption = #1059#1087#1083#1072#1090#1080
            GridView = cxGrid2DBTableView1
          end
          object cxGrid2Level2: TcxGridLevel
            Caption = #1040#1074#1072#1085#1089#1080
            GridView = cxGrid2DBTableView2
            Options.TabsForEmptyDetails = False
          end
        end
      end
      object cxSplitter2: TcxSplitter
        Left = 554
        Top = 1
        Width = 7
        Height = 210
        Control = pnlDolgDesno
      end
    end
  end
  object pnlTipUplata: TPanel
    Left = 0
    Top = 126
    Width = 964
    Height = 240
    Align = alTop
    Color = 15790320
    ParentBackground = False
    TabOrder = 2
    object pageTipUplata: TcxPageControl
      Left = 1
      Top = 1
      Width = 962
      Height = 190
      Align = alTop
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = tabBankov
      Properties.CustomButtons.Buttons = <>
      Properties.TabHeight = 30
      Properties.TabWidth = 200
      OnPageChanging = pageTipUplataPageChanging
      ClientRectBottom = 190
      ClientRectRight = 962
      ClientRectTop = 32
      object tabBankov: TcxTabSheet
        Caption = #1041#1040#1053#1050#1054#1042' '#1048#1047#1042#1054#1044
        ImageIndex = 0
        object Label8: TLabel
          Left = 28
          Top = 23
          Width = 150
          Height = 20
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1073#1072#1085#1082#1086#1074' '#1080#1079#1074#1086#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 134
          Top = 45
          Width = 44
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1072#1085#1082#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 38
          Top = 67
          Width = 140
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1085#1082#1086#1074' '#1080#1079#1074#1086#1076' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label11: TLabel
          Left = 111
          Top = 89
          Width = 67
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1087#1080#1089' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cmbBanka: TcxLookupComboBox
          Tag = 1
          Left = 240
          Top = 42
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'BANKANAZIV'
            end>
          Properties.ListSource = dm.dsBanki
          Properties.OnEditValueChanged = cmbBankaPropertiesEditValueChanged
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 400
        end
        object dateBankov: TcxDateEdit
          Tag = 1
          Left = 180
          Top = 20
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object txtBankovBroj: TcxTextEdit
          Tag = 1
          Left = 180
          Top = 64
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object txtBanka: TcxTextEdit
          Tag = 1
          Left = 180
          Top = 42
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 60
        end
        object memoBankovOpis: TcxMemo
          Left = 180
          Top = 86
          TabStop = False
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Height = 50
          Width = 677
        end
      end
      object tabKasa: TcxTabSheet
        Caption = #1050#1040#1057#1040
        ImageIndex = 1
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label3: TLabel
          Left = 37
          Top = 23
          Width = 140
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 27
          Top = 67
          Width = 150
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1082#1072#1089#1072' '#1087#1088#1080#1084#1080' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 133
          Top = 45
          Width = 44
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1050#1072#1089#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl2: TLabel
          Left = 110
          Top = 89
          Width = 67
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1087#1080#1089' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object cmbKasa: TcxLookupComboBox
          Left = 240
          Top = 42
          Properties.ClearKey = 46
          Properties.KeyFieldNames = 'SMETKA'
          Properties.ListColumns = <
            item
              FieldName = 'SMETKA'
            end
            item
              FieldName = 'OPIS'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsKasaIO
          Properties.OnEditValueChanged = cmbKasaPropertiesEditValueChanged
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 400
        end
        object dateKasa: TcxDateEdit
          Left = 180
          Top = 20
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object txtKasaBroj: TcxTextEdit
          Left = 180
          Top = 64
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object txtKasa: TcxTextEdit
          Left = 180
          Top = 42
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 60
        end
        object memoKasaOpis: TcxMemo
          Left = 180
          Top = 86
          TabStop = False
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Height = 50
          Width = 677
        end
      end
      object tabOstanato: TcxTabSheet
        Caption = #1054#1057#1058#1040#1053#1040#1058#1048' '#1044#1054#1050#1059#1052#1045#1053#1058#1048
        ImageIndex = 2
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label7: TLabel
          Left = 110
          Top = 23
          Width = 67
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 57
          Top = 45
          Width = 120
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl1: TLabel
          Left = 110
          Top = 67
          Width = 67
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1054#1087#1080#1089' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dateOstanat: TcxDateEdit
          Left = 180
          Top = 20
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object txtOstanatBroj: TcxTextEdit
          Left = 180
          Top = 42
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 121
        end
        object memoOstanatOpis: TcxMemo
          Left = 180
          Top = 64
          TabStop = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Height = 50
          Width = 677
        end
      end
    end
    object pnlVK: TPanel
      Left = 1
      Top = 191
      Width = 962
      Height = 48
      Align = alClient
      Color = 15790320
      ParentBackground = False
      TabOrder = 1
      object Label13: TLabel
        Left = 85
        Top = 19
        Width = 89
        Height = 13
        Caption = #1042#1086#1076#1086#1082#1086#1088#1080#1089#1085#1080#1082' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object btnPrikazi: TcxButton
        Left = 863
        Top = 13
        Width = 75
        Height = 25
        Action = aPrikaziZaVodokorisnik
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 2
      end
      object cmbVK: TcxExtLookupComboBox
        Left = 280
        Top = 16
        Hint = #1042#1086#1076#1086#1082#1086#1088#1080#1089#1085#1080#1082
        Properties.ClearKey = 46
        Properties.ImmediatePost = True
        Properties.OnEditValueChanged = cmbVKPropertiesEditValueChanged
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 577
      end
      object txtVK: TcxTextEdit
        Left = 180
        Top = 16
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 97
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 640
    Top = 8
  end
  object PopupMenu1: TPopupMenu
    Left = 712
    Top = 8
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 784
    Top = 8
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 468
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 721
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 116
      DockedTop = 0
      FloatLeft = 1024
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Caption = #1053#1086#1074#1072' '#1087#1086#1074#1088#1096#1080#1085#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 90
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1085#1080#1074#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 89
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Caption = #1041#1088#1080#1096#1080' '#1085#1080#1074#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 91
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aKarticaFinansika
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aKarticaSiteGodini
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aUplatiLista
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aAvansiLista
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 856
    Top = 72
    object aNov: TAction
      Caption = #1053#1086#1074#1072
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
      OnUpdate = aNovUpdate
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
      OnUpdate = aAzurirajUpdate
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
      OnUpdate = aBrisiUpdate
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPrikaziZaVodokorisnik: TAction
      Caption = #1055#1088#1080#1082#1072#1078#1080
      ImageIndex = 80
      OnExecute = aPrikaziZaVodokorisnikExecute
    end
    object aZapisiStavka: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiStavkaExecute
    end
    object aOtkaziStavka: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziStavkaExecute
    end
    object aEscape: TAction
      Caption = #1048#1079#1083#1077#1079
      ShortCut = 27
      OnExecute = aEscapeExecute
    end
    object aPecatiPocetna: TAction
      Caption = #1055#1086#1095#1077#1090#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
      ImageIndex = 30
      OnExecute = aPecatiPocetnaExecute
    end
    object aPecatiPocetnaDizajn: TAction
      Caption = 'aPecatiPocetnaDizajn'
      OnExecute = aPecatiPocetnaDizajnExecute
    end
    object aResetNurkovci: TAction
      Caption = 'aResetNurkovci'
      OnExecute = aResetNurkovciExecute
    end
    object aUplati: TAction
      Caption = #1042#1085#1077#1089#1080' '#1091#1087#1083#1072#1090#1072
      ImageIndex = 7
      OnUpdate = aUplatiUpdate
    end
    object aPopupDIZAJN: TAction
      Caption = 'aPopupDIZAJN'
      ShortCut = 24697
      OnExecute = aPopupDIZAJNExecute
    end
    object aKarticaFinansika: TAction
      Caption = #1060#1080#1085#1072#1085#1089#1080#1082#1072' '#1082#1072#1088#1090#1080#1094#1072
      ImageIndex = 64
      OnExecute = aKarticaFinansikaExecute
    end
    object aKarticaSiteGodini: TAction
      Caption = #1060#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1082#1072#1088#1090#1080#1094#1072' '#1079#1072' '#1089#1080#1090#1077' '#1075#1086#1076#1080#1085#1080
      ImageIndex = 80
      OnExecute = aKarticaSiteGodiniExecute
    end
    object aKarticaFinansiskaDIZAJN: TAction
      Caption = #1060#1080#1085#1072#1085#1089#1080#1082#1072' '#1082#1072#1088#1090#1080#1094#1072
      ShortCut = 24697
      OnExecute = aKarticaFinansiskaDIZAJNExecute
    end
    object aKarticaSiteGodiniDIZAJN: TAction
      Caption = #1060#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1082#1072#1088#1090#1080#1094#1072' '#1079#1072' '#1089#1080#1090#1077' '#1075#1086#1076#1080#1085#1080
      OnExecute = aKarticaSiteGodiniDIZAJNExecute
    end
    object aUplatiLista: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1091#1087#1083#1072#1090#1080
      ImageIndex = 74
      OnExecute = aUplatiListaExecute
    end
    object aAvansiLista: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1072#1074#1072#1085#1089#1080
      ImageIndex = 74
      OnExecute = aAvansiListaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 920
    Top = 128
    object dxComponentPrinter1Link1: TdxGridReportLink
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 904
    Top = 8
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object popupDizajn: TPopupMenu
    Left = 664
    Top = 192
    object aKarticaFinansiskaDIZAJN1: TMenuItem
      Action = aKarticaFinansiskaDIZAJN
    end
    object aKarticaSiteGodiniDIZAJN1: TMenuItem
      Action = aKarticaSiteGodiniDIZAJN
    end
  end
end
