unit dmUnit;

interface

uses
    System.SysUtils, System.Classes, Data.DB, FIBDataSet, pFIBDataSet, FIBQuery,
  pFIBQuery, Variants, Controls, FIBDatabase, pFIBDatabase, pFIBStoredProc,
  frxClass, frxDBSet, cxCustomPivotGrid, cxTL, cxGridCardView,
  cxGridBandedTableView, cxStyles, cxGridTableView, cxClasses, XMLIntf, XMLDoc,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP,
  frxExportMail, frxExportPDF, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack,
  IdSSL, IdSSLOpenSSL, Vcl.Menus, IdMessage, IdGlobal, System.Bluetooth,
  System.Bluetooth.Components;

type
  Tdm = class(TDataModule)
    tblGodini: TpFIBDataSet;
    dsGodini: TDataSource;
    tblGodiniAPP: TFIBStringField;
    tblGodiniGODINA: TFIBIntegerField;
    tblGodiniRE: TFIBIntegerField;
    tblGodiniOTVORENA: TFIBIntegerField;
    tblGodiniSALDIRANA: TFIBIntegerField;
    tblGodiniZATVORENA: TFIBIntegerField;
    tblGodiniARHIVIRANA: TFIBIntegerField;
    tblResenija: TpFIBDataSet;
    dsResenija: TDataSource;
    tblResenijaID: TFIBBCDField;
    tblResenijaRE: TFIBIntegerField;
    tblResenijaGODINA: TFIBIntegerField;
    tblResenijaTIP_DOK: TFIBSmallIntField;
    tblResenijaTIP_DOK_NAZIV: TFIBStringField;
    tblResenijaDATUM_DOK: TFIBDateField;
    tblResenijaDDO: TFIBDateField;
    tblResenijaDOK_BROJ: TFIBStringField;
    tblResenijaVODOKORISNIK_ID: TFIBBCDField;
    tblResenijaPRAVNO_LICE: TFIBSmallIntField;
    tblResenijaNAZIV: TFIBStringField;
    tblResenijaEMB: TFIBStringField;
    tblResenijaDANOCEN: TFIBStringField;
    tblResenijaTIP_ZEMJISTE: TFIBSmallIntField;
    tblResenijaTIP_ZEMJISTE_NAZIV: TFIBStringField;
    tblResenijaPOVRSINA: TFIBBCDField;
    tblResenijaCENA: TFIBBCDField;
    tblResenijaFOND_ZA_VODI: TFIBIntegerField;
    tblResenijaDDV: TFIBIntegerField;
    tblResenijaREF_ID: TFIBBCDField;
    tblResenijaARC_O_BR: TFIBStringField;
    tblResenijaARC_BR: TFIBIntegerField;
    tblResenijaARC_PODBR: TFIBSmallIntField;
    tblResenijaARC_BROJ: TFIBStringField;
    tblResenijaDATUM_DOSTAVA: TFIBDateField;
    tblResenijaSTATUS: TFIBSmallIntField;
    tblResenijaCUSTOM1: TFIBStringField;
    tblResenijaCUSTOM2: TFIBStringField;
    tblResenijaCUSTOM3: TFIBStringField;
    tblResenijaTS_INS: TFIBDateTimeField;
    tblResenijaTS_UPD: TFIBDateTimeField;
    tblResenijaUSR_INS: TFIBStringField;
    tblResenijaUSR_UPD: TFIBStringField;
    tblVodokorisnici: TpFIBDataSet;
    dsVodokorisnici: TDataSource;
    tblVodokorisniciID: TFIBBCDField;
    tblVodokorisniciRE: TFIBIntegerField;
    tblVodokorisniciPRAVNO_LICE: TFIBSmallIntField;
    tblVodokorisniciNAZIV: TFIBStringField;
    tblVodokorisniciIME: TFIBStringField;
    tblVodokorisniciTATKO: TFIBStringField;
    tblVodokorisniciPREZIME: TFIBStringField;
    tblVodokorisniciEMB: TFIBStringField;
    tblVodokorisniciDANOCEN: TFIBStringField;
    tblVodokorisniciADRESA: TFIBStringField;
    tblVodokorisniciADRESA_2: TFIBStringField;
    tblVodokorisniciMESTO: TFIBIntegerField;
    tblVodokorisniciTEL: TFIBStringField;
    tblVodokorisniciFAX: TFIBStringField;
    tblVodokorisniciLK: TFIBStringField;
    tblVodokorisniciOLD_ID: TFIBIntegerField;
    tblVodokorisniciTS_INS: TFIBDateTimeField;
    tblVodokorisniciTS_UPD: TFIBDateTimeField;
    tblVodokorisniciUSR_INS: TFIBStringField;
    tblVodokorisniciUSR_UPD: TFIBStringField;
    qDokID: TpFIBQuery;
    qArhivskiBr: TpFIBQuery;
    qArhivskiBrPredlog: TpFIBQuery;
    qArhivskiPodBr: TpFIBQuery;
    qCenovnik: TpFIBQuery;
    tblResenieZadolzuvanjeRef: TpFIBDataSet;
    dsResenieZadolzuvanjeRef: TDataSource;
    tblResenieZadolzuvanjeRefID: TFIBBCDField;
    tblResenieZadolzuvanjeRefPOVRSINA: TFIBBCDField;
    tblResenieZadolzuvanjeRefGODINA: TFIBIntegerField;
    tblPocetnaSostojba: TpFIBDataSet;
    dsPocetnaSostojba: TDataSource;
    tblPocetnaSostojbaID: TFIBBCDField;
    tblPocetnaSostojbaRE: TFIBIntegerField;
    tblPocetnaSostojbaGODINA: TFIBIntegerField;
    tblPocetnaSostojbaGODINA_SEZONA: TFIBIntegerField;
    tblPocetnaSostojbaTIP_DOK: TFIBSmallIntField;
    tblPocetnaSostojbaTIP_DOK_NAZIV: TFIBStringField;
    tblPocetnaSostojbaDATUM_DOK: TFIBDateField;
    tblPocetnaSostojbaDDO: TFIBDateField;
    tblPocetnaSostojbaDOK_BROJ: TFIBStringField;
    tblPocetnaSostojbaVODOKORISNIK_ID: TFIBBCDField;
    tblPocetnaSostojbaPRAVNO_LICE: TFIBSmallIntField;
    tblPocetnaSostojbaNAZIV: TFIBStringField;
    tblPocetnaSostojbaEMB: TFIBStringField;
    tblPocetnaSostojbaDANOCEN: TFIBStringField;
    tblPocetnaSostojbaTIP_ZEMJISTE: TFIBSmallIntField;
    tblPocetnaSostojbaTIP_ZEMJISTE_NAZIV: TFIBStringField;
    tblPocetnaSostojbaPOVRSINA: TFIBBCDField;
    tblPocetnaSostojbaCENA: TFIBBCDField;
    tblPocetnaSostojbaFOND_ZA_VODI: TFIBIntegerField;
    tblPocetnaSostojbaDDV: TFIBIntegerField;
    tblPocetnaSostojbaREF_ID: TFIBBCDField;
    tblPocetnaSostojbaARC_O_BR: TFIBStringField;
    tblPocetnaSostojbaARC_BR: TFIBIntegerField;
    tblPocetnaSostojbaARC_PODBR: TFIBSmallIntField;
    tblPocetnaSostojbaARC_BROJ: TFIBStringField;
    tblPocetnaSostojbaDATUM_DOSTAVA: TFIBDateField;
    tblPocetnaSostojbaSTATUS: TFIBSmallIntField;
    tblPocetnaSostojbaCUSTOM1: TFIBStringField;
    tblPocetnaSostojbaCUSTOM2: TFIBStringField;
    tblPocetnaSostojbaCUSTOM3: TFIBStringField;
    tblPocetnaSostojbaTS_INS: TFIBDateTimeField;
    tblPocetnaSostojbaTS_UPD: TFIBDateTimeField;
    tblPocetnaSostojbaUSR_INS: TFIBStringField;
    tblPocetnaSostojbaUSR_UPD: TFIBStringField;
    tblPocetnaSostojbaOPIS: TFIBStringField;
    pBroj: TpFIBStoredProc;
    tblPocetnaSostojbaBROJ: TFIBBCDField;
    tblPocetnaSostojbaIZNOS_VKUPEN: TFIBBCDField;
    tblPocetnaSostojbaPRAVNO_LICE_NAZIV: TFIBStringField;
    tblResenijaGODINA_SEZONA: TFIBIntegerField;
    tblResenijaBROJ: TFIBBCDField;
    tblResenieZadolzuvanjeRefBROJ: TFIBBCDField;
    tblResenijaIZNOS_VKUPEN: TFIBBCDField;
    tblResenieZadolzuvanjeRefIZNOS_VKUPEN: TFIBBCDField;
    tblResenijaIMOTEN_LIST: TFIBStringField;
    tblResenijaBROJ_PARCELA: TFIBStringField;
    tblResenijaK_OPSTINA: TFIBIntegerField;
    tblResenijaKO_NAZIV: TFIBStringField;
    tblSifKatastarskaOpstina: TpFIBDataSet;
    dsSifKatastarskaOpstina: TDataSource;
    tblSifKatastarskaOpstinaID: TFIBIntegerField;
    tblSifKatastarskaOpstinaNAZIV: TFIBStringField;
    tblSifKatastarskaOpstinaRE: TFIBIntegerField;
    tblSifKatastarskaOpstinaTS_INS: TFIBDateTimeField;
    tblSifKatastarskaOpstinaTS_UPD: TFIBDateTimeField;
    tblSifKatastarskaOpstinaUSR_INS: TFIBStringField;
    tblSifKatastarskaOpstinaUSR_UPD: TFIBStringField;
    tblNeplateni: TpFIBDataSet;
    dsNeplateni: TDataSource;
    tblUplati: TpFIBDataSet;
    dsUplati: TDataSource;
    tblAvansi: TpFIBDataSet;
    dsAvansi: TDataSource;
    pUplataRaspredeli: TpFIBStoredProc;
    tblKasaIO: TpFIBDataSet;
    tblNeplateniRES_BROJ: TFIBBCDField;
    tblNeplateniRES_DATUM: TFIBDateField;
    tblNeplateniRES_TIP: TFIBIntegerField;
    tblNeplateniRES_IZNOS: TFIBBCDField;
    tblNeplateniUPLATA_IZNOS: TFIBBCDField;
    tblNeplateniDOLG: TFIBBCDField;
    tblNeplateniRES_ID: TFIBIntegerField;
    tblNeplateniRES_ODZADOLZI_IZNOS: TFIBBCDField;
    tblUplatiID: TFIBBCDField;
    tblUplatiRE: TFIBIntegerField;
    tblUplatiGODINA: TFIBIntegerField;
    tblUplatiDATUM: TFIBDateField;
    tblUplatiTIP: TFIBSmallIntField;
    tblUplatiBROJ: TFIBStringField;
    tblUplatiFAKTURA_BROJ: TFIBBCDField;
    tblUplatiIZNOS: TFIBBCDField;
    tblUplatiBANKOV_IZVOD_BR: TFIBStringField;
    tblUplatiBANKA_TIP: TFIBIntegerField;
    tblUplatiBANKA_ID: TFIBIntegerField;
    tblUplatiKASA_PRIMI_BR: TFIBStringField;
    tblUplatiKASA_RE: TFIBIntegerField;
    tblUplatiKASA_SMETKA: TFIBIntegerField;
    tblUplatiDOKUMENT_BROJ: TFIBStringField;
    tblUplatiNALOG_TIP: TFIBStringField;
    tblUplatiNALOG_DATUM: TFIBDateField;
    tblUplatiNALOG: TFIBIntegerField;
    tblUplatiOPIS: TFIBStringField;
    tblUplatiCUSTOM1: TFIBStringField;
    tblUplatiCUSTOM2: TFIBStringField;
    tblUplatiCUSTOM3: TFIBStringField;
    tblUplatiTS_INS: TFIBDateTimeField;
    tblUplatiTS_UPD: TFIBDateTimeField;
    tblUplatiUSR_INS: TFIBStringField;
    tblUplatiUSR_UPD: TFIBStringField;
    tblUplatiVODOKORISNIK_ID: TFIBBCDField;
    tblUplatiUPLATA_ID: TFIBBCDField;
    tblUplatiBANKA_NAZIV: TFIBStringField;
    tblAvansiUPLATA_ID: TFIBBCDField;
    tblAvansiDATUM: TFIBDateField;
    tblAvansiTIPBROJ: TFIBStringField;
    tblAvansiUPLATENIZNOS: TFIBBCDField;
    dsBanki: TDataSource;
    tblBanki: TpFIBDataSet;
    tblBankiBANKANAZIV: TFIBStringField;
    tblBankiMESTOBANKA: TFIBStringField;
    tblBankiID: TFIBIntegerField;
    tblSetup: TpFIBDataSet;
    dsSetup: TDataSource;
    tblSetupV1: TFIBStringField;
    tblSetupV2: TFIBStringField;
    pTransaction: TpFIBTransaction;
    tblVodokorisniciMESTO_NAZIV: TFIBStringField;
    tblPDolznici: TpFIBDataSet;
    dsPDolznici: TDataSource;
    tblPDolzniciRE_OUT: TFIBIntegerField;
    tblPDolzniciVODOKORISNIK_ID: TFIBBCDField;
    tblPDolzniciVODOKORISNIK_NAZIV: TFIBStringField;
    tblPDolzniciEMB: TFIBStringField;
    tblPDolzniciDANOCEN: TFIBStringField;
    tblPDolzniciLK: TFIBStringField;
    tblPDolzniciPRAVNO_LICE: TFIBSmallIntField;
    tblPDolzniciFAKTURA_GODINA_ORIGINAL: TFIBIntegerField;
    tblPDolzniciDATUM_F: TFIBDateField;
    tblPDolzniciGODINA: TFIBIntegerField;
    tblPDolzniciBROJ: TFIBBCDField;
    tblPDolzniciTIP: TFIBIntegerField;
    tblPDolzniciADRESA: TFIBStringField;
    tblPDolzniciMESTO_NAZIV: TFIBStringField;
    tblPDolzniciOPSTINA_OZNAKA: TFIBIntegerField;
    tblPDolzniciOPSTINA_NAZIV: TFIBStringField;
    tblPDolzniciO_IZNOS_DOLG: TFIBBCDField;
    tblPDolzniciOPIS: TFIBStringField;
    tblPDolzniciOPIS_FAKTURA: TFIBStringField;
    tblPDolzniciEMBEDB: TFIBStringField;
    tblPDolzniciTEL: TFIBStringField;
    tblPDolzniciTUZENA: TFIBIntegerField;
    tblKasaIOGODINA: TFIBIntegerField;
    tblKasaIORE: TFIBIntegerField;
    tblKasaIOSMETKA: TFIBIntegerField;
    tblKasaIOOPIS: TFIBStringField;
    tblKasaIOAKTIVNA: TFIBIntegerField;
    dsKasaIO: TDataSource;
    pPROC_VZ_ODV_UPLATA_OD_AVANSI: TpFIBStoredProc;
    tblCenovnikGodina: TpFIBDataSet;
    dsCenovnikGodina: TDataSource;
    tblCenovnikGodinaID: TFIBIntegerField;
    tblCenovnikGodinaRE: TFIBIntegerField;
    tblCenovnikGodinaGODINA: TFIBIntegerField;
    tblCenovnikGodinaTIP_ZEMJISTE: TFIBSmallIntField;
    tblCenovnikGodinaTIP_ZEMJISTE_NAZIV: TFIBStringField;
    tblCenovnikGodinaCENA_BEZ_DDV: TFIBBCDField;
    tblCenovnikGodinaFOND_ZA_VODI: TFIBIntegerField;
    tblCenovnikGodinaDDV: TFIBIntegerField;
    tblCenovnikGodinaCENA_SO_DDV: TFIBBCDField;
    tblCenovnikGodinaCUSTOM1: TFIBStringField;
    tblCenovnikGodinaCUSTOM2: TFIBStringField;
    tblCenovnikGodinaCUSTOM3: TFIBStringField;
    tblCenovnikGodinaTS_INS: TFIBDateTimeField;
    tblCenovnikGodinaTS_UPD: TFIBDateTimeField;
    tblCenovnikGodinaUSR_INS: TFIBStringField;
    tblCenovnikGodinaUSR_UPD: TFIBStringField;
    tblUplataPeriod: TpFIBDataSet;
    dsUplataPeriod: TDataSource;
    tblUplataPeriodID: TFIBBCDField;
    tblUplataPeriodDATUM: TFIBDateField;
    tblUplataPeriodTIP: TFIBSmallIntField;
    tblUplataPeriodBROJ: TFIBStringField;
    tblUplataPeriodFAKTURA_BROJ: TFIBBCDField;
    tblUplataPeriodIZNOS: TFIBBCDField;
    tblUplataPeriodBANKA_NAZIV: TFIBStringField;
    tblUplataPeriodDOKUMENT_BROJ: TFIBStringField;
    tblUplataPeriodNALOG_TIP: TFIBStringField;
    tblUplataPeriodNALOG_DATUM: TFIBDateField;
    tblUplataPeriodOPIS: TFIBStringField;
    tblUplataPeriodCUSTOM1: TFIBStringField;
    tblUplataPeriodCUSTOM2: TFIBStringField;
    tblUplataPeriodCUSTOM3: TFIBStringField;
    tblUplataPeriodTS_INS: TFIBDateTimeField;
    tblUplataPeriodTS_UPD: TFIBDateTimeField;
    tblUplataPeriodUSR_INS: TFIBStringField;
    tblUplataPeriodUSR_UPD: TFIBStringField;
    tblUplataPeriodVODOKORISNIK_ID: TFIBBCDField;
    tblUplataPeriodUPLATA_ID: TFIBBCDField;
    tblUplataPeriodPRAVNO_LICE: TFIBSmallIntField;
    tblUplataPeriodIME: TFIBStringField;
    tblUplataPeriodTATKO: TFIBStringField;
    tblUplataPeriodPREZIME: TFIBStringField;
    tblUplataPeriodEMB: TFIBStringField;
    tblUplataPeriodDANOCEN: TFIBStringField;
    tblUplataPeriodADRESA: TFIBStringField;
    tblUplataPeriodNAZIV: TFIBStringField;
    tblUplataPeriodGODINA: TFIBIntegerField;
    tblUplataPeriodNALOG: TFIBIntegerField;
    tblUplataPeriodKASA_PRIMI_BR: TFIBStringField;
    tblUplataPeriodKASA_RE: TFIBIntegerField;
    tblUplataPeriodKASA_SMETKA: TFIBIntegerField;
    tblUplataPeriodRE: TFIBIntegerField;
    tblUplataPeriodBANKOV_IZVOD_BR: TFIBStringField;
    tblUplataPeriodBANKA_TIP: TFIBIntegerField;
    tblUplataPeriodBANKA_ID: TFIBIntegerField;
    pUplataBrisi: TpFIBStoredProc;
    tblResenijaPRAVNO_LICE_NAZIV: TFIBStringField;
    tblVodokorisniciSTATUS: TFIBSmallIntField;
    tblVodokorisniciSTATUS_OPIS: TFIBStringField;
    tblResenijaSTATUS_VODOKORISNIK: TFIBSmallIntField;
    tblResenijaSTATUS_OPIS: TFIBStringField;
    tblUplataAvansiPeriod: TpFIBDataSet;
    tbl2: TFIBBCDField;
    tbl3: TFIBDateField;
    FIBSmallIntField1: TFIBSmallIntField;
    tbl4: TFIBStringField;
    tbl5: TFIBBCDField;
    tbl6: TFIBBCDField;
    tbl7: TFIBStringField;
    tbl8: TFIBStringField;
    tbl9: TFIBStringField;
    tblUplataAvansiPeriod0: TFIBDateField;
    tblUplataAvansiPeriod1: TFIBStringField;
    tblUplataAvansiPeriod2: TFIBStringField;
    tblUplataAvansiPeriod3: TFIBStringField;
    tblUplataAvansiPeriod4: TFIBStringField;
    tblUplataAvansiPeriod5: TFIBStringField;
    tblUplataAvansiPeriod6: TFIBDateTimeField;
    tblUplataAvansiPeriod7: TFIBStringField;
    tblUplataAvansiPeriod8: TFIBDateTimeField;
    tblUplataAvansiPeriod9: TFIBBCDField;
    tbl20: TFIBBCDField;
    FIBSmallIntField2: TFIBSmallIntField;
    tbl21: TFIBStringField;
    tbl22: TFIBStringField;
    tbl23: TFIBStringField;
    tbl24: TFIBStringField;
    tbl25: TFIBStringField;
    tbl26: TFIBStringField;
    tbl27: TFIBStringField;
    tbl28: TFIBIntegerField;
    tbl29: TFIBIntegerField;
    tbl30: TFIBStringField;
    tbl31: TFIBIntegerField;
    tbl32: TFIBIntegerField;
    tbl33: TFIBIntegerField;
    tbl34: TFIBStringField;
    tbl35: TFIBIntegerField;
    tbl36: TFIBIntegerField;
    dsUplataAvansiPeriod: TDataSource;
    tblVodokorisniciPOVRSINA: TFIBBCDField;
    qPovrsinaVodokorisnik: TpFIBQuery;
    PROC_VZ_POCETNO_SALDO: TpFIBStoredProc;
    tblNeplteniFakturiSaldiranje: TpFIBDataSet;
    dsNeplteniFakturiSaldiranje: TDataSource;
    tblPocetnoSaldoSaldiranje: TpFIBDataSet;
    dsPocetnoSaldoSaldiranje: TDataSource;
    tblResenijaIZNOS_VKUPEN_ZAOKRUZEN: TFIBBCDField;
    qArhivskiPodBrDopolnitelniRes: TpFIBQuery;
    tblResenijaIZNOS_NA_VODI: TFIBBCDField;
    tblResenijaIZNOS_NA_DDV: TFIBBCDField;
    procedure tblGodiniBeforeOpen(DataSet: TDataSet);
    procedure tblGodiniBeforeDelete(DataSet: TDataSet);
    procedure tblGodiniBeforePost(DataSet: TDataSet);
    procedure fbcdfldCenovnikRECENA_BEZ_DDVSetText(Sender: TField;
      const Text: string);
    procedure fbcdfldCenovnikRECENA_SO_DDVSetText(Sender: TField; const Text: string);
    procedure tblResenijaBeforeDelete(DataSet: TDataSet);
    procedure tblResenijaBeforeOpen(DataSet: TDataSet);
    procedure tblVodokorisniciBeforeOpen(DataSet: TDataSet);
    procedure tblResenijaBeforePost(DataSet: TDataSet);
    procedure fbntgrfldCenovnikREDDVSetText(Sender: TField; const Text: string);
    procedure tblPocetnaSostojbaBeforeDelete(DataSet: TDataSet);
    procedure tblPocetnaSostojbaBeforeOpen(DataSet: TDataSet);
    procedure tblPocetnaSostojbaBeforePost(DataSet: TDataSet);
    function return6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 , broj: Variant):Variant;
    procedure tblSifKatastarskaOpstinaBeforeOpen(DataSet: TDataSet);
    procedure tblVodokorisniciBeforeDelete(DataSet: TDataSet);
    procedure tblVodokorisniciBeforePost(DataSet: TDataSet);
    procedure tblSifKatastarskaOpstinaBeforePost(DataSet: TDataSet);
    procedure tblSifKatastarskaOpstinaBeforeDelete(DataSet: TDataSet);
    procedure tblUplatiBeforeDelete(DataSet: TDataSet);
    procedure tblCenovnikGodinaBeforeOpen(DataSet: TDataSet);
    procedure tblCenovnikGodinaBeforeDelete(DataSet: TDataSet);
    procedure tblCenovnikGodinaBeforePost(DataSet: TDataSet);
    procedure insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
  private
    { Private declarations }
  public
    { Public declarations }
   godina, re, zatvorena, otvorena, arhivirana,tekovna_god,saldirana, tip_banka:Integer;
  end;

var
  dm: Tdm;

implementation

uses
  dmKonekcija, DaNe;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure Tdm.tblCenovnikGodinaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;



procedure Tdm.tblCenovnikGodinaBeforeOpen(DataSet: TDataSet);
begin
   dm.tblCenovnikGodina.ParamByName('godina').Value:=dm.godina;
   dm.tblCenovnikGodina.ParamByName('re').Value:=dmKon.firma_id;
end;

procedure Tdm.tblCenovnikGodinaBeforePost(DataSet: TDataSet);
begin
     dm.tblCenovnikGodinaGODINA.Value:= godina;
     dm.tblCenovnikGodinaRE.Value:= dmKon.firma_id;
end;

procedure Tdm.fbcdfldCenovnikRECENA_BEZ_DDVSetText(Sender: TField;
  const Text: string);
begin
//     tblCenovnikCENA_BEZ_DDV.Value:=StrToFloat(Text);
//     if not tblCenovnikDDV.IsNull then
//        begin
//          tblCenovnikCENA_SO_DDV.Value:=(tblCenovnikCENA_BEZ_DDV.Value* tblCenovnikDDV.Value/100) + tblCenovnikCENA_BEZ_DDV.Value;
//        end;

end;

procedure Tdm.fbcdfldCenovnikRECENA_SO_DDVSetText(Sender: TField; const Text: string);
begin
//     tblCenovnikCENA_SO_DDV.Value:=StrToFloat(Text);
//     if not tblCenovnikDDV.IsNull then
//        begin
//          tblCenovnikCENA_BEZ_DDV.Value:=tblCenovnikCENA_SO_DDV.Value*100/(100 + tblCenovnikDDV.Value);
//        end;
end;

procedure Tdm.fbntgrfldCenovnikREDDVSetText(Sender: TField; const Text: string);
begin
//     tblCenovnikDDV.Value:=StrToInt(Text);
//     if (not tblCenovnikDDV.IsNull)  and (not tblCenovnikCENA_BEZ_DDV.IsNull) then
//        begin
//          tblCenovnikCENA_SO_DDV.Value:=(tblCenovnikCENA_BEZ_DDV.Value* tblCenovnikDDV.Value/100) + tblCenovnikCENA_BEZ_DDV.Value;
//        end;
end;

procedure Tdm.tblGodiniBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblGodiniBeforeOpen(DataSet: TDataSet);
begin
     dm.tblGodini.ParamByName('app').AsString:=  dmkon.aplikacija;
     dm.tblGodini.ParamByName('firma').AsInteger:= dmkon.firma_id;
end;

procedure Tdm.tblGodiniBeforePost(DataSet: TDataSet);
begin
   tblGodiniAPP.AsString := dmKon.aplikacija;
   tblGodiniRE.AsInteger := dmkon.re;
end;

procedure Tdm.tblPocetnaSostojbaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblPocetnaSostojbaBeforeOpen(DataSet: TDataSet);
begin
      dm.tblPocetnaSostojba.ParamByName('re').Value:=dmkon.firma_id;
      dm.tblPocetnaSostojba.ParamByName('godina').Value:=dm.godina;
end;

procedure Tdm.tblPocetnaSostojbaBeforePost(DataSet: TDataSet);
begin
     dm.tblPocetnaSostojbaBROJ.Value:=dm.return6(dm.pBroj,'RE', 'TIP_DOK', 'GODINA', null, null, null,dmKon.firma_id, 10, dm.godina, null, null, null,'BROJ');
     dm.tblPocetnaSostojbaRE.Value:=dmkon.firma_id;
     dm.tblPocetnaSostojbaGODINA.Value:=dm.godina;
     dm.tblPocetnaSostojbaTIP_DOK.Value:=10;
end;

procedure Tdm.tblResenijaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblResenijaBeforeOpen(DataSet: TDataSet);
begin
     dm.tblResenija.ParamByName('godina').AsInteger:= godina;
     dm.tblResenija.ParamByName('re').AsInteger:= dmkon.firma_id;
end;

procedure Tdm.tblResenijaBeforePost(DataSet: TDataSet);
begin
     dm.tblResenijaGODINA.Value:= godina;
     dm.tblResenijaRE.Value:= dmkon.firma_id;
end;

procedure Tdm.tblSifKatastarskaOpstinaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblSifKatastarskaOpstinaBeforeOpen(DataSet: TDataSet);
begin
     dm.tblSifKatastarskaOpstina.ParamByName('re').Value:=dmKon.firma_id;
end;

procedure Tdm.tblSifKatastarskaOpstinaBeforePost(DataSet: TDataSet);
begin
    dm.tblSifKatastarskaOpstinaRE.Value:=dmKon.firma_id;
end;

procedure Tdm.tblUplatiBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblVodokorisniciBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure Tdm.tblVodokorisniciBeforeOpen(DataSet: TDataSet);
begin
     dm.tblVodokorisnici.ParamByName('re').AsInteger:= dmkon.firma_id;
end;

procedure Tdm.tblVodokorisniciBeforePost(DataSet: TDataSet);
begin
      dm.tblVodokorisniciRE.Value:= dmkon.firma_id;
end;

function Tdm.return6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 , broj: Variant):Variant;
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;
 procedure Tdm.insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
    end;

end.
