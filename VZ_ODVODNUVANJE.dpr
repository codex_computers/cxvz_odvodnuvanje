program VZ_ODVODNUVANJE;

uses
  Vcl.Forms,
  Master in '..\Share2010\Master.pas' {frmMaster},
  Cenovnik in 'Cenovnik.pas' {frmCenovnik},
  Utils in '..\Share2010\Utils.pas',
  Firma in '..\Share2010\Firma.pas' {frmFirma},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  Login in '..\Share2010\Login.pas' {frmLogin},
  Main in 'Main.pas' {frmMain},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  Godini in 'Godini.pas' {frmGodini},
  MK in '..\Share2010\MK.pas' {frmMK},
  Resenija in 'Resenija.pas' {frmResenija},
  PocetnaSostojba in 'PocetnaSostojba.pas' {frmPocetnaSostojba},
  cxConstantsMak in '..\Share2010\cxConstantsMak.pas',
  KatasterskiOpstini in 'KatasterskiOpstini.pas' {frmKatasterskiOpstini},
  Vodokorisnik in 'Vodokorisnik.pas' {frmVodokorisnik},
  Mesto in '..\Share2010\Mesto.pas' {frmMesto},
  Uplata in 'Uplata.pas' {frmUplata},
  Dolznici in 'Dolznici.pas' {frmDolznici},
  StariTuzbi in 'StariTuzbi.pas' {frmStariTuzbi},
  Partner in '..\Share2010\Partner.pas' {frmPartner},
  ListaAvansi in 'ListaAvansi.pas' {frmListaAvansi};

{$R *.res}

begin
 Application.Initialize;

  Application.Title := '�� �����������';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='VZO';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.
