inherited frmCenovnik: TfrmCenovnik
  Caption = #1062#1077#1085#1086#1074#1085#1080#1082
  ClientHeight = 659
  ClientWidth = 594
  ExplicitWidth = 610
  ExplicitHeight = 698
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 594
    Height = 290
    ExplicitWidth = 594
    ExplicitHeight = 290
    inherited cxGrid1: TcxGrid
      Width = 590
      Height = 286
      ExplicitWidth = 590
      ExplicitHeight = 286
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsCenovnikGodina
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 52
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
          Width = 140
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Visible = False
          Width = 56
        end
        object cxGrid1DBTableView1TIP_ZEMJISTE_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_ZEMJISTE_NAZIV'
          Width = 94
        end
        object cxGrid1DBTableView1TIP_ZEMJISTE: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_ZEMJISTE'
          Visible = False
          Width = 130
        end
        object cxGrid1DBTableView1FOND_ZA_VODI: TcxGridDBColumn
          DataBinding.FieldName = 'FOND_ZA_VODI'
          Width = 137
        end
        object cxGrid1DBTableView1CENA_BEZ_DDV: TcxGridDBColumn
          DataBinding.FieldName = 'CENA_BEZ_DDV'
          Width = 87
        end
        object cxGrid1DBTableView1DDV: TcxGridDBColumn
          DataBinding.FieldName = 'DDV'
          Width = 52
        end
        object cxGrid1DBTableView1CENA_SO_DDV: TcxGridDBColumn
          DataBinding.FieldName = 'CENA_SO_DDV'
          Width = 81
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 59
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 61
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 60
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 170
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 222
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 203
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 239
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 416
    Width = 594
    Height = 220
    ExplicitTop = 416
    ExplicitWidth = 594
    ExplicitHeight = 220
    inherited Label1: TLabel
      Left = 402
      Top = 14
      Visible = False
      ExplicitLeft = 402
      ExplicitTop = 14
    end
    object Label2: TLabel [1]
      Left = 31
      Top = 88
      Width = 89
      Height = 13
      Caption = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 38
      Top = 169
      Width = 82
      Height = 13
      Caption = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 34
      Top = 115
      Width = 86
      Height = 13
      Caption = #1060#1086#1085#1076' '#1079#1072' '#1074#1086#1076#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label5: TLabel [4]
      Left = 226
      Top = 142
      Width = 13
      Height = 13
      Caption = '%'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel [5]
      Left = 86
      Top = 142
      Width = 33
      Height = 13
      Caption = #1044#1044#1042' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label7: TLabel [6]
      Left = 226
      Top = 115
      Width = 13
      Height = 13
      Caption = '%'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 454
      Top = 6
      TabStop = False
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsCenovnikGodina
      TabOrder = 2
      Visible = False
      ExplicitLeft = 454
      ExplicitTop = 6
      ExplicitWidth = 94
      Width = 94
    end
    inherited OtkaziButton: TcxButton
      Left = 503
      Top = 180
      TabOrder = 7
      ExplicitLeft = 503
      ExplicitTop = 180
    end
    inherited ZapisiButton: TcxButton
      Left = 422
      Top = 180
      TabOrder = 6
      ExplicitLeft = 422
      ExplicitTop = 180
    end
    object RadioDB_tipZemjiste: TcxDBRadioGroup
      Left = 126
      Top = 31
      TabStop = False
      Caption = #1058#1080#1087' '#1085#1072' '#1079#1077#1084#1112#1080#1096#1090#1077
      DataBinding.DataField = 'TIP_ZEMJISTE'
      DataBinding.DataSource = dm.dsCenovnikGodina
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1047#1077#1084#1112#1086#1076#1077#1083#1089#1082#1086
          Value = 1
        end
        item
          Caption = #1043#1088#1072#1076#1077#1078#1085#1086
          Value = 2
        end>
      TabOrder = 0
      Height = 43
      Width = 194
    end
    object CENA_BEZ_DDV: TcxDBTextEdit
      Tag = 1
      Left = 126
      Top = 85
      BeepOnEnter = False
      DataBinding.DataField = 'CENA_BEZ_DDV'
      DataBinding.DataSource = dm.dsCenovnikGodina
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 94
    end
    object CENA_SO_DDV: TcxDBTextEdit
      Left = 126
      Top = 166
      TabStop = False
      BeepOnEnter = False
      DataBinding.DataField = 'CENA_SO_DDV'
      DataBinding.DataSource = dm.dsCenovnikGodina
      Enabled = False
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      StyleDisabled.TextColor = clInfoText
      TabOrder = 5
      OnKeyDown = EnterKakoTab
      Width = 94
    end
    object FOND_ZA_VODI: TcxDBTextEdit
      Tag = 1
      Left = 126
      Top = 112
      BeepOnEnter = False
      DataBinding.DataField = 'FOND_ZA_VODI'
      DataBinding.DataSource = dm.dsCenovnikGodina
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 3
      OnKeyDown = EnterKakoTab
      Width = 94
    end
    object DDV: TcxDBTextEdit
      Tag = 1
      Left = 126
      Top = 139
      BeepOnEnter = False
      DataBinding.DataField = 'DDV'
      DataBinding.DataSource = dm.dsCenovnikGodina
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 4
      OnKeyDown = EnterKakoTab
      Width = 94
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 594
    ExplicitWidth = 594
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 636
    Width = 594
    ExplicitTop = 636
    ExplicitWidth = 594
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 240
  end
  inherited PopupMenu1: TPopupMenu
    Top = 232
  end
  inherited dxBarManager1: TdxBarManager
    ImageOptions.Images = dmRes.cxLargeImages
    Top = 248
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 224
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 43005.562317233790000000
      StyleRepository = nil
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      Styles.StyleSheet = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 520
    Top = 200
    PixelsPerInch = 96
  end
end
