unit Vodokorisnik;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbonCustomizationForm,
  dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore, dxPScxCommon,
  System.Actions, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxGroupBox, cxRadioGroup, cxCheckBox, cxImageComboBox;

type
  TfrmVodokorisnik = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1PRAVNO_LICE: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1TATKO: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1EMB: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1TEL: TcxGridDBColumn;
    cxGrid1DBTableView1FAX: TcxGridDBColumn;
    txtID: TcxDBTextEdit;
    txtPrezime: TcxDBTextEdit;
    txtTatko: TcxDBTextEdit;
    txtTel: TcxDBTextEdit;
    txtEMB: TcxDBTextEdit;
    lblnaziv: TLabel;
    rgPravnoLice: TcxDBRadioGroup;
    Label3: TLabel;
    txtMesto: TcxDBTextEdit;
    cmbMesto: TcxDBLookupComboBox;
    Label8: TLabel;
    Label10: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtFax: TcxDBTextEdit;
    txtAdresa: TcxDBTextEdit;
    Label2: TLabel;
    Label6: TLabel;
    txtLK: TcxDBTextEdit;
    Label7: TLabel;
    Label9: TLabel;
    txtOldID: TcxDBTextEdit;
    cxGrid1DBTableView1LK: TcxGridDBColumn;
    txtAdresa2: TcxDBTextEdit;
    Label11: TLabel;
    cxGrid1DBTableView1ADRESA_2: TcxGridDBColumn;
    txtDanocen: TcxDBTextEdit;
    Label12: TLabel;
    cxGrid1DBTableView1DANOCEN: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    aKarticaFinansiska: TAction;
    aKarticaSiteGodini: TAction;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGroupBoxStatus: TcxGroupBox;
    cxDBCheckBox1: TcxDBCheckBox;
    STATUS_OPIS: TcxDBTextEdit;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_OPIS: TcxGridDBColumn;
    lbl1: TLabel;
    cxPOVRSINA: TcxDBTextEdit;
    cxGrid1DBTableView1POVRSINA: TcxGridDBColumn;
    procedure aRefreshExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aKarticaFinansiskaExecute(Sender: TObject);
    procedure aKarticaSiteGodiniExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVodokorisnik: TfrmVodokorisnik;

implementation

uses
  dmUnit, dmKonekcija, Mesto, dmResources;

{$R *.dfm}

procedure TfrmVodokorisnik.aKarticaFinansiskaExecute(Sender: TObject);
begin
//  inherited;
//    try
//      dmRes.Spremi('VZ',33101);
//      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:= dm.tblVodokorisnikID.Value; //txtVK.EditValue;
//      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
//      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;
//
//      dmRes.frxReport1.ShowReport();
//    except
//      ShowMessage('�� ���� �� �� ������� ���������!');
//    end;
end;

procedure TfrmVodokorisnik.aKarticaSiteGodiniExecute(Sender: TObject);
begin
//  inherited;
//    try
//      dmRes.Spremi('VZ',33102);
//      dmKon.tblSqlReport.ParamByName('vodokorisnik_id').Value:=dm.tblVodokorisnikID.Value;
//      dmKon.tblSqlReport.ParamByName('re').Value:=dmkon.re;
//      dmKon.tblSqlReport.ParamByName('godina').Value:=dm.godina;
//
//      dmRes.frxReport1.ShowReport();
//    except
//      ShowMessage('�� ���� �� �� ������� ���������!');
//    end;
end;

procedure TfrmVodokorisnik.aRefreshExecute(Sender: TObject);
begin
  inherited;
  dm.tblVodokorisnici.close;
  dm.tblVodokorisnici.Open;
end;

procedure TfrmVodokorisnik.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
    if(Key = VK_INSERT) then
    begin
          if(Sender = txtMesto) then
          begin
              frmMesto := TfrmMesto.Create(self, false);
              frmMesto.ShowModal;
              if (frmMesto.ModalResult = mrOK) then
                //dmMat.tblPartnerMESTO.Value := StrToInt(frmMesto.GetSifra(0));
                dm.tblVodokorisniciMESTO.Value:=StrToInt(frmMesto.GetSifra(0));
              frmMesto.Free;
          end;

    end;

end;

initialization
  RegisterClass(TfrmVodokorisnik);
end.
