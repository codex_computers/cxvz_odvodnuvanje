inherited frmKatasterskiOpstini: TfrmKatasterskiOpstini
  Caption = #1050#1072#1090#1072#1089#1090#1072#1088#1089#1082#1080' '#1086#1087#1096#1090#1080#1085#1080
  ClientHeight = 627
  ExplicitHeight = 666
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 358
    ExplicitHeight = 358
    inherited cxGrid1: TcxGrid
      Height = 354
      ExplicitHeight = 354
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsSifKatastarskaOpstina
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 65
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 350
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 250
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 484
    Height = 120
    ExplicitTop = 484
    ExplicitHeight = 120
    object Label2: TLabel [1]
      Left = 21
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1096#1090#1080#1085#1072
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsSifKatastarskaOpstina
    end
    inherited OtkaziButton: TcxButton
      Top = 80
      TabOrder = 3
      ExplicitTop = 80
    end
    inherited ZapisiButton: TcxButton
      Top = 80
      TabOrder = 2
      ExplicitTop = 80
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 45
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1086#1087#1096#1090#1080#1085#1072
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsSifKatastarskaOpstina
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 484
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 604
    ExplicitTop = 604
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 256
    Top = 232
  end
  inherited dxBarManager1: TdxBarManager
    Top = 232
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButtonSoberi'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 358
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    inherited dxBarLBtnSnimiIzgled: TdxBarLargeButton
      ScreenTip = tipSnimiIzgled
    end
    inherited dxBarLBtnBrisiIzgled: TdxBarLargeButton
      ScreenTip = tipBrisiIzgled
    end
    inherited dxBarLargeButtonSoberi: TdxBarLargeButton
      ScreenTip = tipSpustiSoberi
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aRefresh
      Category = 0
      ScreenTip = tipOsvezi
    end
  end
  inherited ActionList1: TActionList
    inherited aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.Orientation = poPortrait
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 41155.465156562500000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
    object dxBarScreenTipRepository1ScreenTip1: TdxScreenTip
      Header.Text = 'dxBarScreenTipRepository1ScreenTip1'
    end
    object tipSpustiSoberi: TdxScreenTip
      Header.Text = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1090#1072#1073#1077#1083#1072
      Description.Text = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1075#1080' '#1076#1077#1092#1080#1085#1080#1088#1072#1085#1080#1090#1077' '#1075#1088#1091#1087#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipBrisiIzgled: TdxScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1041#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipOsvezi: TdxScreenTip
      Header.Text = #1054#1089#1074#1077#1078#1080' '#1090#1072#1073#1077#1083#1072
      Description.Text = 
        #1054#1089#1074#1077#1078#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1074#1086' '#1090#1072#1073#1077#1083#1072#1090#1072' ('#1086#1090#1089#1090#1088#1072#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1092#1080#1083#1090#1088#1080#1090#1077' '#1085#1072' '#1090#1072 +
        #1073#1077#1083#1072#1090#1072' '#1080' '#1086#1089#1074#1077#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077')'
    end
    object tipPecatiTabela: TdxScreenTip
      Header.Text = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Description.Text = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1087#1086#1076#1072#1090#1086#1094#1080
    end
    object tipNov: TdxScreenTip
      Header.Text = #1053#1086#1074#1072' '#1082#1072#1090#1072#1089#1090#1072#1088#1089#1082#1072' '#1086#1087#1096#1090#1080#1085#1072
      Description.Text = #1045#1074#1080#1076#1077#1085#1090#1080#1088#1072#1112' '#1085#1086#1074#1072' '#1082#1072#1090#1072#1089#1090#1072#1088#1089#1082#1072' '#1086#1087#1096#1090#1080#1085#1072
    end
    object tipAzuriraj: TdxScreenTip
      Header.Text = #1040#1078#1091#1088#1080#1088#1072#1112' '#1082#1072#1090#1072#1089#1090#1072#1088#1089#1082#1072' '#1086#1087#1096#1090#1080#1085#1072
      Description.Text = #1040#1078#1091#1088#1080#1088#1072#1112' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1079#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1072#1090#1072' '#1082#1072#1090#1072#1089#1090#1072#1088#1089#1082#1072' '#1086#1087#1096#1090#1080#1085#1072
    end
    object tipBrisi: TdxScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1072#1090#1072#1089#1090#1072#1088#1089#1082#1072' '#1086#1087#1096#1090#1080#1085#1072
      Description.Text = #1041#1088#1080#1096#1080' '#1112#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1072#1090#1072' '#1082#1072#1090#1072#1089#1090#1072#1088#1089#1082#1072' '#1086#1087#1096#1090#1080#1085#1072
    end
  end
  object dxBarLargeButtonAzuriraj: TdxBarLargeButton
    Category = -1
    ScreenTip = tipAzuriraj
    Visible = ivAlways
  end
  object dxBarLargeButtonBrisi: TdxBarLargeButton
    Category = -1
    ScreenTip = tipBrisi
    Visible = ivAlways
  end
  object dxBarLargeButtonPecatiTabela: TdxBarLargeButton
    Category = -1
    ScreenTip = tipPecatiTabela
    Visible = ivAlways
  end
  object dxBarLargeButtonNov: TdxBarLargeButton
    Category = -1
    ScreenTip = tipNov
    Visible = ivAlways
  end
end
