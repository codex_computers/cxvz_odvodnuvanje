unit UplataNalog;

interface

uses
  System.DateUtils,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, System.Actions,
  cxNavigator, dxCore, cxDateUtils;

type
//  niza = Array[1..5] of Variant;

  TfrmUplataNalog = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    pnlP: TPanel;
    pnlG: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dateOd: TcxDateEdit;
    dateDo: TcxDateEdit;
    btnPrikazi: TcxButton;
    aPrikazi: TAction;
    Label3: TLabel;
    Label2: TLabel;
    aSelektiraniVoNalog: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiVoNalog: TAction;
    aBrisiOdNalog: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    aSelektirajSite: TAction;
    cxBarEditItemNALOG: TcxBarEditItem;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1FAKTURA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1BANKA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DOKUMENT_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG_TIP: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1VODOKORISNIK_ID: TcxGridDBColumn;
    cxGrid1DBTableView1UPLATA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1PRAVNO_LICE: TcxGridDBColumn;
    cxGrid1DBTableView1TATKO: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1EMB: TcxGridDBColumn;
    cxGrid1DBTableView1DANOCEN: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1KASA_PRIMI_BR: TcxGridDBColumn;
    cxGrid1DBTableView1KASA_RE: TcxGridDBColumn;
    cxGrid1DBTableView1KASA_SMETKA: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1BANKOV_IZVOD_BR: TcxGridDBColumn;
    cxGrid1DBTableView1BANKA_TIP: TcxGridDBColumn;
    cxGrid1DBTableView1BANKA_ID: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aPrikaziExecute(Sender: TObject);
    procedure ZatvoriDataset(Sender: TObject);
    procedure aSelektiraniVoNalogExecute(Sender: TObject);
    procedure aDodadiVoNalogExecute(Sender: TObject);
    procedure aBrisiOdNalogExecute(Sender: TObject);
    procedure aSelektirajSiteExecute(Sender: TObject);
    procedure aSelektiraniVoNalogUpdate(Sender: TObject);
    procedure aBrisiOdNalogUpdate(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmUplataNalog: TfrmUplataNalog;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmUplataNalog.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmUplataNalog.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmUplataNalog.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmUplataNalog.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmUplataNalog.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;


procedure TfrmUplataNalog.aBrisiOdNalogExecute(Sender: TObject);
var i, flag:Integer;
    status: TStatusWindowHandle;
    datum:string;
begin
{
  //aPrikazi.Execute;
  flag:=-1;
  if (cxGrid1DBTableView1.Controller.SelectedRecordCount >0) then
    begin
      frmDaNe := TfrmDaNe.Create(self, '������ �� ������������� ������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
      if (frmDaNe.ShowModal = mrYes) then
          begin
            status := cxCreateStatusWindow();
            try
                with (cxGrid1DBTableView1.DataController) do
                for I := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
                    begin
                      flag:=cxGrid1DBTableView1.Controller.SelectedRecords[i].RecordIndex;

                      dm.pUplataVoNalog.Close;
                      dm.pUplataVoNalog.ParamByName('vodokorisnik_id').Value:=GetValue(flag,cxGrid1DBTableView1VODOKORISNIK_ID.Index);
                      dm.pUplataVoNalog.ParamByName('uplata_id').Value:=GetValue(flag,cxGrid1DBTableView1UPLATA_ID.Index);
                      dm.pUplataVoNalog.ParamByName('fin_nalog').Value:=Null;//nalog od panel ili od kade
                      dm.pUplataVoNalog.ExecProc;

                    end;
                if flag <> -1 then ShowMessage('������� ������ �� ����� !!!');
                aPrikazi.Execute;
            finally
 	            cxRemoveStatusWindow(status);
//              dm.tblUplataPeriod.Close;
//              dm.tblUplataPeriod.Open;
            end;
          end
    end
  else ShowMessage('����������� ������ !!!')
  }
end;
procedure TfrmUplataNalog.aBrisiOdNalogUpdate(Sender: TObject);
begin
  aBrisiodNalog.Enabled:=(dm.saldirana = 0) //ne e saldirana godinata
  and (dm.tblUplataPeriod.RecordCount>0)
      and (cxGrid1DBTableView1.DataController.FilteredRecordCount>0)
      and (cxGrid1DBTableView1.Controller.SelectedRecordCount >0);
end;

//	����� �� ���������� �� ����������
procedure TfrmUplataNalog.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmUplataNalog.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmUplataNalog.aSelektirajSiteExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.SelectAll;
//  cxGrid1DBTableView1.DataController.se;
end;

procedure TfrmUplataNalog.aSelektiraniVoNalogExecute(Sender: TObject);
var i, flag:Integer;
    status: TStatusWindowHandle;
    datum:string;
begin
 {
  if NOT(varisnull(cxBarEditItemNALOG.EditValue)) then
    begin
      if (cxGrid1DBTableView1.Controller.SelectedRecordCount >0) then
        begin
          frmDaNe := TfrmDaNe.Create(self, '��������� �� ������������� ������ �� �����', '���� ��������� ������ �� �� ��������?', 1);
          if (frmDaNe.ShowModal = mrYes) then
          begin
            status := cxCreateStatusWindow();
            try
              with (cxGrid1DBTableView1.DataController) do
              for I := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
                begin
                  flag:=cxGrid1DBTableView1.Controller.SelectedRecords[i].RecordIndex;

                  //ShowMessage(VarToStr(GetValue(flag,cxGrid1DBTableView1UPLATA_ID.Index)));
                  dm.pUplataVoNalog.Close;
                  dm.pUplataVoNalog.ParamByName('vodokorisnik_id').Value:=GetValue(flag,cxGrid1DBTableView1VODOKORISNIK_ID.Index);
                  dm.pUplataVoNalog.ParamByName('uplata_id').Value:=GetValue(flag,cxGrid1DBTableView1UPLATA_ID.Index);
                  dm.pUplataVoNalog.ParamByName('fin_nalog').Value:=cxBarEditItemNALOG.EditValue; // 2001459;//nalog od panel ili od kade
                  dm.pUplataVoNalog.ExecProc;
                end;
               if flag <> -1 then ShowMessage('������a ��������� �� ����� !!!');
               aPrikazi.Execute;
            finally
              cxRemoveStatusWindow(status);
            end;
          end
        end
        else ShowMessage('����������� ������ !!!');
    end
  else ShowMessage('�������� ����� !!!');    }
end;

procedure TfrmUplataNalog.aSelektiraniVoNalogUpdate(Sender: TObject);
begin
 { aSelektiraniVoNalog.Enabled:=(dm.saldirana = 0) //ne e saldirana godinata
  and (dm.tblUplataPeriod.RecordCount>0)
    and (cxGrid1DBTableView1.DataController.FilteredRecordCount>0)
    and (cxGrid1DBTableView1.Controller.SelectedRecordCount >0)
    and not(VarIsNull(cxBarEditItemNALOG.EditValue))   }
end;

procedure TfrmUplataNalog.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmUplataNalog.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmUplataNalog.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmUplataNalog.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmUplataNalog.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmUplataNalog.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmUplataNalog.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmUplataNalog.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmUplataNalog.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmUplataNalog.prefrli;
begin
end;

procedure TfrmUplataNalog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmUplataNalog.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmUplataNalog.FormShow(Sender: TObject);
begin
//  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
//	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

 //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

   { dm.tblUplataPeriod.Close; //ili na form klose da se stave
    dm.tblNalogUplata.Close;
    dm.tblNalogUplata.ParamByName('re').Value:=dmkon.re;
    dm.tblNalogUplata.ParamByName('godina').Value:=dm.godina;
    dm.tblNalogUplata.Open;             }

    sobrano := true;
//    dateOd.Date:= StartOfAYear(dm.godina);
    dateOd.Date:= StartOfTheMonth(date);
    dateDo.Date:=Date;
    dateod.SetFocus;
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
//cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmUplataNalog.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmUplataNalog.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmUplataNalog.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmUplataNalog.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmUplataNalog.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmUplataNalog.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmUplataNalog.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmUplataNalog.aPrikaziExecute(Sender: TObject);
begin
  if ((dateOd.Text<>'') and (dateDo.Text <>'')) then
  begin
    //������ ����_��_������_��_������
    dm.tblUplataPeriod.Close;
    dm.tblUplataPeriod.ParamByName('re').Value:=dmkon.re;
    dm.tblUplataPeriod.ParamByName('godina').Value:=dm.godina;
    dm.tblUplataPeriod.ParamByName('datum_od').Value:=dateOd.Date;
    dm.tblUplataPeriod.ParamByName('datum_do').Value:=dateDo.Date;
    dm.tblUplataPeriod.ParamByName('param_datum').Value:=1;
    dm.tblUplataPeriod.Open;

    if dm.tblUplataPeriod.RecordCount>0 then cxGrid1.SetFocus;

  end
  else
  begin
    dm.tblUplataPeriod.Close;
    dm.tblUplataPeriod.ParamByName('re').Value:=dmkon.re;
    dm.tblUplataPeriod.ParamByName('godina').Value:=dm.godina;
    dm.tblUplataPeriod.ParamByName('param_datum').Value:=0;
    dm.tblUplataPeriod.Open;
  end;
end;

procedure TfrmUplataNalog.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmUplataNalog.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmUplataNalog.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmUplataNalog.aDodadiVoNalogExecute(Sender: TObject);
begin
  //pokazi go panelot za izbor na nalog
  //
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmUplataNalog.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmUplataNalog.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

procedure TfrmUplataNalog.ZatvoriDataset(Sender: TObject);
begin
//  dm.tblUplataPeriod.Close;
  cxGrid1DBTableView1.DataController.DataSet.Close;
end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmUplataNalog.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
